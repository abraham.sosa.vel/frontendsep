import { Search } from "@mui/icons-material";
import {
  Grid,
  InputAdornment,
  Paper,
  TextField,
  useTheme,
} from "@mui/material";
import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router";
import { BaseCreateButton } from "../../components/BaseCreateButton/BaseCreateButton";
import BaseModalConfirm from "../../components/BaseModalConfirm";
import BaseTable from "../../components/BaseTable";
import TitlePage from "../../components/TitlePage";
import { useModal } from "../../utils/useModal";
import ItemRegistersTable from "./components/ItemRegistersTable";
import { isEditingSuccess, setValuesForm } from "./redux/redux";
import {
  deleteRegisterStart,
  getForUpdateRegister,
  searchAllRegistersStart,
} from "./redux/thunk";
import { getRegistersFormat } from "./utils/formatRegister";
import { useColumnsRegisters } from "./utils/useColumnsRegisters";

const Register = () => {
  const dispatch = useDispatch();
  const [statusModal, handleCloseModal, handleOpenModal] = useModal();
  const [idDelete, setIdDelete] = useState(false);
  const navigate = useNavigate();
  const theme = useTheme();
  const [t] = useTranslation("global");
  const { registers, loaderRegisters } = useSelector((state) => state.register);
  const onDelete = (id) => {
    setIdDelete(id);
    handleOpenModal();
  };
  const confirmModal = () => {
    dispatch(deleteRegisterStart(idDelete));
    handleCloseModal();
  };
  const onEdit = (id) => {
    dispatch(getForUpdateRegister(id));
    navigate("/registers/create");
  };
  const onCreate = () => {
    navigate("/registers/create");
    dispatch(isEditingSuccess(false));
    dispatch(setValuesForm());
  };
  const searchRegister = (e) => {
    dispatch(searchAllRegistersStart(e.target.value));
  };
  return (
    <Paper
      sx={{ p: 2, pt: 3 }}
      elevation={theme.palette.mode === "dark" ? 0 : 5}
    >
      <BaseModalConfirm
        title="¿Esta seguro que desea eliminar el registro"
        status={statusModal}
        handleClose={handleCloseModal}
        confirmModal={confirmModal}
      />
      <Grid container direction="column">
        <Grid sx={{ mb: 1 }}>
          <TitlePage title={t("NavItems.registers").toUpperCase()} />
        </Grid>
        <Grid
          item
          container
          xs={12}
          spacing={2}
          justifyContent="space-between"
          direction={{ xs: "column-reverse", md: "row" }}
        >
          {/*Input Search*/}
          <Grid item xs={12} md={6}>
            <TextField
              size="small"
              fullWidth
              type="search"
              id="input-search"
              placeholder="Buscar por: Id"
              onChange={searchRegister}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <Search />
                  </InputAdornment>
                ),
              }}
              variant="outlined"
            />
          </Grid>
          {/*Button create*/}
          <Grid item container xs={12} md={4} justifyContent="flex-end">
            <BaseCreateButton onClick={onCreate} />
          </Grid>
        </Grid>
        <BaseTable
          columns={useColumnsRegisters()}
          loading={loaderRegisters}
          rowsLength={6}
        >
          {getRegistersFormat(registers).map((e) => (
            <ItemRegistersTable
              key={e.id}
              item={e}
              handleDeleteUser={onDelete}
              handleEditUser={onEdit}
            />
          ))}
        </BaseTable>
      </Grid>
    </Paper>
  );
};

export default Register;
