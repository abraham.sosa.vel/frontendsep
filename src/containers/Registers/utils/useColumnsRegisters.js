import { useTranslation } from "react-i18next";
import createColumns from "../../../components/BaseTable/createColumns";

export const useColumnsRegisters = () => {
  const [t] = useTranslation("global");
  return [
    createColumns({ id: "action", title: t("tableRegister.action") }),
    createColumns({ id: "id", title: t("tableRegister.id") }),
    createColumns({ id: "deliteType", title: t("tableRegister.deliteType") }),
    createColumns({
      id: "registerDate",
      title: t("tableRegister.registerDate"),
    }),
    createColumns({ id: "victimName", title: t("tableRegister.victimName") }),
    createColumns({
      id: "victimLastName",
      title: t("tableRegister.victimLastName"),
    }),
    createColumns({
      id: "perpetratorName",
      title: t("tableRegister.perpetratorName"),
    }),
    createColumns({
      id: "perpetratorLastName",
      title: t("tableRegister.perpetratorLastName"),
    }),
    createColumns({ id: "numberCase", title: t("tableRegister.numberCase") }),
    createColumns({
      id: "actionsSepdavi",
      title: t("tableRegister.actionsSepdavi"),
    }),
    createColumns({
      id: "proceduralStage",
      title: t("tableRegister.proceduralStage"),
    }),
  ];
};
