import { store } from "../../../redux/store";

export const getRegistersFormat = (data) => {
  const format = data?.map((e) => {
    return {
      id: e.id,
      crime: e.registroDelitos.map((e) => e.delito.nombre),
      registerDate: e?.fechaRegistro,
      victimName: e?.informacionVictimas.map((e) => e?.persona?.nombre),
      victimLastName: e?.informacionVictimas.map((e) => e?.persona?.apellido),
      denouncedName: e?.informacionDenunciados.map((e) => e?.persona?.nombre),
      denouncedLastName: e?.informacionDenunciados.map(
        (e) => e?.persona?.apellido
      ),
      numberCase: e?.numeroCaso,
      actionsSepdavi: e?.tipoAsistencia,
      proceduralStage:
        e.etapaCasos.length === 0
          ? "Sin etapa"
          : e.etapaCasos[e.etapaCasos.length - 1].etapasProcesale.nombre,
    };
  });
  return format.sort((a, b) => b.id - a.id);
};
export const registerFormatData = (
  general,
  victim,
  complainant,
  Perpetrator
) => {
  const { auth, register } = store.getState();

  const formatData = {
    register: {
      fechaRegistro: general.registerDate,
      casoAntiguo: general.oldCase,
      area: general.state,
      descripcion: general.description,
      observacion: general.observation,
      direccion: general.direction,
      fechaInicioProceso: general.processStartDate,
      numeroCaso: general.numberCase,
      tipoAsistencia: general.serviceType,
      accionesSepdavi: general.actionsSepdavi,
      tieneSentencia: general.hasSentence,
      usuarioId: auth.auth.user.id,
      ciudadId: general.city,
    },
    delito: {
      delito: register.otherCrimes.filter((e) => e.label === general.crimes),
    },
    victim: {
      nombre: victim.name,
      apellido: victim.lastName,
      fechaNacimiento: victim.birthday,
      sexo: victim.sex,
      typoDocumento: victim.typeDocument,
      numeroDocumento: victim.numberDocument,
      direccion: victim.direction,
      telefono: victim.phoneNumber === "" ? 0 : victim.phoneNumber,
      observacion: victim.observation,
      nacionalidad: victim.nacionality,
      gradoInstruccionId: register.otherInstructionDegree.filter(
        (e) => e.label === victim.instructionDegree
      ),
      ocupacionId: register.otherOccupation.filter(
        (e) => e.label === victim.occupation
      ),
      orientacionSexualId: victim.sexualOrientation,
    },
    perpetrator: {
      nombre: Perpetrator.name,
      apellido: Perpetrator.lastName,
      fechaNacimiento: Perpetrator.birthday,
      sexo: Perpetrator.sex,
      typoDocumento: Perpetrator.typeDocument,
      numeroDocumento: Perpetrator.numberDocument,
      direccion: Perpetrator.direction,
      telefono: Perpetrator.phoneNumber === "" ? 0 : Perpetrator.phoneNumber,
      observacion: Perpetrator.observation,
      nacionalidad: Perpetrator.nacionality,
      gradoInstruccionId: register.otherInstructionDegree.filter(
        (e) => e.label === Perpetrator.instructionDegree
      ),
      ocupacionId: register.otherOccupation.filter(
        (e) => e.label === Perpetrator.occupation
      ),
      orientacionSexualId: Perpetrator.sexualOrientation,
    },
    complainant: {
      nombre: complainant.name,
      apellido: complainant.lastName,
      fechaNacimiento: complainant.birthday,
      sexo: complainant.sex,
      typoDocumento: complainant.typeDocument,
      numeroDocumento: complainant.numberDocument,
      direccion: complainant.direction,
      telefono: complainant.phoneNumber === "" ? 0 : complainant.phoneNumber,
      observacion: complainant.observation,
      nacionalidad: complainant.nacionality,
      gradoInstruccionId: register.otherInstructionDegree.filter(
        (e) => e.label === complainant.instructionDegree
      ),
      ocupacionId: register.otherOccupation.filter(
        (e) => e.label === complainant.occupation
      ),
      orientacionSexualId: complainant.sexualOrientation,
    },
  };
  if (general.hasSentence === "true") {
    formatData.register.fechaSentencia = general.sentenceDate;
    formatData.register.numeroSentencia = general.sentenceNumber;
  }
  return formatData;
};
