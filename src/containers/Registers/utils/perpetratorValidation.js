import { useState } from "react";
import { useTranslation } from "react-i18next";
import {
  regexNumber,
  regexOnlyLetters,
} from "../../../utils/regularExpresions";

const useValidationsPerpetrator = () => {
  const [message, setMessages] = useState({
    name: "",
    lastName: "",
    birthday: "",
    sex: "",
    documentType: "",
    numberDocument: "",
    sexualOrientation: "",
    nacionality: "",
    direction: "",
    phoneNumber: "",
    observation: "",
    occupation: "",
    instructionDegree: "",
    birthday: "",
  });
  const [t] = useTranslation("global");
  //validate name
  const validationName = (value) => {
    if (value === "") {
      setMessages({ ...message, name: t("errorMesages.required") });
      return false;
    }
    if (!regexOnlyLetters.test(value)) {
      setMessages({ ...message, name: t("errorMesages.onlyLetters") });
      return false;
    }
    if (value.length > 45) {
      setMessages({
        ...message,
        name: `${t("errorMesages.maxLength")} 45 ${t(
          "errorMesages.caracters"
        )}`,
      });
      return false;
    }
    return true;
  };
  const validationLastName = (value) => {
    if (value === "") {
      setMessages({ ...message, lastName: t("errorMesages.required") });
      return false;
    }
    if (!regexOnlyLetters.test(value)) {
      setMessages({ ...message, lastName: t("errorMesages.onlyLetters") });
      return false;
    }
    if (value.length > 45) {
      setMessages({
        ...message,
        lastName: `${t("errorMesages.maxLength")} 45 ${t(
          "errorMesages.caracters"
        )}`,
      });
      return false;
    }
    return true;
  };
  //sex
  const validationSex = (value) => {
    if (value === "") {
      setMessages({ ...message, sex: t("errorMesages.required") });
      return false;
    }
    return true;
  };
  const validationTypeDocument = (value) => {
    if (value === "") {
      setMessages({ ...message, documentType: t("errorMesages.required") });
      return false;
    }
    return true;
  };
  const validationNumberDocument = (value) => {
    if (value === "") {
      setMessages({ ...message, numberDocument: t("errorMesages.required") });
      return false;
    }
    if (value.length > 45) {
      setMessages({
        ...message,
        numberDocument: `${t("errorMesages.maxLength")} 45 caracteres`,
      });
      return false;
    }
    return true;
  };
  const validationSexualOrientation = (value) => {
    if (value === "") {
      setMessages({
        ...message,
        sexualOrientation: t("errorMesages.required"),
      });
      return false;
    }
    return true;
  };
  const validationNacionality = (value) => {
    if (value === "") {
      setMessages({ ...message, nacionality: t("errorMesages.required") });
      return false;
    }
    if (!regexOnlyLetters.test(value)) {
      setMessages({ ...message, nacionality: t("errorMesages.onlyLetters") });
      return false;
    }
    if (value.length > 50) {
      setMessages({
        ...message,
        nacionality: `${t("errorMesages.maxLength")} 50 ${t(
          "errorMesages.caracters"
        )}`,
      });
      return false;
    }
    return true;
  };
  const validationDirection = (value) => {
    if (value.length > 200) {
      setMessages({
        ...message,
        direction: `${t("errorMesages.maxLength")} 200 ${t(
          "errorMesages.caracters"
        )}`,
      });
      return false;
    }
    return true;
  };
  const validationPhoneNumber = (value) => {
    if (!regexNumber.test(value)) {
      setMessages({ ...message, phoneNumber: t("errorMesages.onlyNumbers") });
      return false;
    }
    if (value.length > 50) {
      setMessages({
        ...message,
        phoneNumber: `${t("errorMesages.maxLength")} 50 ${t(
          "errorMesages.caracters"
        )}`,
      });
      return false;
    }
    return true;
  };
  const validationObservation = (value) => {
    if (value.length > 255) {
      setMessages({
        ...message,
        observation: `${t("errorMesages.maxLength")} 255 ${t(
          "errorMesages.caracters"
        )}`,
      });
      return false;
    }
    return true;
  };
  const validationOccupation = (value) => {
    if (value === null || value === "") {
      setMessages({ ...message, occupation: t("errorMesages.required") });
      return false;
    }
    return true;
  };
  const validationInstructionDegree = (value) => {
    if (value === null || value === "") {
      setMessages({
        ...message,
        instructionDegree: t("errorMesages.required"),
      });
      return false;
    }
    return true;
  };
  const validateBirthday = (value) => {
    if (value === null) {
      setMessages({ ...message, birthday: t("errorMesages.required") });
      return false;
    }
    return true;
  };
  return {
    validateBirthday,
    validationName,
    validationLastName,
    validationSex,
    validationTypeDocument,
    validationNumberDocument,
    validationSexualOrientation,
    validationNacionality,
    validationDirection,
    validationPhoneNumber,
    validationObservation,
    validationOccupation,
    validationInstructionDegree,
    message,
  };
};
export default useValidationsPerpetrator;
