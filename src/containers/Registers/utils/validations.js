import { useState } from "react";
import { useTranslation } from "react-i18next";

const useValidations = () => {
  const [message, setMessages] = useState({
    registerDate: "",
    processStartDate: "",
  });
  const [cityMessage, setCity] = useState(true);
  const [stateMessage, setState] = useState(true);
  const [numberCaseMessage, setNumberCase] = useState(true);
  const [oldCaseMessage, setOldCase] = useState(true);
  const [observationMessage, setObservation] = useState(true);
  const [descriptionMessage, setDescription] = useState(true);
  const [directionMessage, setDirection] = useState(true);
  const [actionsSepdaviMessage, setActionSepdavi] = useState(true);
  const [sentenceMessage, setSentence] = useState(false);
  const [crimesMessage, setCrimes] = useState(true);

  const [t] = useTranslation("global");
  /*GENERAL INFORMATION */
  const validationCity = (value) => {
    if (value === "") {
      setCity(t("validateRegister.validateCity"));
      return false;
    }
    setCity(false);
    return true;
  };
  const validationState = (value) => {
    if (value === "") {
      setState(t("validateRegister.validateZone"));
      return false;
    }
    setState(false);
    return true;
  };
  const validationNumberCase = (value) => {
    if (value === "") {
      setNumberCase(t("validateRegister.validateNumberCase"));
      return false;
    }
    if (value.length > 50) {
      setNumberCase(`${t("errorMesages.maxLength")} 50 caracteres`);
      return false;
    }
    setNumberCase(false);
    return true;
  };
  const validateOldCase = (value) => {
    if (value === "") {
      setOldCase(t("errorMesages.required"));
      return false;
    }
    setOldCase(false);
    return true;
  };
  const validateObservation = (value) => {
    if (value === "") {
      setObservation(t("errorMesages.required"));
      return false;
    }
    if (value.length > 255) {
      setObservation(`${t("errorMesages.maxLength")} 255 caracteres`);
      return false;
    }
    setObservation(false);
    return true;
  };
  const validateDescription = (value) => {
    if (value === "") {
      setDescription(t("errorMesages.required"));
      return false;
    }
    if (value.length > 255) {
      setDescription(`${t("errorMesages.maxLength")} 255 caracteres`);
      return false;
    }
    setDescription(false);
    return true;
  };
  const validatedirection = (value) => {
    if (value === "") {
      setDirection(t("errorMesages.required"));
      return false;
    }
    if (value.length > 100) {
      setDirection(`${t("errorMesages.maxLength")} 100 caracteres`);
      return false;
    }
    setDirection(false);
    return true;
  };
  const validateActionsSepdavi = (value) => {
    if (value === "") {
      setActionSepdavi(t("errorMesages.required"));
      return false;
    }
    if (value.length > 255) {
      setActionSepdavi(`${t("errorMesages.maxLength")} 255 caracteres`);
      return false;
    }
    setActionSepdavi(false);
    return true;
  };
  const validateSentenceNumber = (value) => {
    if (value === "") {
      setSentence(t("errorMesages.required"));
      return false;
    }
    if (value.length > 100) {
      setSentence(`${t("errorMesages.maxLength")} 100 caracteres`);
      return false;
    }
    setSentence(false);
    return true;
  };
  const validationCrimes = (value) => {
    if (value === null || value === "") {
      setCrimes(t("errorMesages.required"));
      return false;
    }
    setCrimes(false);
    return true;
  };

  const validateStartDate = (value) => {
    if (value === null) {
      setMessages({ ...message, processStartDate: t("errorMesages.required") });
      return false;
    }
    return true;
  };
  const validateRegisterDate = (value) => {
    if (value === null) {
      setMessages({ ...message, registerDate: t("errorMesages.required") });
      return false;
    }
    return true;
  };

  return {
    message,
    validateStartDate,
    validateRegisterDate,
    validationNumberCase,
    validationCity,
    validationState,
    validateOldCase,
    validateObservation,
    validateDescription,
    validatedirection,
    validateActionsSepdavi,
    validateSentenceNumber,
    validationCrimes,
    cityMessage,
    stateMessage,
    numberCaseMessage,
    oldCaseMessage,
    observationMessage,
    descriptionMessage,
    directionMessage,
    actionsSepdaviMessage,
    sentenceMessage,
    crimesMessage,
  };
};
export default useValidations;
