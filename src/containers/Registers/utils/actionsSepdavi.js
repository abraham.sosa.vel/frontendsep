export const actionsSepdavi = [
  {
    id: "Patrocinio activo",
    name: "Patrocinio activo",
    description: "El caso sera atendido actualmente por el SEPDAVI",
    icon: "Stars",
  },
  {
    id: "Patrocinio inactivo",
    name: "Patrocinio inactivo",
    description: "El caso no sera atendido actualmente por el SEPDAVI",
    icon: "Unpublished",
  },
  {
    id: "Orientación jurídica",
    name: "Orientación jurídica",
    description: "Solo se realizo una orientación jurídica",
    icon: "Help",
  },
];
