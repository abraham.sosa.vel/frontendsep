import {
  Autocomplete,
  Button,
  FormControl,
  FormControlLabel,
  FormHelperText,
  FormLabel,
  Grid,
  InputLabel,
  MenuItem,
  Radio,
  RadioGroup,
  Select,
  Skeleton,
  TextField,
  Typography,
} from "@mui/material";

import { DatePicker } from "@mui/x-date-pickers";
import React, { useEffect, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import SelectRol from "../../../components/SelectRol/SelectRol";
import { sendGeneralInformation } from "../redux/redux";
import { getAllCitiesStart, getAllCrimesStart } from "../redux/thunk";
import { actionsSepdavi } from "../utils/actionsSepdavi";
import useValidations from "../utils/validations";

const GeneralInformation = ({ handleNext }) => {
  const { generalInformation } = useSelector((state) => state.register);
  const dispatch = useDispatch();
  const [t] = useTranslation("global");
  const [defaultValues, setDefaultValue] = useState({
    city: "",
    state: "",
    oldCase: "",
    serviceType: "Patrocinio activo",
    hasSentence: "false",
    crimes: "",
    registerDate: null,
    processStartDate: null,
  });
  const {
    register,
    control,
    setValue,
    handleSubmit,
    formState: { errors },
    watch,
    reset,
  } = useForm({
    mode: "onChange",
    defaultValues,
  });
  useEffect(() => {
    dispatch(getAllCitiesStart());
    dispatch(getAllCrimesStart());
  }, [dispatch]);
  useEffect(() => {
    if (Object.keys(generalInformation).length === 0) return;
    setDefaultValue(generalInformation);
  }, [generalInformation]);
  useEffect(() => {
    reset(defaultValues);
  }, [defaultValues, reset, generalInformation]);
  const {
    message,
    cityMessage,
    stateMessage,
    numberCaseMessage,
    oldCaseMessage,
    observationMessage,
    descriptionMessage,
    directionMessage,
    actionsSepdaviMessage,
    sentenceMessage,
    crimesMessage,
    validationCity,
    validationState,
    validationNumberCase,
    validateOldCase,
    validateObservation,
    validateActionsSepdavi,
    validateDescription,
    validatedirection,
    validateSentenceNumber,
    validateStartDate,
    validateRegisterDate,
    validationCrimes,
  } = useValidations();
  const { cities, loaderCities, crimes, loaderCrimes } = useSelector(
    (e) => e.register
  );
  const onSubmit = (data, event) => {
    if (Object.keys(generalInformation).length === 0) {
      data.processStartDate = data.processStartDate.$d.toISOString();
      data.registerDate = data.registerDate.$d.toISOString();
    }
    event.preventDefault();
    dispatch(sendGeneralInformation(data));
    handleNext();
  };
  const sentence = watch("hasSentence");
  return (
    <Grid container direction="column">
      <form onSubmit={handleSubmit(onSubmit)}>
        <Grid container alignItems="center" direction="column">
          <Typography
            variant="h5"
            component="h2"
            align="center"
            sx={{ color: "text.TextCuston" }}
          >
            {t("stepperRegister.generalInformation").toUpperCase()}
          </Typography>
          <Typography
            variant="subtitle2"
            component="h2"
            align="center"
            sx={{ color: "text.TextCuston", mt: 1, mb: 2, opacity: 1 }}
          >
            {t("stepperRegister.descriptiongeneralInformation")}
          </Typography>
        </Grid>
        <Grid container spacing={1}>
          <Grid item xs={12} md={6}>
            <Controller
              control={control}
              name="city"
              rules={{
                validate: validationCity,
              }}
              render={({ field: { onChange, value } }) => (
                <FormControl fullWidth margin="normal" error={!!errors?.city}>
                  <InputLabel>{t("createRegister.city")}</InputLabel>
                  {loaderCities ? (
                    <Skeleton variant="rounded" height={55} />
                  ) : (
                    <Select
                      fullWidth
                      onChange={onChange}
                      value={value}
                      label={t("createRegister.city")}
                    >
                      {cities.map((e) => (
                        <MenuItem key={e.id} value={e.id}>
                          {e.nombre}
                        </MenuItem>
                      ))}
                    </Select>
                  )}
                  <FormHelperText>{errors?.city && cityMessage}</FormHelperText>
                </FormControl>
              )}
            />
          </Grid>
          <Grid item xs={12} md={6} container justifyContent="center">
            <Controller
              name="state"
              control={control}
              rules={{
                validate: validationState,
              }}
              render={({ field: { onChange, value } }) => (
                <FormControl sx={{ mt: 2 }} error={!!errors?.state}>
                  <FormLabel sx={{ textAlign: "center" }}>
                    {t("createRegister.geographicZone")}
                  </FormLabel>
                  <RadioGroup
                    row
                    value={value}
                    onChange={onChange}
                    sx={{ justifyContent: "center" }}
                  >
                    <FormControlLabel
                      value={true}
                      control={<Radio />}
                      label={t("createRegister.urban")}
                    />
                    <FormControlLabel
                      value={false}
                      control={<Radio />}
                      label={t("createRegister.rural")}
                    />
                  </RadioGroup>
                  <FormHelperText>
                    {errors?.state && stateMessage}
                  </FormHelperText>
                </FormControl>
              )}
            />
          </Grid>
        </Grid>
        <Grid container spacing={1}>
          <Grid item xs={12} md={6} sx={{ my: 2 }}>
            <Controller
              control={control}
              name="registerDate"
              rules={{
                validate: validateRegisterDate,
              }}
              render={({ field: { onChange, value } }) => (
                <DatePicker
                  onChange={onChange}
                  disableFuture
                  label={t("createRegister.registerDate")}
                  value={value}
                  renderInput={(params) => (
                    <TextField
                      fullWidth
                      {...params}
                      error={!!errors?.registerDate}
                      helperText={errors?.registerDate && message.registerDate}
                    />
                  )}
                />
              )}
            />
          </Grid>
          <Grid item xs={12} md={6} sx={{ mt: 2 }}>
            <Controller
              control={control}
              name="processStartDate"
              rules={{
                validate: validateStartDate,
              }}
              render={({ field: { onChange, value } }) => (
                <DatePicker
                  onChange={onChange}
                  disableFuture
                  label={t("createRegister.procesStartDate")}
                  views={["year", "month", "day"]}
                  value={value}
                  renderInput={(params) => (
                    <TextField
                      fullWidth
                      {...params}
                      error={!!errors?.processStartDate}
                      helperText={
                        errors?.processStartDate && message.processStartDate
                      }
                    />
                  )}
                />
              )}
            />
          </Grid>
        </Grid>
        <Grid container spacing={1}>
          <Grid item xs={12} md={6}>
            <TextField
              margin="normal"
              fullWidth
              label={t("createRegister.numberCase")}
              {...register("numberCase", {
                validate: validationNumberCase,
              })}
              autoComplete="numberCase"
              error={!!errors?.numberCase}
              helperText={errors?.numberCase && numberCaseMessage}
            />
          </Grid>
          <Grid item xs={12} md={6} container justifyContent="center">
            <Controller
              name="oldCase"
              control={control}
              rules={{
                validate: validateOldCase,
              }}
              render={({ field: { onChange, value } }) => (
                <FormControl sx={{ mt: 2 }} error={!!errors?.oldCase}>
                  <FormLabel sx={{ textAlign: "center" }}>
                    {t("createRegister.caseType")}
                  </FormLabel>
                  <RadioGroup
                    row
                    value={value}
                    onChange={onChange}
                    sx={{ justifyContent: "center" }}
                  >
                    <FormControlLabel
                      value={true}
                      control={<Radio />}
                      label={t("createRegister.old")}
                    />
                    <FormControlLabel
                      value={false}
                      control={<Radio />}
                      label={t("createRegister.new")}
                    />
                  </RadioGroup>
                  <FormHelperText>
                    {errors?.oldCase && oldCaseMessage}
                  </FormHelperText>
                </FormControl>
              )}
            />
          </Grid>
        </Grid>
        <Grid container spacing={1}>
          <Grid item xs={12} md={6}>
            <TextField
              multiline
              margin="normal"
              fullWidth
              label={t("createRegister.observation")}
              {...register("observation", {
                validate: validateObservation,
              })}
              error={!!errors?.observation}
              autoComplete="observation"
              helperText={errors?.observation && observationMessage}
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <TextField
              multiline
              margin="normal"
              fullWidth
              label={t("createRegister.direction")}
              {...register("direction", {
                validate: validatedirection,
              })}
              error={!!errors?.direction}
              autoComplete="direction"
              helperText={errors?.direction && directionMessage}
            />
          </Grid>
        </Grid>
        <Grid container>
          <Grid item xs={12}>
            <TextField
              multiline
              margin="normal"
              fullWidth
              label={t("createRegister.description")}
              {...register("description", {
                validate: validateDescription,
              })}
              error={!!errors?.description}
              autoComplete="description"
              helperText={errors?.description && descriptionMessage}
            />
          </Grid>
        </Grid>
        <Grid container direction="column" sx={{ mt: 1 }}>
          <Typography variant="body2" color="text.secondary" sx={{ mt: 1 }}>
            {t("createRegister.descriptionTypeService")}
          </Typography>
          <Controller
            name="serviceType"
            control={control}
            render={({ field: { onChange, value } }) => (
              <FormControl>
                <RadioGroup row value={value} onChange={onChange}>
                  <Grid container spacing={1}>
                    {actionsSepdavi?.map((e) => (
                      <Grid item xs={12} md={4} sx={{ mt: 1 }} key={e.name}>
                        <SelectRol
                          id={e.id}
                          title={e.name}
                          description={e.description}
                          icon={e.icon}
                          setValue={setValue}
                          nameControl="serviceType"
                        >
                          <FormControlLabel
                            value={e.name}
                            control={<Radio />}
                          />
                        </SelectRol>
                      </Grid>
                    ))}
                  </Grid>
                </RadioGroup>
              </FormControl>
            )}
          />
        </Grid>
        <Grid container sx={{ mt: 1 }}>
          <Grid item xs={12}>
            <TextField
              multiline
              margin="normal"
              fullWidth
              label={t("createRegister.actionsSepdavi")}
              {...register("actionsSepdavi", {
                validate: validateActionsSepdavi,
              })}
              error={!!errors?.actionsSepdavi}
              helperText={errors.actionsSepdavi && actionsSepdaviMessage}
            />
          </Grid>
        </Grid>
        <Grid container>
          <Grid item xs={12} container justifyContent="center">
            <FormControl sx={{ mt: 2 }}>
              <FormLabel sx={{ textAlign: "center" }}>
                {t("createRegister.hasSentenceDescription")}
              </FormLabel>
              <Controller
                name="hasSentence"
                control={control}
                render={({ field: { onChange, value } }) => (
                  <RadioGroup
                    row
                    value={value}
                    onChange={onChange}
                    sx={{ justifyContent: "center" }}
                  >
                    <FormControlLabel
                      value={true}
                      control={<Radio />}
                      label={t("createRegister.hasSentence")}
                    />
                    <FormControlLabel
                      value={false}
                      control={<Radio />}
                      label={t("createRegister.hasNotSentence")}
                    />
                  </RadioGroup>
                )}
              />
            </FormControl>
          </Grid>
        </Grid>
        {sentence === "true" && (
          <Grid container spacing={1}>
            <Grid item xs={12} md={6}>
              <Controller
                control={control}
                name="sentenceDate"
                render={({ field: { onChange, value } }) => (
                  <DatePicker
                    disableFuture
                    label={t("createRegister.judgmentDate")}
                    views={["year", "month", "day"]}
                    value={value}
                    onChange={onChange}
                    renderInput={(params) => (
                      <TextField margin="normal" fullWidth {...params} />
                    )}
                  />
                )}
              />
            </Grid>
            <Grid item xs={12} md={6}>
              <TextField
                margin="normal"
                fullWidth
                label={t("createRegister.numberJudgment")}
                {...register("sentenceNumber", {
                  validate: validateSentenceNumber,
                })}
                autoComplete="sentenceNumber"
                error={!!errors?.sentenceNumber}
                helperText={errors.sentenceNumber && sentenceMessage}
              />
            </Grid>
          </Grid>
        )}

        <Grid item xs={12} sx={{ mt: 1 }}>
          <Typography variant="body2" color="text.secondary" sx={{ mt: 1 }}>
            {t("createRegister.typologyDescription")}
          </Typography>
        </Grid>
        <Grid container sx={{ mb: 4 }}>
          <Grid item xs={12} md={12}>
            <Controller
              control={control}
              name="crimes"
              rules={{
                validate: validationCrimes,
              }}
              render={({ field: { onChange, value } }) => (
                <Autocomplete
                  value={value}
                  onChange={(event, newValue) => {
                    onChange(newValue);
                  }}
                  options={crimes}
                  isOptionEqualToValue={(option, value) =>
                    option.id === value.id
                  }
                  renderInput={(params) =>
                    loaderCrimes ? (
                      <Skeleton variant="rounded" height={55} />
                    ) : (
                      <TextField
                        {...params}
                        fullWidth
                        margin="normal"
                        label={t("createRegister.principalTypology")}
                        error={!!errors?.crimes}
                        helperText={errors?.crimes && crimesMessage}
                      />
                    )
                  }
                />
              )}
            />
          </Grid>
        </Grid>
        <Grid container sx={{ mt: 2 }} justifyContent="center">
          <Grid item xs={12} md={3}>
            <Button fullWidth variant="contained" color="primary" type="submit">
              Siguiente
            </Button>
          </Grid>
        </Grid>
      </form>
    </Grid>
  );
};

export default GeneralInformation;
