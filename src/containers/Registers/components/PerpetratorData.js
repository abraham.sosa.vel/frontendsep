import {
  Autocomplete,
  Button,
  FormControl,
  FormControlLabel,
  FormHelperText,
  FormLabel,
  Grid,
  InputLabel,
  MenuItem,
  Radio,
  RadioGroup,
  Select,
  Skeleton,
  TextField,
  Typography,
} from "@mui/material";
import { DatePicker } from "@mui/x-date-pickers";
import React, { useEffect, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import BaseModalConfirm from "../../../components/BaseModalConfirm";
import { useModal } from "../../../utils/useModal";
import { sendPerpetratorInformation } from "../redux/redux";
import {
  getAllInstructionDegreeStart,
  getAllOccupationStart,
  getAllSexualOrientationsStart,
} from "../redux/thunk";
import useValidationsPerpetrator from "../utils/perpetratorValidation";

const PerpetratorData = ({ handleBack, clickSubmit }) => {
  const { perpetratorInformation } = useSelector((state) => state.register);

  const [defaultValues, setDefaultValues] = useState({
    sex: "",
    typeDocument: "",
    sexualOrientation: "",
    occupation: "",
    instructionDegree: "",
    birthday: null,
  });
  const {
    register,
    control,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm({
    mode: "onChange",
    defaultValues,
  });
  const [statusModal, handleCloseModal, handleOpenModal] = useModal();
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getAllOccupationStart());
    dispatch(getAllSexualOrientationsStart());
    dispatch(getAllInstructionDegreeStart());
  }, [dispatch]);
  const {
    sexualOrientations,
    occupations,
    instructionDegree,
    loaderOccupation,
    loaderDegree,
  } = useSelector((state) => state.register);
  const [t] = useTranslation("global");
  useEffect(() => {
    if (Object.keys(perpetratorInformation).length === 0) return;
    setDefaultValues(perpetratorInformation);
  }, [perpetratorInformation]);
  useEffect(() => {
    reset(defaultValues);
  }, [defaultValues, reset, perpetratorInformation]);
  const {
    validateBirthday,
    validationName,
    validationLastName,
    message,
    validationSex,
    validationTypeDocument,
    validationNumberDocument,
    validationSexualOrientation,
    validationNacionality,
    validationDirection,
    validationPhoneNumber,
    validationObservation,
    validationOccupation,
    validationInstructionDegree,
  } = useValidationsPerpetrator();
  const onSubmit = (data, e) => {
    e.preventDefault();
    if (Object.keys(perpetratorInformation).length === 0) {
      data.birthday = data.birthday.$d.toISOString();
    }
    dispatch(sendPerpetratorInformation(data));
    handleOpenModal();
  };
  const confirmModal = () => {
    handleCloseModal();
    clickSubmit();
  };
  return (
    <Grid container direction="column">
      <BaseModalConfirm
        title="¿Esta seguro que desea crear el registro del caso?"
        status={statusModal}
        handleClose={handleCloseModal}
        confirmModal={confirmModal}
      />
      <Grid container alignItems="center" direction="column">
        <Typography
          variant="h5"
          component="h2"
          align="center"
          sx={{ color: "text.TextCuston" }}
        >
          {t("stepperRegister.perpetratorInformation").toUpperCase()}
        </Typography>
        <Typography
          variant="subtitle2"
          component="h2"
          align="center"
          sx={{ color: "text.TextCuston", mt: 1, mb: 2, opacity: 1 }}
        >
          {t("stepperRegister.descriptionPerpetratorInformation")}
        </Typography>
      </Grid>
      <form onSubmit={handleSubmit(onSubmit)}>
        {/*Name and lastName */}
        <Grid container spacing={2}>
          <Grid item xs={12} md={6}>
            <TextField
              label={t("createPerpetrator.name")}
              variant="outlined"
              margin="normal"
              fullWidth
              {...register("name", {
                validate: validationName,
              })}
              type="text"
              autoComplete="name"
              error={!!errors?.name}
              helperText={errors.name && message.name}
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <TextField
              fullWidth
              label={t("createPerpetrator.lastName")}
              {...register("lastName", {
                validate: validationLastName,
              })}
              variant="outlined"
              margin="normal"
              autoComplete="lastName"
              error={!!errors?.lastName}
              helperText={errors.lastName && message.lastName}
            />
          </Grid>
        </Grid>
        {/*birthday and sex */}
        <Grid container spacing={1}>
          <Grid item xs={12} md={6} sx={{ mt: 2 }}>
            <Controller
              control={control}
              name="birthday"
              rules={{
                validate: validateBirthday,
              }}
              render={({ field: { onChange, value } }) => (
                <DatePicker
                  disableFuture
                  label={t("createPerpetrator.birthday")}
                  views={["year", "month", "day"]}
                  value={value}
                  onChange={onChange}
                  renderInput={(params) => (
                    <TextField
                      fullWidth
                      {...params}
                      error={!!errors?.birthday}
                      helperText={errors?.birthday && message.birthday}
                    />
                  )}
                />
              )}
            />
          </Grid>
          <Grid item xs={12} md={6} container justifyContent="center">
            <Controller
              name="sex"
              control={control}
              rules={{
                validate: validationSex,
              }}
              render={({ field: { onChange, value } }) => (
                <FormControl sx={{ mt: 2 }} error={!!errors?.sex}>
                  <FormLabel sx={{ textAlign: "center" }}>
                    {t("createPerpetrator.sex")}
                  </FormLabel>
                  <RadioGroup
                    row
                    value={value}
                    onChange={onChange}
                    sx={{ justifyContent: "center" }}
                  >
                    <FormControlLabel
                      value={true}
                      control={<Radio />}
                      label={t("global.male")}
                    />
                    <FormControlLabel
                      value={false}
                      control={<Radio />}
                      label={t("global.female")}
                    />
                  </RadioGroup>
                  <FormHelperText sx={{ textAlign: "center" }}>
                    {errors?.sex && message.sex}
                  </FormHelperText>
                </FormControl>
              )}
            />
          </Grid>
        </Grid>
        {/*document and type */}
        <Grid container spacing={2}>
          <Grid item xs={12} md={6}>
            <Controller
              control={control}
              name="typeDocument"
              rules={{
                validate: validationTypeDocument,
              }}
              render={({ field: { onChange, value } }) => (
                <FormControl
                  fullWidth
                  margin="normal"
                  error={!!errors?.typeDocument}
                >
                  <InputLabel>{t("createPerpetrator.typeDocument")}</InputLabel>
                  <Select
                    fullWidth
                    onChange={onChange}
                    value={value}
                    label={t("createPerpetrator.typeDocument")}
                  >
                    <MenuItem value={"Carnet de identidad"}>
                      Carnet de identidad
                    </MenuItem>
                    <MenuItem value={"Pasaporte"}>Pasaporte</MenuItem>
                    <MenuItem value={"Libreta militar"}>
                      Libreta militar
                    </MenuItem>
                    <MenuItem value={"Licencia de conducir"}>
                      Licencia de conducir
                    </MenuItem>
                  </Select>
                  <FormHelperText>
                    {errors?.typeDocument && message.documentType}
                  </FormHelperText>
                </FormControl>
              )}
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <TextField
              label={t("createPerpetrator.numberDocument")}
              variant="outlined"
              margin="normal"
              fullWidth
              {...register("numberDocument", {
                validate: validationNumberDocument,
              })}
              type="text"
              autoComplete="numberDocument"
              error={!!errors?.numberDocument}
              helperText={errors?.numberDocument && message.numberDocument}
            />
          </Grid>
        </Grid>
        {/*orientationSexual and nacionaity */}
        <Grid container spacing={2}>
          <Grid item xs={12} md={6}>
            <Controller
              control={control}
              name="sexualOrientation"
              rules={{
                validate: validationSexualOrientation,
              }}
              render={({ field: { onChange, value } }) => (
                <FormControl
                  fullWidth
                  margin="normal"
                  error={!!errors?.sexualOrientation}
                >
                  <InputLabel>
                    {t("createPerpetrator.sexualOrientation")}
                  </InputLabel>
                  <Select
                    fullWidth
                    onChange={onChange}
                    value={value}
                    label={t("createPerpetrator.sexualOrientation")}
                  >
                    {sexualOrientations.map((e) => (
                      <MenuItem key={e.id} value={e.id}>
                        {e.nombre}
                      </MenuItem>
                    ))}
                  </Select>
                  <FormHelperText>
                    {errors?.sexualOrientation && message.sexualOrientation}
                  </FormHelperText>
                </FormControl>
              )}
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <TextField
              label={t("createPerpetrator.nacionality")}
              variant="outlined"
              margin="normal"
              fullWidth
              {...register("nacionality", {
                validate: validationNacionality,
              })}
              type="text"
              autoComplete="nacionality"
              error={!!errors?.nacionality}
              helperText={errors.nacionality && message.nacionality}
            />
          </Grid>
        </Grid>
        {/*diretion and phone */}
        <Grid container spacing={2}>
          <Grid item xs={12} md={6}>
            <TextField
              multiline
              label={t("createPerpetrator.direction")}
              variant="outlined"
              margin="normal"
              fullWidth
              {...register("direction", {
                validate: validationDirection,
              })}
              type="text"
              autoComplete="direction"
              error={!!errors?.direction}
              helperText={errors?.direction && message.direction}
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <TextField
              fullWidth
              label={t("createPerpetrator.phone")}
              {...register("phoneNumber", {
                validate: validationPhoneNumber,
              })}
              type="phone"
              variant="outlined"
              margin="normal"
              autoComplete="phoneNumber"
              error={!!errors?.phoneNumber}
              helperText={errors.phoneNumber && message.phoneNumber}
            />
          </Grid>
        </Grid>
        {/*observation */}
        <Grid container spacing={2}>
          <Grid item xs={12} container spacing={1}>
            <Grid item xs={12}>
              <TextField
                multiline
                label={t("createPerpetrator.observation")}
                variant="outlined"
                margin="normal"
                fullWidth
                {...register("observation", {
                  validate: validationObservation,
                })}
                type="text"
                autoComplete="observation"
                error={!!errors?.observation}
                helperText={errors.observation && message.observation}
              />
            </Grid>
          </Grid>
        </Grid>
        {/*ocupation and grade instruction */}
        <Grid container spacing={2}>
          <Grid item xs={12} md={6}>
            <Controller
              control={control}
              name="occupation"
              rules={{
                validate: validationOccupation,
              }}
              render={({ field: { onChange, value } }) =>
                loaderOccupation ? (
                  <Skeleton variant="rounded" height={55} />
                ) : (
                  <Autocomplete
                    value={value}
                    onChange={(event, newValue) => {
                      onChange(newValue);
                    }}
                    options={occupations}
                    isOptionEqualToValue={(option, value) =>
                      option.id === value.id
                    }
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        fullWidth
                        margin="normal"
                        label={t("createPerpetrator.ocupation")}
                        error={!!errors?.occupation}
                        helperText={errors?.occupation && message.occupation}
                      />
                    )}
                  />
                )
              }
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <Controller
              control={control}
              name="instructionDegree"
              rules={{
                validate: validationInstructionDegree,
              }}
              render={({ field: { onChange, value } }) =>
                loaderDegree ? (
                  <Skeleton variant="rounded" height={55} />
                ) : (
                  <Autocomplete
                    value={value}
                    onChange={(event, newValue) => {
                      onChange(newValue);
                    }}
                    options={instructionDegree}
                    isOptionEqualToValue={(option, value) =>
                      option.id === value.id
                    }
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        fullWidth
                        margin="normal"
                        label={t("createPerpetrator.instructionDegree")}
                        error={!!errors?.instructionDegree}
                        helperText={
                          errors?.instructionDegree && message.instructionDegree
                        }
                      />
                    )}
                  />
                )
              }
            />
          </Grid>
        </Grid>
        <Grid container sx={{ mt: 2 }} justifyContent="center" spacing={2}>
          <Grid item xs={12} md={3}>
            <Button
              fullWidth
              variant="contained"
              color="primary"
              onClick={(e) => {
                handleBack();
              }}
            >
              Anterior
            </Button>
          </Grid>
          <Grid item xs={12} md={3}>
            <Button type="submit" fullWidth variant="contained" color="primary">
              Registrar
            </Button>
          </Grid>
        </Grid>
      </form>
    </Grid>
  );
};

export default PerpetratorData;
