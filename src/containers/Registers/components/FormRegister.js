import { Grid, Paper, Step, StepLabel, Stepper, useTheme } from "@mui/material";
import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router";
import { setValuesForm } from "../redux/redux";
import { createRegisterStart } from "../redux/thunk";
import { registerFormatData } from "../utils/formatRegister";
import ComplainantData from "./ComplainantData";
import GeneralInformation from "./GeneralInformation";
import PerpetratorData from "./PerpetratorData";
import VictimData from "./VictimData";
const FormRegister = () => {
  const [t] = useTranslation("global");
  const [activeStep, setActiveStep] = useState(0);
  const dispatch = useDispatch();
  const theme = useTheme();
  const navigate = useNavigate();
  const steps = [
    t("stepperRegister.generalInformation"),
    t("stepperRegister.victimInformation"),
    t("stepperRegister.complainantInformation"),
    t("stepperRegister.perpetratorInformation"),
  ];
  const {
    generalInformation,
    victimInformation,
    complainantInformation,
    perpetratorInformation,
  } = useSelector((state) => state.register);
  const renderStepContent = (step, handleNext, handleBack, clickSubmit) => {
    switch (step) {
      case 0:
        return <GeneralInformation handleNext={handleNext} />;
      case 1:
        return <VictimData handleNext={handleNext} handleBack={handleBack} />;
      case 2:
        return (
          <ComplainantData handleNext={handleNext} handleBack={handleBack} />
        );
      case 3:
        return (
          <PerpetratorData handleBack={handleBack} clickSubmit={clickSubmit} />
        );
      default:
        return <div>Not Found</div>;
    }
  };
  const handleNext = () => {
    setActiveStep(activeStep + 1);
  };
  const handleBack = () => {
    setActiveStep(activeStep - 1);
  };
  const clickSubmit = () => {
    new Promise((resolve, reject) => {
      dispatch(
        createRegisterStart({
          data: registerFormatData(
            generalInformation,
            victimInformation,
            complainantInformation,
            perpetratorInformation
          ),
          resolve,
          reject,
        })
      );
    })
      .then((e) => {
        navigate("/registers");
        dispatch(setValuesForm());
      })
      .catch((e) => {});
  };

  return (
    <Paper
      sx={{
        p: 2,
        pt: 3,
        overflow: "auto",
        "&::-webkit-scrollbar": {
          width: "0.60em",
          height: "0.35em",
        },
        "&::-webkit-scrollbar-track": {
          "-webkit-box-shadow": "inset 0 0 6px rgba(0,0,0,0.00)",
        },
        "&::-webkit-scrollbar-thumb": {
          backgroundColor:
            theme.palette.mode === "dark" ? "#9D9D9D" : "#AFAFAF",
          borderRadius: 5,
        },
      }}
      elevation={theme.palette.mode === "dark" ? 0 : 5}
    >
      <Stepper activeStep={activeStep} alternativeLabel>
        {steps.map((label) => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
      <Grid container justifyContent="center" sx={{ my: 4 }}>
        <Grid item xs={12} md={8}>
          {renderStepContent(activeStep, handleNext, handleBack, clickSubmit)}
          <Grid
            container
            sx={{ mt: 2 }}
            justifyContent="space-evenly"
            spacing={2}
          ></Grid>
        </Grid>
      </Grid>
    </Paper>
  );
};

export default FormRegister;
