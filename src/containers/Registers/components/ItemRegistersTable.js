import { Delete, Edit } from "@mui/icons-material";
import {
  Box,
  Divider,
  Grid,
  ListItemIcon,
  ListItemText,
  MenuItem,
  TableCell,
  Typography,
  useTheme,
} from "@mui/material";
import { blue, red } from "@mui/material/colors";
import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import MenuActions from "../../../components/MenuActions.js/index.js";

const ItemRegistersTable = ({
  item = {},
  handleEditUser,
  handleDeleteUser,
}) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const theme = useTheme();
  const [t] = useTranslation("global");
  const handleEdit = () => {
    setAnchorEl(false);
    handleEditUser(item.id);
  };
  const handleDelete = () => {
    setAnchorEl(false);
    handleDeleteUser(item.id);
  };
  return (
    <>
      <TableCell sx={{ width: 50 }}>
        <MenuActions anchorEl={anchorEl} setAnchorEl={setAnchorEl}>
          <Box>
            <MenuItem onClick={handleEdit}>
              <ListItemIcon>
                <Edit />
              </ListItemIcon>
              <ListItemText primary={t("global.edit")} />
            </MenuItem>
            <MenuItem onClick={handleDelete}>
              <ListItemIcon>
                <Delete />
              </ListItemIcon>
              <ListItemText primary={t("global.delete")} />
            </MenuItem>
          </Box>
        </MenuActions>
      </TableCell>
      <TableCell>{item.id}</TableCell>
      <TableCell>
        {item.crime.length === 0
          ? "No se registro delito"
          : item.crime.map((e, i) => (
              <Grid key={i}>
                {i !== 0 && <Divider />}
                <Grid sx={{ my: 1 }}>{e}</Grid>
              </Grid>
            ))}
      </TableCell>
      <TableCell sx={{ textAlign: "center" }}>
        {new Date(item.registerDate).toLocaleDateString()}
      </TableCell>
      <TableCell sx={{ textAlign: "center" }}>
        {item.victimName.length === 0
          ? "No se registro una victima"
          : item.victimName.map((e, i) => (
              <Grid key={i}>
                {i !== 0 && <Divider />}
                <Typography sx={{ my: 1 }} variant="subtitle2">
                  {e}
                </Typography>
              </Grid>
            ))}
      </TableCell>
      <TableCell sx={{ textAlign: "center" }}>
        {item.victimLastName.length === 0
          ? "No se registro una victima"
          : item.victimLastName.map((e, i) => (
              <Grid key={i}>
                {i !== 0 && <Divider />}
                <Typography sx={{ my: 1 }} variant="subtitle2">
                  {e}
                </Typography>
              </Grid>
            ))}
      </TableCell>
      <TableCell>
        {item.denouncedName.length === 0
          ? "No se registro datos del victimario"
          : item.denouncedName.map((e, i) => (
              <Grid key={i}>
                {i !== 0 && <Divider />}
                <Typography sx={{ my: 1 }} variant="subtitle2">
                  {e}
                </Typography>
              </Grid>
            ))}
      </TableCell>
      <TableCell>
        {item.denouncedLastName.length === 0
          ? "No se registro datos del victimario"
          : item.denouncedLastName.map((e, i) => (
              <Grid key={i}>
                {i !== 0 && <Divider />}
                <Typography sx={{ my: 1 }} variant="subtitle2">
                  {e}
                </Typography>
              </Grid>
            ))}
      </TableCell>
      <TableCell>{item?.numberCase ? item?.numberCase : "---------"}</TableCell>
      <TableCell>{item.actionsSepdavi}</TableCell>
      <TableCell>
        {item.proceduralStage === "Sin etapa" ? (
          <Box
            sx={{
              bgcolor: theme.palette.mode === "dark" ? red[800] : red[500],
              borderRadius: 6,
              py: ".5rem",
              px: ".5rem",
            }}
          >
            <Typography align="center" sx={{ color: "#ffff" }}>
              {item.proceduralStage}
            </Typography>
          </Box>
        ) : (
          <Box
            sx={{
              bgcolor: theme.palette.mode === "dark" ? blue[800] : blue[500],
              borderRadius: 6,
              py: ".5rem",
              px: ".5rem",
            }}
          >
            <Typography align="center" sx={{ color: "#ffff" }}>
              {item.proceduralStage}
            </Typography>
          </Box>
        )}
      </TableCell>
    </>
  );
};

export default ItemRegistersTable;
