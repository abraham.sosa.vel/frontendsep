import { createSlice } from "@reduxjs/toolkit";
const initialState = {
  registers: [],
  loaderRegisters: false,
  cities: [],
  loaderCities: false,
  crimes: [],
  otherCrimes: [],
  loaderCrimes: false,
  generalInformation: {},
  victimInformation: {},
  complainantInformation: {},
  perpetratorInformation: {},
  sexualOrientations: [],
  loaderOrientation: false,
  occupations: [],
  otherOccupation: [],
  loaderOccupation: false,
  instructionDegree: [],
  otherInstructionDegree: [],
  loaderDegree: false,
  isEditing: false,
};
export const registerSlice = createSlice({
  name: "registerSlice",
  initialState,
  reducers: {
    getAllRegistersSuccess: (state, action) => {
      state.registers = action.payload;
    },
    showLoaderRegisters: (state, action) => {
      state.loaderRegisters = true;
    },
    hideLoaderRegisters: (state, action) => {
      state.loaderRegisters = false;
    },
    getAllCitiesSuccess: (state, action) => {
      state.cities = action.payload;
    },
    showLoaderCities: (state, action) => {
      state.loaderCities = true;
    },
    hideLoaderCities: (state, action) => {
      state.loaderCities = false;
    },
    getAllCrimesSuccess: (state, action) => {
      state.otherCrimes = action.payload;
      state.crimes = action.payload.map((e) => (e.label = e.nombre));
    },
    showLoaderCrimes: (state, action) => {
      state.loaderCrimes = true;
    },
    hideLoaderCrimes: (state, action) => {
      state.loaderCrimes = false;
    },
    sendGeneralInformation: (state, action) => {
      state.generalInformation = action.payload;
    },
    sendVictimInformation: (state, action) => {
      state.victimInformation = action.payload;
    },
    sendPerpetratorInformation: (state, action) => {
      state.perpetratorInformation = action.payload;
    },
    sendComplainantInformation: (state, action) => {
      state.complainantInformation = action.payload;
    },
    getAllSexualOrientationsSuccess: (state, action) => {
      state.sexualOrientations = action.payload;
    },
    getAllOccupationSuccess: (state, action) => {
      state.otherOccupation = action.payload;
      state.occupations = action.payload.map((e) => (e.label = e.nombre));
    },
    getAllInstructionDegreeSuccess: (state, action) => {
      state.otherInstructionDegree = action.payload;
      state.instructionDegree = action.payload.map((e) => (e.label = e.nombre));
    },
    showLoaderOccupation: (state) => {
      state.loaderOccupation = true;
    },
    hideLoaderOccupation: (state) => {
      state.loaderOccupation = false;
    },
    showLoaderDegree: (state) => {
      state.loaderDegree = true;
    },
    hideLoaderDegree: (state) => {
      state.loaderDegree = false;
    },
    showLoaderOrientation: (state) => {
      state.loaderOrientation = true;
    },
    hideLoaderOrientation: (state) => {
      state.loaderOrientation = false;
    },
    setValuesForm: (state) => {
      state.generalInformation = {};
      state.victimInformation = {};
      state.complainantInformation = {};
      state.perpetratorInformation = {};
    },
    getForUpdateSucces: (state, action) => {
      state.generalInformation = {
        actionsSepdavi: action.payload.records.accionesSepdavi,
        city: action.payload.records.ciudadId,
        crimes: action.payload.records.registroDelitos[0].delito.nombre,
        description: action.payload.records.descripcion,
        direction: action.payload.records.direccion,
        hasSentence: action.payload.records.tieneSentencia,
        numberCase: action.payload.records.numeroCaso,
        observation: action.payload.records.observacion,
        oldCase: action.payload.records.casoAntiguo,
        processStartDate: action.payload.records.fechaInicioProceso,
        registerDate: action.payload.records.fechaRegistro,
        serviceType: action.payload.records.tipoAsistencia,
        state: action.payload.records.area,
      };
      state.victimInformation = {
        birthday:
          action.payload.records.informacionVictimas[0].persona.fechaNacimiento,
        direction:
          action.payload.records.informacionVictimas[0].persona.direccion,
        instructionDegree:
          action.payload.records.informacionVictimas[0].persona
            .gradoInstruccione.nombre,
        lastName:
          action.payload.records.informacionVictimas[0].persona.apellido,
        nacionality:
          action.payload.records.informacionVictimas[0].persona.nacionalidad,
        name: action.payload.records.informacionVictimas[0].persona.nombre,
        numberDocument:
          action.payload.records.informacionVictimas[0].persona.numeroDocumento,
        observation:
          action.payload.records.informacionVictimas[0].persona.observacion,
        occupation:
          action.payload.records.informacionVictimas[0].persona.ocupacione
            .nombre,
        phoneNumber:
          action.payload.records.informacionVictimas[0].persona.telefono,
        sex: action.payload.records.informacionVictimas[0].persona.sexo,
        sexualOrientation:
          action.payload.records.informacionVictimas[0].persona
            .orientacionesSexuale.id,
        typeDocument:
          action.payload.records.informacionVictimas[0].persona.typoDocumento,
      };
      state.complainantInformation = {
        birthday:
          action.payload.records.informacionDenunciados[0].persona
            .fechaNacimiento,
        direction:
          action.payload.records.informacionDenunciados[0].persona.direccion,
        instructionDegree:
          action.payload.records.informacionDenunciados[0].persona
            .gradoInstruccione.nombre,
        lastName:
          action.payload.records.informacionDenunciados[0].persona.apellido,
        nacionality:
          action.payload.records.informacionDenunciados[0].persona.nacionalidad,
        name: action.payload.records.informacionDenunciados[0].persona.nombre,
        numberDocument:
          action.payload.records.informacionDenunciados[0].persona
            .numeroDocumento,
        observation:
          action.payload.records.informacionDenunciados[0].persona.observacion,
        occupation:
          action.payload.records.informacionDenunciados[0].persona.ocupacione
            .nombre,
        phoneNumber:
          action.payload.records.informacionDenunciados[0].persona.telefono,
        sex: action.payload.records.informacionDenunciados[0].persona.sexo,
        sexualOrientation:
          action.payload.records.informacionDenunciados[0].persona
            .orientacionesSexuale.id,
        typeDocument:
          action.payload.records.informacionDenunciados[0].persona
            .typoDocumento,
      };
      state.perpetratorInformation = {
        birthday:
          action.payload.records.informacionDenunciantes[0].persona
            .fechaNacimiento,
        direction:
          action.payload.records.informacionDenunciantes[0].persona.direccion,
        instructionDegree:
          action.payload.records.informacionDenunciantes[0].persona
            .gradoInstruccione.nombre,
        lastName:
          action.payload.records.informacionDenunciantes[0].persona.apellido,
        nacionality:
          action.payload.records.informacionDenunciantes[0].persona
            .nacionalidad,
        name: action.payload.records.informacionDenunciantes[0].persona.nombre,
        numberDocument:
          action.payload.records.informacionDenunciantes[0].persona
            .numeroDocumento,
        observation:
          action.payload.records.informacionDenunciantes[0].persona.observacion,
        occupation:
          action.payload.records.informacionDenunciantes[0].persona.ocupacione
            .nombre,
        phoneNumber:
          action.payload.records.informacionDenunciantes[0].persona.telefono,
        sex: action.payload.records.informacionDenunciantes[0].persona.sexo,
        sexualOrientation:
          action.payload.records.informacionDenunciantes[0].persona
            .orientacionesSexuale.id,
        typeDocument:
          action.payload.records.informacionDenunciantes[0].persona
            .typoDocumento,
      };
    },
    isEditingSuccess: (state, action) => {
      state.isEditing = action.payload;
    },
  },
});

export const {
  isEditingSuccess,
  getForUpdateSucces,
  getAllRegistersSuccess,
  showLoaderRegisters,
  hideLoaderRegisters,
  setValuesForm,
  sendPerpetratorInformation,
  sendComplainantInformation,
  sendVictimInformation,
  showLoaderOccupation,
  hideLoaderOccupation,
  showLoaderDegree,
  hideLoaderDegree,
  showLoaderOrientation,
  hideLoaderOrientation,
  getAllSexualOrientationsSuccess,
  getAllOccupationSuccess,
  getAllInstructionDegreeSuccess,
  getAllCitiesSuccess,
  showLoaderCities,
  hideLoaderCities,
  getAllCrimesSuccess,
  showLoaderCrimes,
  hideLoaderCrimes,
  sendGeneralInformation,
} = registerSlice.actions;
export default registerSlice.reducer;
