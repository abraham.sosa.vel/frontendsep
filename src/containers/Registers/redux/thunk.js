import {
  hideGlobalLoader,
  messageError,
  messageSuccess,
  showGlobalLoader,
} from "../../../redux/globalRedux";
import request, {
  deleteOptionWithToken,
  getOptionWithoutToken,
  getOptionWithToken,
  postOptionWithToken,
} from "../../../utils/request";
import {
  getAllCitiesSuccess,
  getAllCrimesSuccess,
  getAllInstructionDegreeSuccess,
  getAllOccupationSuccess,
  getAllRegistersSuccess,
  getAllSexualOrientationsSuccess,
  getForUpdateSucces,
  hideLoaderCities,
  hideLoaderCrimes,
  hideLoaderDegree,
  hideLoaderOccupation,
  hideLoaderOrientation,
  hideLoaderRegisters,
  isEditingSuccess,
  showLoaderCities,
  showLoaderCrimes,
  showLoaderDegree,
  showLoaderOccupation,
  showLoaderOrientation,
  showLoaderRegisters,
} from "./redux";

export const getAllRegistersStart = () => {
  return async (dispatch, getState) => {
    try {
      const { auth } = getState().auth;
      dispatch(showLoaderRegisters());
      let url = `${process.env.REACT_APP_URL_API}/register/${auth.user.id}`;
      let options = getOptionWithToken();
      let registers = await request(url, options);
      dispatch(getAllRegistersSuccess(registers));
    } catch (error) {
      let message = error?.message
        ? error.message
        : "No se pudo obtener los registros";
      dispatch(messageError(message));
    } finally {
      dispatch(hideLoaderRegisters());
    }
  };
};
export const searchAllRegistersStart = (search) => {
  return async (dispatch, getState) => {
    try {
      const { auth } = getState().auth;
      dispatch(showLoaderRegisters());
      let url = `${process.env.REACT_APP_URL_API}/register/search/${auth.user.id}`;
      let options = postOptionWithToken({ name: search });
      let registers = await request(url, options);
      dispatch(getAllRegistersSuccess(registers));
    } catch (error) {
      let message = error?.message
        ? error.message
        : "No se pudo obtener los registros";
      dispatch(messageError(message));
    } finally {
      dispatch(hideLoaderRegisters());
    }
  };
};
export const getAllCitiesStart = () => {
  return async (dispatch) => {
    try {
      dispatch(showLoaderCities());
      let url = `${process.env.REACT_APP_URL_API}/cities`;
      let options = getOptionWithoutToken();
      let cities = await request(url, options);
      dispatch(getAllCitiesSuccess(cities));
    } catch (error) {
      let message = error?.message
        ? error.message
        : "No se pudo cargar los datos de las ciudades";
      dispatch(messageError(message));
    } finally {
      dispatch(hideLoaderCities());
    }
  };
};
export const getAllCrimesStart = () => {
  return async (dispatch) => {
    try {
      dispatch(showLoaderCrimes());
      let url = `${process.env.REACT_APP_URL_API}/crimes`;
      let options = getOptionWithoutToken();
      let crimes = await request(url, options);
      dispatch(getAllCrimesSuccess(crimes));
    } catch (error) {
      let message = error?.message
        ? error.message
        : "No se pudo cargar los datos de los delitos";
      dispatch(messageError(message));
    } finally {
      dispatch(hideLoaderCrimes());
    }
  };
};
//orientacion sexual
export const getAllSexualOrientationsStart = () => {
  return async (dispatch, getState) => {
    const { sexualOrientations } = getState().register;
    try {
      dispatch(showLoaderOrientation());
      if (sexualOrientations.length > 0) return;
      let url = `${process.env.REACT_APP_URL_API}/sexualOrientation`;
      let options = getOptionWithoutToken();
      let sexualOrientation = await request(url, options);
      dispatch(getAllSexualOrientationsSuccess(sexualOrientation));
    } catch (error) {
      let message = error?.message
        ? error.message
        : "No se pudo cargar los datos de las orientaciones sexuales";
      dispatch(messageError(message));
    } finally {
      dispatch(hideLoaderOrientation());
    }
  };
};
//ocupacion
export const getAllOccupationStart = () => {
  return async (dispatch, getState) => {
    const { occupations } = getState().register;
    try {
      dispatch(showLoaderOccupation());
      if (occupations.length > 0) return;
      let url = `${process.env.REACT_APP_URL_API}/occupation`;
      let options = getOptionWithoutToken();
      let occupationsDegree = await request(url, options);
      dispatch(getAllOccupationSuccess(occupationsDegree));
    } catch (error) {
      let message = error?.message
        ? error.message
        : "No se pudo cargar los datos de las ocupaciones";
      dispatch(messageError(message));
    } finally {
      dispatch(hideLoaderOccupation());
    }
  };
};
//grado de instruccion
export const getAllInstructionDegreeStart = () => {
  return async (dispatch, getState) => {
    const { instructionDegree } = getState().register;
    try {
      dispatch(showLoaderDegree());
      if (instructionDegree.length > 0) return;
      let url = `${process.env.REACT_APP_URL_API}/instructionDegree`;
      let options = getOptionWithoutToken();
      let instructions = await request(url, options);
      dispatch(getAllInstructionDegreeSuccess(instructions));
    } catch (error) {
      let message = error?.message
        ? error.message
        : "No se pudo cargar los datos de los grados de instrucción";
      dispatch(messageError(message));
    } finally {
      dispatch(hideLoaderDegree());
    }
  };
};

//createRegister
export const createRegisterStart = ({ data, resolve, reject }) => {
  return async (dispatch, getState) => {
    try {
      const { registers, isEditing } = getState().register;
      dispatch(showGlobalLoader());
      let url = `${process.env.REACT_APP_URL_API}/register`;
      let options = postOptionWithToken(data);
      let createRegister = await request(url, options);
      resolve();
      dispatch(
        messageSuccess(
          isEditing
            ? "Se actualizo el registro con éxito"
            : "Se creo el registro con éxito"
        )
      );
      if (isEditing) {
        dispatch(getAllRegistersSuccess(registers));
      } else {
        dispatch(getAllRegistersSuccess([createRegister, ...registers]));
      }
    } catch (error) {
      let message = error?.message
        ? error.message
        : "No se pudo crear el registro del caso";
      dispatch(messageError(message));
      reject();
    } finally {
      dispatch(hideGlobalLoader());
    }
  };
};
//createRegister
export const deleteRegisterStart = (id) => {
  return async (dispatch, getState) => {
    try {
      const { registers } = getState().register;

      dispatch(showGlobalLoader());
      let url = `${process.env.REACT_APP_URL_API}/register/${id}`;
      let options = deleteOptionWithToken();
      let createRegister = await request(url, options);
      dispatch(getAllRegistersSuccess(registers.filter((e) => e.id !== id)));
      dispatch(messageSuccess("Se elimino el registro con exito"));
    } catch (error) {
      let message = error?.message
        ? error.message
        : "No se pudo eliminar el registro";
      dispatch(messageError(message));
    } finally {
      dispatch(hideGlobalLoader());
    }
  };
};

export const getForUpdateRegister = (id) => {
  return async (dispatch, getState) => {
    try {
      dispatch(showGlobalLoader());
      let url = `${process.env.REACT_APP_URL_API}/utils/register/${id}`;

      let options = getOptionWithToken();
      let Register = await request(url, options);
      dispatch(getForUpdateSucces(Register));
      dispatch(isEditingSuccess(Register.records.id));
    } catch (error) {
      let message = error?.message
        ? error.message
        : "No se pudo eliminar el registro";
      dispatch(messageError(message));
    } finally {
      dispatch(hideGlobalLoader());
    }
  };
};
