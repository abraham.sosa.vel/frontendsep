import { useTranslation } from "react-i18next";
import createColumns from "../../../components/BaseTable/createColumns";

export const useColumsTracing = () => {
  const [t] = useTranslation("global");
  return [
    createColumns({ id: "id", title: t("tracingRegister.id") }),
    createColumns({
      id: "registerDate",
      title: t("tracingRegister.registerDate"),
    }),
    createColumns({
      id: "initialActions",
      title: t("tracingRegister.initialActions"),
    }),
    createColumns({
      id: "preparatory",
      title: t("tracingRegister.preparatory"),
    }),
    createColumns({
      id: "oralTrial",
      title: t("tracingRegister.oralTrial"),
    }),
    createColumns({
      id: "means",
      title: t("tracingRegister.means"),
    }),
    createColumns({
      id: "performance",
      title: t("tracingRegister.performance"),
    }),
  ];
};
