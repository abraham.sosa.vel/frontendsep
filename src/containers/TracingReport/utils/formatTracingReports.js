export const getAllTracingFormat = (data) => {
  const formate = data.map((e) => {
    const item = {
      id: e.id,
      register: e.fechaRegistro,
    };
    item.initialActions = e?.etapaCasos?.filter((d) => d.etapaProcesalId === 1);
    item.preparatory = e?.etapaCasos?.filter((d) => d.etapaProcesalId === 2);
    item.oralTrial = e?.etapaCasos?.filter((d) => d.etapaProcesalId === 3);
    item.means = e?.etapaCasos?.filter((d) => d.etapaProcesalId === 4);
    item.performance = e?.etapaCasos?.filter((d) => d.etapaProcesalId === 5);
    return item;
  });
  return formate;
};
