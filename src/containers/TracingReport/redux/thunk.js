import {
  hideGlobalLoader,
  messageError,
  messageSuccess,
  showGlobalLoader,
} from "../../../redux/globalRedux";
import request, {
  deleteOptionWithToken,
  getOptionWithToken,
  postOptionWithToken,
  putOptionWithToken,
} from "../../../utils/request";
import { getAllTracingReportsSuccess, hideLoader, showLoader } from "./redux";

export const getAllTracingReportsStart = () => {
  return async (dispatch, getState) => {
    try {
      const { auth } = getState().auth;
      dispatch(showLoader());
      let url = `${process.env.REACT_APP_URL_API}/stageCases/${auth.user.id}`;
      let options = getOptionWithToken();
      let registers = await request(url, options);
      dispatch(getAllTracingReportsSuccess(registers));
    } catch (error) {
      let message = error?.message
        ? error.message
        : "No se pudo obtener los registros";
      dispatch(messageError(message));
    } finally {
      dispatch(hideLoader());
    }
  };
};
export const CreateTracingStart = (values) => {
  return async (dispatch, getState) => {
    try {
      const { data } = getState().tracingReport;
      dispatch(showGlobalLoader());
      let url = `${process.env.REACT_APP_URL_API}/stageCases`;
      let options = postOptionWithToken(values);
      let registers = await request(url, options);
      dispatch(
        getAllTracingReportsSuccess(
          data.map((e) => {
            if (e.id === registers[0].id) {
              return {
                ...e,
                etapaCasos: [...e.etapaCasos, registers[0].etapaCasos[0]],
              };
            }
            return e;
          })
        )
      );
      dispatch(messageSuccess("Se creo con éxito el seguimiento"));
    } catch (error) {
      let message = error?.message
        ? error.message
        : "No se crear el seguimiento";
      dispatch(messageError(message));
    } finally {
      dispatch(hideGlobalLoader());
    }
  };
};

export const deleteTracingStart = (id, itemId) => {
  return async (dispatch, getState) => {
    try {
      const { data } = getState().tracingReport;
      dispatch(showGlobalLoader());
      let url = `${process.env.REACT_APP_URL_API}/stageCases/${id}`;
      let options = deleteOptionWithToken();
      let registers = await request(url, options);
      dispatch(
        getAllTracingReportsSuccess(
          data.map((e) => {
            if (e.id === itemId) {
              return {
                ...e,
                etapaCasos: e.etapaCasos.filter((e) => e.id !== id),
              };
            }
            return e;
          })
        )
      );
      dispatch(messageSuccess(registers));
    } catch (error) {
      let message = error?.message ? error.message : "No se pudo eliminar";
      dispatch(messageError(message));
    } finally {
      dispatch(hideGlobalLoader());
    }
  };
};

export const updateTracingStart = (values, id, registerId) => {
  return async (dispatch, getState) => {
    try {
      const { data } = getState().tracingReport;
      dispatch(showGlobalLoader());
      let url = `${process.env.REACT_APP_URL_API}/stageCases/${id}`;
      let options = putOptionWithToken(values);
      let registers = await request(url, options);

      dispatch(
        getAllTracingReportsSuccess(
          data.map((e) => {
            if (e.id === registerId) {
              return {
                ...e,
                etapaCasos: e.etapaCasos.map((e) =>
                  e.id === registers.id ? registers : e
                ),
              };
            }
            return e;
          })
        )
      );
      dispatch(messageSuccess("Se edito con éxito el seguimiento"));
    } catch (error) {
      let message = error?.message
        ? error.message
        : "No se crear el seguimiento";
      dispatch(messageError(message));
    } finally {
      dispatch(hideGlobalLoader());
    }
  };
};
