import { createSlice } from "@reduxjs/toolkit";
const initialState = {
  data: [],
  loader: [],
};
export const tracingReportSlice = createSlice({
  name: "tracingReports",
  initialState,
  reducers: {
    getAllTracingReportsSuccess: (state, action) => {
      state.data = action.payload;
    },
    showLoader: (state) => {
      state.loader = true;
    },
    hideLoader: (state) => {
      state.loader = false;
    },
  },
});

export const { getAllTracingReportsSuccess, showLoader, hideLoader } =
  tracingReportSlice.actions;
export default tracingReportSlice.reducer;
