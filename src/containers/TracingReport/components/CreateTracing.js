import { Button, Grid, TextField, Typography, useTheme } from "@mui/material";
import { DatePicker } from "@mui/x-date-pickers";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import BaseModal from "../../../components/BaseModal/BaseModal";
import {
  CreateTracingStart,
  deleteTracingStart,
  updateTracingStart,
} from "../redux/thunk";

const CreateTracing = ({ status, closeModal, info }) => {
  const [date, setDate] = useState(null);
  const [text, SetText] = useState("");
  const [errorDate, setErrorDate] = useState(false);
  const [errorText, setErrorText] = useState(false);
  const [isEdit, setIsEdit] = useState(false);
  const theme = useTheme();
  const dispatch = useDispatch();
  const createData = () => {
    if (!date) return setErrorDate(true);
    if (text.length > 200) return setErrorText(true);
    if (info.edit) {
      return dispatch(
        updateTracingStart(
          {
            fechaRegistro: date?.$d || date,
            accionSeguir: text,
          },
          info.edit.id,
          info.edit.registroId
        )
      );
    }
    dispatch(
      CreateTracingStart({
        fechaRegistro: date.$d,
        accionSeguir: text,
        registroId: info.itemId,
        etapaProcesalId: info.id,
      })
    );
  };
  useEffect(() => {
    if (info.edit) {
      setIsEdit(true);
      setDate(info.edit.fechaRegistro);
      SetText(info.edit.accionSeguir);
    } else {
      setIsEdit(false);
      setDate(null);
      SetText("");
    }
  }, [status]);
  const handleDelete = () => {
    dispatch(deleteTracingStart(info.edit.id, info.edit.registroId));
  };
  return (
    <BaseModal
      title={`Seguimiento: ${info.title} caso ${info.itemId}`}
      statusModal={status}
      closeModal={closeModal}
    >
      {isEdit ? (
        <Grid container justifyContent="center">
          <Grid
            item
            xs={12}
            container
            sx={{ p: 2 }}
            direction="column"
            alignItems="center"
          >
            <Typography
              color={theme.palette.text.TextCuston}
              sx={{ mb: 1, fontWeight: 600 }}
            >
              Fecha registro:
            </Typography>
            <Typography color={theme.palette.text.TextCuston}>
              {new Date(info.edit.fechaRegistro).toLocaleDateString()}
            </Typography>
          </Grid>
          <Grid
            item
            xs={12}
            container
            sx={{ p: 2 }}
            direction="column"
            alignItems="center"
          >
            <Typography
              color={theme.palette.text.TextCuston}
              sx={{ mb: 1, fontWeight: 600 }}
            >
              Detalle del registro:
            </Typography>
            <Typography color={theme.palette.text.TextCuston}>
              {info.edit.accionSeguir}
            </Typography>
          </Grid>
          <Grid container justifyContent="space-around" sx={{ my: 2 }}>
            {info.isDelit.length === 0 && (
              <Grid item xs={12} lg={4}>
                <Button
                  fullWidth
                  variant="contained"
                  color="error"
                  onClick={handleDelete}
                >
                  Eliminar
                </Button>
              </Grid>
            )}
            <Grid item xs={12} lg={4}>
              <Button
                fullWidth
                variant="contained"
                sx={{ mt: { xs: 2, lg: 0 } }}
                color="info"
                onClick={(e) => {
                  setIsEdit(false);
                }}
              >
                Editar
              </Button>
            </Grid>
          </Grid>
        </Grid>
      ) : (
        <Grid container justifyContent="center">
          <Grid item xs={12} lg={9}>
            <DatePicker
              minDate={info.initDate}
              onChange={(date) => setDate(date)}
              disableFuture
              label="Seleccione la fecha del registro"
              value={date}
              renderInput={(params) => (
                <TextField
                  fullWidth
                  {...params}
                  error={errorDate}
                  helperText={errorDate && "Debe ingresar un fecha"}
                />
              )}
            />
          </Grid>
          <Grid item xs={12} lg={9} sx={{ mt: 3 }}>
            <TextField
              multiline
              value={text}
              onChange={(e) => SetText(e.target.value)}
              fullWidth
              label="acciones a seguir"
              error={errorText}
              helperText={
                errorText &&
                "La descripción no puede ser mayor a 200 caracteres"
              }
            ></TextField>
          </Grid>
          <Grid item xs={12} lg={9} sx={{ my: 3 }}>
            <Button
              fullWidth
              variant="contained"
              color="success"
              size="large"
              onClick={createData}
            >
              {info.edit ? "Editar Seguimiento" : "Crear seguimiento"}
            </Button>
          </Grid>
        </Grid>
      )}
    </BaseModal>
  );
};

export default CreateTracing;
