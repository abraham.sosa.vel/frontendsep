import { Button, TableCell } from "@mui/material";
import React, { useState } from "react";
import { useModal } from "../../../utils/useModal";
import CreateTracing from "./CreateTracing";

const ItemTracingTable = ({ item }) => {
  const [statusModal, handleCloseModal, handleOpenModal] = useModal();
  const [dataModal, setDataModal] = useState({ title: "", id: 0, itemId: 0 });
  const handleCreate = (
    id,
    itemId,
    title,
    initDate,
    edit = false,
    isDelit = []
  ) => {
    setDataModal({
      title,
      id,
      itemId,
      initDate,
      edit,
      isDelit,
    });
    handleOpenModal();
  };
  return (
    <>
      <CreateTracing
        status={statusModal}
        closeModal={handleCloseModal}
        info={dataModal}
      />
      <TableCell sx={{ textAlign: "center", py: 4 }}>{item.id}</TableCell>
      <TableCell sx={{ textAlign: "center" }}>
        {new Date(item.register).toLocaleDateString()}
      </TableCell>
      <TableCell sx={{ textAlign: "center" }}>
        {item.initialActions.length === 0 ? (
          <Button
            fullWidth
            variant="contained"
            color="success"
            onClick={(e) => {
              handleCreate(1, item.id, "Acción inicial", item.register);
            }}
          >
            Crear
          </Button>
        ) : (
          <Button
            fullWidth
            variant="contained"
            color="info"
            onClick={(e) => {
              handleCreate(
                1,
                item.id,
                "Acción inicial",
                item.register,
                item.initialActions[0],
                item.preparatory
              );
            }}
          >
            Detalle
          </Button>
        )}
      </TableCell>
      <TableCell sx={{ textAlign: "center" }}>
        {item.initialActions.length === 0 ? (
          <Button fullWidth disabled variant="contained" color="warning">
            Aun no disponible
          </Button>
        ) : item.preparatory.length === 0 ? (
          <Button
            fullWidth
            variant="contained"
            color="success"
            onClick={(e) => {
              handleCreate(
                2,
                item.id,
                "Preparatoria",
                item.initialActions[0].fechaRegistro
              );
            }}
          >
            Crear
          </Button>
        ) : (
          <Button
            fullWidth
            variant="contained"
            color="info"
            onClick={(e) => {
              handleCreate(
                1,
                item.id,
                "Acción inicial",
                item.register,
                item.preparatory[0],
                item.oralTrial
              );
            }}
          >
            Detalle
          </Button>
        )}
      </TableCell>
      <TableCell sx={{ textAlign: "center" }}>
        {item.preparatory.length === 0 ? (
          <Button fullWidth disabled variant="contained" color="warning">
            Aun no disponible
          </Button>
        ) : item.oralTrial.length === 0 ? (
          <Button
            fullWidth
            variant="contained"
            color="success"
            onClick={(e) => {
              handleCreate(
                3,
                item.id,
                "Juicio oral",
                item.preparatory[0].fechaRegistro
              );
            }}
          >
            Crear
          </Button>
        ) : (
          <Button
            fullWidth
            variant="contained"
            color="info"
            onClick={(e) => {
              handleCreate(
                1,
                item.id,
                "Acción inicial",
                item.register,
                item.oralTrial[0],
                item.means
              );
            }}
          >
            Detalle
          </Button>
        )}
      </TableCell>
      <TableCell sx={{ textAlign: "center" }}>
        {item.oralTrial.length === 0 ? (
          <Button fullWidth disabled variant="contained" color="warning">
            Aun no disponible
          </Button>
        ) : item.means.length === 0 ? (
          <Button
            fullWidth
            variant="contained"
            color="success"
            onClick={(e) => {
              handleCreate(
                4,
                item.id,
                "Recursos",
                item.oralTrial[0].fechaRegistro
              );
            }}
          >
            Crear
          </Button>
        ) : (
          <Button
            fullWidth
            variant="contained"
            color="info"
            onClick={(e) => {
              handleCreate(
                1,
                item.id,
                "Acción inicial",
                item.register,
                item.means[0],
                item.performance
              );
            }}
          >
            Detalle
          </Button>
        )}
      </TableCell>
      <TableCell sx={{ textAlign: "center" }}>
        {item.means.length === 0 ? (
          <Button fullWidth disabled variant="contained" color="warning">
            Aun no disponible
          </Button>
        ) : item.performance.length === 0 ? (
          <Button
            fullWidth
            variant="contained"
            color="success"
            onClick={(e) => {
              handleCreate(
                5,
                item.id,
                "Ejecución",
                item.means[0].fechaRegistro
              );
            }}
          >
            Crear
          </Button>
        ) : (
          <Button
            fullWidth
            variant="contained"
            color="info"
            onClick={(e) => {
              handleCreate(
                1,
                item.id,
                "Acción inicial",
                item.register,
                item.performance[0]
              );
            }}
          >
            Detalle
          </Button>
        )}
      </TableCell>
    </>
  );
};

export default ItemTracingTable;
