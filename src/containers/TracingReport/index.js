import { Grid, Paper, useTheme } from "@mui/material";
import React from "react";
import { useTranslation } from "react-i18next";
import { useSelector } from "react-redux";
import BaseTable from "../../components/BaseTable";
import TitlePage from "../../components/TitlePage";
import ItemTracingTable from "./components/ItemTracingTable";
import { getAllTracingFormat } from "./utils/formatTracingReports";
import { useColumsTracing } from "./utils/useColumsTracing";

const TracingReport = () => {
  const theme = useTheme();
  const [t] = useTranslation("global");
  const { data, loader } = useSelector((state) => state.tracingReport);
  return (
    <Paper
      sx={{ p: 2, pt: 3 }}
      elevation={theme.palette.mode === "dark" ? 0 : 5}
    >
      <Grid container direction="column">
        <Grid sx={{ mb: 1 }}>
          <TitlePage title={t("NavItems.tracingReports").toUpperCase()} />
        </Grid>
      </Grid>
      <BaseTable columns={useColumsTracing()} loading={loader} rowsLength={6}>
        {getAllTracingFormat(data).map((e) => (
          <ItemTracingTable key={e.id} item={e} />
        ))}
      </BaseTable>
    </Paper>
  );
};

export default TracingReport;
