import { ArrowCircleRight } from "@mui/icons-material";
import { Button, Grid, TextField } from "@mui/material";
import React from "react";
import { useForm } from "react-hook-form";
import { useTranslation } from "react-i18next";
import useValidations from "../utils/validations";
const LoginForm = ({ onSubmit }) => {
  const [t] = useTranslation("global");
  const { message, validationEmail, validatePassword } = useValidations();
  const {
    register,
    formState: { errors },
    handleSubmit,
  } = useForm({
    mode: "onChange",
  });

  return (
    <Grid container item xs={12} direction="column">
      <form sx={{ mt: 1 }} onSubmit={handleSubmit(onSubmit)}>
        <Grid item xs={12}>
          <TextField
            margin="normal"
            fullWidth
            {...register("email", {
              validate: validationEmail,
            })}
            label={t("login.email")}
            autoComplete="email"
            autoFocus
            error={!!errors?.email}
            helperText={errors.email && message.email}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            margin="normal"
            fullWidth
            {...register("password", {
              validate: validatePassword,
            })}
            label={t("login.password")}
            type="password"
            autoComplete="password"
            error={!!errors?.password}
            helperText={errors.password && message.password}
          />
        </Grid>
        <Grid item xs={12}>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
            size="large"
            endIcon={<ArrowCircleRight />}
          >
            {t("login.logIn")}
          </Button>
        </Grid>
      </form>
    </Grid>
  );
};

export default LoginForm;
