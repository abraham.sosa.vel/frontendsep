import { useState } from "react";
import { useTranslation } from "react-i18next";
import { regexEmail, regexPassword } from "../../../utils/regularExpresions";

const useValidations = () => {
  const [t] = useTranslation("global");
  const [message, setMessages] = useState({
    email: "",
    password: "",
  });
  const validationEmail = (value) => {
    if (value === "") {
      setMessages({ ...message, email: t("errorMesages.required") });
      return false;
    }
    if (!regexEmail.test(value)) {
      setMessages({ ...message, email: t("errorMesages.email") });
      return false;
    }
    return true;
  };

  const validatePassword = (value) => {
    if (value === "") {
      setMessages({ ...message, password: t("errorMesages.required") });
      return false;
    }
    if (value.length < 8 || value.length > 15) {
      setMessages({ ...message, password: t("errorMesages.passwordLength") });
      return false;
    }
    if (!regexPassword.test(value)) {
      setMessages({ ...message, password: t("errorMesages.password") });
      return false;
    }
    return true;
  };
  return {
    message,
    validationEmail,
    validatePassword,
  };
};

export default useValidations;
