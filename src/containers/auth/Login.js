import * as React from "react";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import { useTranslation } from "react-i18next";
import { Typography, useTheme } from "@mui/material";
import ButtonTheme from "../../components/ButtonTheme/ButtonTheme";
import LoginForm from "./component/LoginForm";
import { useDispatch } from "react-redux";
import { signInStart } from "./redux/thunk";

const Login = () => {
  const dispatch = useDispatch();
  const theme = useTheme();
  const [t] = useTranslation("global");

  const handleSubmitForm = (data) => {
    dispatch(signInStart(data));
  };

  return (
    <>
      <Grid
        container
        component="main"
        sx={{
          height: "100vh",
          bgcolor: theme.palette.background.paper,
        }}
      >
        <Grid
          container
          item
          xs={false}
          sm={4}
          md={7}
          justifyContent="center"
          alignContent="center"
          sx={{ bgcolor: theme.palette.background.loginDef }}
        >
          <img
            src={
              theme.palette.mode === "light"
                ? "/logos/logoWhite.png"
                : "/logos/logoDark.png"
            }
            alt={t("global.logoSepdavi")}
            style={{
              width: "50%",
            }}
          />
        </Grid>
        <Grid
          item
          xs={12}
          sm={8}
          md={5}
          component={Paper}
          elevation={6}
          square
          justifyContent="center"
          alignItems="start"
          container
        >
          <Grid
            xs={12}
            item
            container
            direction="column"
            alignContent="end"
            sx={{ mt: 2, mr: 2 }}
          >
            <ButtonTheme />
          </Grid>
          <Grid
            container
            direction="column"
            alignItems="center"
            sx={{
              mb: 8,
              mx: 4,
            }}
          >
            <Typography component="h1" variant="h5" sx={{ fontWeight: "bold" }}>
              {t("login.title")}
            </Typography>
            <LoginForm onSubmit={handleSubmitForm} />
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};
export default Login;
