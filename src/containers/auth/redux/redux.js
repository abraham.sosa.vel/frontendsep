import { createSlice } from "@reduxjs/toolkit";
const initialState = {
  auth: JSON.parse(localStorage.getItem("userData")) || false,
  token: "",
};
export const authSlice = createSlice({
  name: "authSlice",
  initialState,
  reducers: {
    signInSuccess: (state, action) => {
      state.auth = action.payload;
    },
    logOutSuccess: (state, action) => {
      state.auth = false;
    },
  },
});

export const { signInSuccess, logOutSuccess } = authSlice.actions;
export default authSlice.reducer;
