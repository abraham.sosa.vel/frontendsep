import {
  hideGlobalLoader,
  messageError,
  messageSuccess,
  showGlobalLoader,
} from "../../../redux/globalRedux";
import request, { postOptionWithoutToken } from "../../../utils/request";
import { signInSuccess } from "./redux";

export const signInStart = ({ email, password }) => {
  return async (dispatch) => {
    try {
      dispatch(showGlobalLoader());
      const url = `${process.env.REACT_APP_URL_API}/auth/signIn`;
      const options = postOptionWithoutToken({
        correoElectronico: email,
        contrasenia: password,
      });
      const data = await request(url, options);
      if (!data.user.estado) {
        return dispatch(
          messageError({ message: "La cuenta se encuentra desactivada" })
        );
      }
      localStorage.setItem("userData", JSON.stringify(data));
      dispatch(signInSuccess(data));
      dispatch(messageSuccess("Inicio de sesión exitoso"));
    } catch (error) {
      dispatch(messageError(error.message));
    } finally {
      dispatch(hideGlobalLoader());
    }
  };
};
