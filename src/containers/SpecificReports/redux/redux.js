import { createSlice } from "@reduxjs/toolkit";
const initialState = {
  years: [],
  loaderYear: false,
  department: [],
  victimSex: [],
  generalCrimes: [],
  instructionVictim: [],
  occupationVictim: [],
  orientationVictim: [],
  countDelits: 0,
  crimes: [],
};
export const specificReportSlice = createSlice({
  name: "specificReport",
  initialState,
  reducers: {
    getAllYearRegiterSuccess: (state, action) => {
      const date = action.payload.map((e) => {
        return {
          id: e.year.substring(0, 4),
          year: e.year.substring(0, 4),
        };
      });
      state.years = date;
    },
    showLoaderYear: (state) => {
      state.loaderYear = true;
    },
    hideLoaderYear: (state) => {
      state.loaderYear = false;
    },
    generateSpecificReportSuccess: (state, action) => {
      state.department = action.payload.department;
      state.victimSex = action.payload.victimSex;
      state.generalCrimes = action.payload.generalCrimes;
      state.instructionVictim = action.payload.instructionVictim;
      state.occupationVictim = action.payload.occupationVictim;
      state.orientationVictim = action.payload.orientationVictim;
      state.countDelits = action.payload.countDelits;
    },
    getAllCrimesSuccess: (state, action) => {
      state.crimes = action.payload.map((item) => ({
        label: item.nombre,
        value: item.id,
      }));
    },
  },
});

export const {
  getAllCrimesSuccess,
  getAllYearRegiterSuccess,
  showLoaderYear,
  hideLoaderYear,
  generateSpecificReportSuccess,
} = specificReportSlice.actions;
export default specificReportSlice.reducer;
