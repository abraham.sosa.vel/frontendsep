import {
  hideGlobalLoader,
  messageError,
  messageSuccess,
  showGlobalLoader,
} from "../../../redux/globalRedux";
import request, {
  getOptionWithoutToken,
  getOptionWithToken,
  postOptionWithToken,
} from "../../../utils/request";
import {
  generateSpecificReportSuccess,
  getAllCrimesSuccess,
  getAllYearRegiterSuccess,
  hideLoaderYear,
  showLoaderYear,
} from "./redux";

export const getAllYearRegiterStart = () => {
  return async (dispatch, getState) => {
    const { years } = getState().specificReports;
    try {
      dispatch(showLoaderYear());
      if (years.length > 0) return;
      let url = `${process.env.REACT_APP_URL_API}/utils/years`;
      let options = getOptionWithToken();
      let getYears = await request(url, options);
      dispatch(getAllYearRegiterSuccess(getYears));
    } catch (error) {
      let message = error?.message
        ? error.message
        : "No se pudo cargar los datos de las ocupaciones";
      dispatch(messageError(message));
    } finally {
      dispatch(hideLoaderYear());
    }
  };
};

export const generateSpecificReport = (data) => {
  return async (dispatch, getState) => {
    try {
      const colors = [
        "#ef5350",
        "#ec407a",
        "#ab47bc",
        "#7e57c2",
        "#5c6bc0",
        "#42a5f5",
        "#29b6f6",
        "#26c6da",
        "#26a69a",
        "#66bb6a",
        "#9ccc65",
        "#d4e157",
        "#ffee58",
        "#ffca28",
        "#ffa726",
        "#ff7043",
        "#8d6e63",
      ];
      const datas = {
        department: [["Año", "Casos atendidos", { role: "style" }]],
        victimSex: [["Sexo", "cantidad", { role: "style" }]],
        generalCrimes: [["Delito", "cantidad"]],
        instructionVictim: [
          ["Grado de instrucción", "cantidad", { role: "style" }],
        ],
        occupationVictim: [["Ocupación", "Cantidad", { role: "style" }]],
        orientationVictim: [
          ["Orientación sexual", "cantidad", { role: "style" }],
        ],
        countDelits: 0,
      };
      dispatch(showGlobalLoader());
      const url = `${process.env.REACT_APP_URL_API}/reporter`;
      const options = postOptionWithToken(data);
      const getData = await request(url, options);
      getData?.departament.forEach((e) => {
        datas.department.push([
          e[0],
          e.length,
          colors[Math.floor(Math.random() * 16) + 1],
        ]);
      });
      getData?.sexVictim.forEach((e) => {
        if (e[0])
          return datas.victimSex.push([
            "Masculino",
            e.length,
            colors[Math.floor(Math.random() * 16) + 1],
          ]);
        datas.victimSex.push([
          "Femenino",
          e.length,
          colors[Math.floor(Math.random() * 16) + 1],
        ]);
      });
      getData?.generalCrimes.forEach((e) => {
        datas.countDelits += e.length;
        datas.generalCrimes.push([e[0], e.length]);
      });
      getData?.instructionVictim.forEach((e) => {
        datas.instructionVictim.push([
          e[0],
          e.length,
          colors[Math.floor(Math.random() * 16) + 1],
        ]);
      });
      getData?.occupationVictim.forEach((e) => {
        datas.occupationVictim.push([
          e[0],
          e.length,
          colors[Math.floor(Math.random() * 16) + 1],
        ]);
      });
      getData?.orientationVictim.forEach((e) => {
        datas.orientationVictim.push([
          e[0],
          e.length,
          colors[Math.floor(Math.random() * 16) + 1],
        ]);
      });
      dispatch(generateSpecificReportSuccess(datas));
      dispatch(messageSuccess("Se obtuvo los datos con éxito"));
    } catch (error) {
      let message = error?.message
        ? error.message
        : "No se pudo cargar los datos";
      dispatch(messageError(message));
    } finally {
      dispatch(hideGlobalLoader());
    }
  };
};
export const getAllCrimesStart = () => {
  return async (dispatch) => {
    try {
      let url = `${process.env.REACT_APP_URL_API}/crimes`;
      let options = getOptionWithoutToken();
      let crimes = await request(url, options);
      dispatch(getAllCrimesSuccess(crimes));
    } catch (error) {
      let message = error?.message
        ? error.message
        : "No se pudo cargar los datos de los delitos";
      dispatch(messageError(message));
    }
  };
};
