import { useTheme } from "@emotion/react";
import {
  Autocomplete,
  Button,
  Dialog,
  DialogContent,
  DialogContentText,
  FormControl,
  FormControlLabel,
  FormHelperText,
  FormLabel,
  Grid,
  InputLabel,
  MenuItem,
  Paper,
  Radio,
  RadioGroup,
  Select,
  TextField,
  Typography,
} from "@mui/material";
import { red } from "@mui/material/colors";
import { PDFDownloadLink } from "@react-pdf/renderer";
import html2canvas from "html2canvas";
import React, { useState } from "react";
import Chart from "react-google-charts";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import TitlePage from "../../components/TitlePage";
import { useModal } from "../../utils/useModal";
import { NewGenerate } from "./components/NewGenerate";
import { Victim } from "./components/Victim";
import { generateSpecificReport } from "./redux/thunk";
import {
  formatSpecificReports,
  formatView,
} from "./utlis/formatSpecificReports";
import { periods } from "./utlis/periods";

const refDepartament = React.createRef();
const refVictim = React.createRef();
const refVictim2 = React.createRef();
const SpecificReports = () => {
  const theme = useTheme();
  const [t] = useTranslation("global");
  const [modalVictimStatus, handleCloseVictimModal, handleOpenVictimModal] =
    useModal();
  const [modalGeneralStatus, handleCloseGeneralModal, handleOpenGeneralModal] =
    useModal();
  const [dataSend, setDataSend] = useState({});
  const [values, setValues] = useState({
    period: "",
    age: [],
    month: "",
    crimeSelect: [],
    errorPeriod: false,
    errorAge: false,
    errorMonth: false,
  });

  const [image, setImage] = useState({
    departament: "/",
    victim: "/",
    victim2: "/",
  });
  const [active, setActive] = useState(false);
  const {
    years,
    department,
    victimSex,
    generalCrimes,
    instructionVictim,
    occupationVictim,
    orientationVictim,
    countDelits,
    crimes,
  } = useSelector((state) => state.specificReports);
  const dispatch = useDispatch();
  const handleChange = (e) => {
    const value = e.target?.value;
    const name = e.target.name;
    setValues({
      ...values,
      [name]: value,
    });
    return;
  };
  const onSubmit = () => {
    if (values.period === "")
      return setValues({ ...values, errorPeriod: true });
    if (values.age.length === 0) {
      return setValues({ ...values, errorAge: true });
    }
    if ((values.month === "") & (values.period !== "anual"))
      return setValues({ ...values, errorMonth: true });
    setValues({
      ...values,
      errorPeriod: false,
      errorAge: false,
      errorMonth: false,
    });
    setActive(true);
    setDataSend(formatView(values));
    dispatch(generateSpecificReport(formatSpecificReports(values)));
  };
  const generate = () => {
    html2canvas(refDepartament.current).then((canvas) => {
      const base64Image = canvas.toDataURL();
      setImage({ ...image, departament: base64Image });
    });
  };
  const generateVictim = () => {
    html2canvas(refVictim.current).then((canvas) => {
      const base64Image = canvas.toDataURL();
      html2canvas(refVictim2.current).then((canvas) => {
        const base64Image2 = canvas.toDataURL();
        setImage({ ...image, victim: base64Image, victim2: base64Image2 });
      });
    });
  };
  return (
    <>
      <Paper
        sx={{ p: 2, pt: 3 }}
        elevation={theme.palette.mode === "dark" ? 0 : 5}
      >
        <Grid container justifyContent="center">
          <Grid container justifyContent="center" item xs={12} lg={9} gap={2}>
            <Grid sx={{ mb: 1, textAlign: "center" }}>
              <TitlePage title={t("NavItems.specificReports").toUpperCase()} />
            </Grid>
            <Grid item xs={12} container justifyContent="center">
              <FormControl sx={{ mt: 2 }} error={values.errorPeriod}>
                <FormLabel sx={{ textAlign: "center" }}>
                  Periodo de tiempo
                </FormLabel>
                <RadioGroup
                  name="period"
                  value={values.period}
                  onChange={handleChange}
                  row
                  sx={{ justifyContent: "center" }}
                >
                  <FormControlLabel
                    value="mensual"
                    control={<Radio />}
                    label={t("filterYear.month")}
                  />
                  <FormControlLabel
                    value="bimestral"
                    control={<Radio />}
                    label={t("filterYear.bimonthly")}
                  />
                  <FormControlLabel
                    value="trimestral"
                    control={<Radio />}
                    label={t("filterYear.quarterly")}
                  />
                  <FormControlLabel
                    value="semestral"
                    control={<Radio />}
                    label={t("filterYear.biannual")}
                  />
                  <FormControlLabel
                    value="anual"
                    control={<Radio />}
                    label={t("filterYear.annual")}
                  />
                </RadioGroup>
                <FormHelperText sx={{ textAlign: "center" }}>
                  {values.errorPeriod && "Debe ingresar un periodo de tiempo"}
                </FormHelperText>
              </FormControl>
            </Grid>
            {values.period.length > 0 && (
              <Grid item xs={12} container justifyContent="center" spacing={2}>
                <Grid item xs={12} md={6}>
                  <FormControl fullWidth error={values.errorAge}>
                    <InputLabel>Gestión</InputLabel>
                    <Select
                      value={values.age}
                      label="Gestión"
                      name="age"
                      multiple
                      onChange={handleChange}
                    >
                      {years.map((e) => (
                        <MenuItem key={e.id} value={e.id}>
                          {e.year}
                        </MenuItem>
                      ))}
                    </Select>
                    <FormHelperText sx={{ textAlign: "center" }}>
                      {values.errorAge && "Debe ingresar el año"}
                    </FormHelperText>
                  </FormControl>
                </Grid>
                {values.period !== "anual" && (
                  <Grid item xs={12} md={6}>
                    <FormControl fullWidth error={values.errorMonth}>
                      <InputLabel>Meses</InputLabel>
                      <Select
                        value={values.month}
                        label="Meses"
                        name="month"
                        onChange={handleChange}
                      >
                        {periods()[values.period]?.map((e) => (
                          <MenuItem key={e.id} value={e.id}>
                            {e.value}
                          </MenuItem>
                        ))}
                      </Select>
                      <FormHelperText sx={{ textAlign: "center" }}>
                        {values.errorMonth && "Campo obligatorio"}
                      </FormHelperText>
                    </FormControl>
                  </Grid>
                )}
              </Grid>
            )}
            <Grid container justifyContent="center">
              <Grid item xs={12} lg={6}>
                <Autocomplete
                  multiple
                  value={values.crimeSelect}
                  onChange={(e, newValue) => {
                    setValues({ ...values, crimeSelect: newValue });
                  }}
                  options={crimes}
                  getOptionLabel={(option) => option.label}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      fullWidth
                      margin="normal"
                      label={t("createRegister.principalTypology")}
                    />
                  )}
                />
              </Grid>
            </Grid>
            <Grid
              item
              xs={12}
              md={3}
              justifyContent="center"
              alignItems="center"
              sx={{ my: 2 }}
            >
              <Button
                fullWidth
                variant="contained"
                color="primary"
                onClick={onSubmit}
              >
                Generar reporte
              </Button>
            </Grid>
          </Grid>
        </Grid>
        {active ? (
          victimSex.length === 1 ? (
            <Grid justifyContent="center" container sx={{ py: 10 }}>
              <Typography
                variant="h5"
                sx={{ color: red[700], fontWeight: 600 }}
              >
                No se tiene registros
              </Typography>
            </Grid>
          ) : (
            <>
              {/*general departament */}
              <Grid sx={{ position: "relative", height: "500px" }}>
                <Grid
                  sx={{
                    position: "absolute",
                    top: 0,
                    left: 0,
                    width: "100%",
                    opacity: 0,
                  }}
                >
                  <div ref={refDepartament}>
                    <Chart
                      chartType="ColumnChart"
                      data={department}
                      responsive={true}
                      weight="100%"
                      height="500px"
                      options={{
                        title: "Casos anuales atendidos ",
                        backgroundColor: "transparent",

                        hAxis: {
                          title: "Departamento",
                        },
                        vAxis: {
                          title: "Cantidad",
                        },
                      }}
                      legendToggle
                    />
                  </div>
                </Grid>
                <Grid
                  sx={{
                    position: "absolute",
                    top: 0,
                    left: 0,
                    width: "100%",
                  }}
                >
                  <Chart
                    chartType="ColumnChart"
                    data={department}
                    responsive={true}
                    weight="100%"
                    height="500px"
                    options={{
                      title: "Casos anuales atendidos ",
                      backgroundColor: "transparent",
                      titleTextStyle: {
                        color: theme.palette.text.TextCuston,
                      },
                      hAxis: {
                        title: "Departamento",
                        textStyle: {
                          color: theme.palette.text.textFields,
                        },
                      },
                      vAxis: {
                        title: "Cantidad",
                        textStyle: {
                          color: theme.palette.text.textFields,
                        },
                      },
                      legend: {
                        textStyle: {
                          color: theme.palette.text.TextCuston,
                        },
                      },
                    }}
                    legendToggle
                  />
                </Grid>
              </Grid>
              <Grid container justifyContent="center" sx={{ my: 2 }}>
                <Typography variant="h4" component="h4">
                  Datos de la victima
                </Typography>
              </Grid>
              {/*victim sex */}
              <Grid sx={{ position: "relative", height: "2800px" }}>
                <Grid
                  sx={{
                    position: "absolute",
                    top: 0,
                    left: 0,
                    width: "100%",
                    opacity: 0,
                  }}
                >
                  <div ref={refVictim}>
                    <Chart
                      chartType="PieChart"
                      data={victimSex}
                      responsive={true}
                      weight="100%"
                      height="700px"
                      options={{
                        pieSliceText: "value",
                        is3D: true,
                        title: "Sexo de las victimas",
                        backgroundColor: "transparent",

                        hAxis: {
                          title: "Departamento",
                        },
                        vAxis: {
                          title: "Cantidad",
                        },
                      }}
                      legendToggle
                    />
                    <Chart
                      chartType="ColumnChart"
                      data={orientationVictim}
                      responsive={true}
                      weight="100%"
                      height="700px"
                      options={{
                        pieSliceText: "value",
                        is3D: true,
                        title: "Orientación sexual victimas",
                        backgroundColor: "transparent",

                        hAxis: {
                          title: "Orientación sexual",
                        },
                        vAxis: {
                          title: "Cantidad",
                        },
                      }}
                      legendToggle
                    />
                  </div>
                  <div ref={refVictim2}>
                    <Chart
                      chartType="ColumnChart"
                      data={occupationVictim}
                      responsive={true}
                      weight="100%"
                      height="700px"
                      options={{
                        pieSliceText: "value",
                        is3D: true,
                        title: "Ocupaciones de la victimas",
                        backgroundColor: "transparent",

                        hAxis: {
                          title: "Ocupación",
                        },
                        vAxis: {
                          title: "Cantidad",
                        },
                      }}
                      legendToggle
                    />
                    <Chart
                      chartType="ColumnChart"
                      data={instructionVictim}
                      responsive={true}
                      weight="100%"
                      height="700px"
                      options={{
                        pieSliceText: "value",
                        is3D: true,
                        title: "Grado de instrucción de la victima",
                        backgroundColor: "transparent",

                        hAxis: {
                          title: "Grado de instrucción",
                        },
                        vAxis: {
                          title: "Cantidad",
                        },
                      }}
                      legendToggle
                    />
                  </div>
                </Grid>
                <Grid
                  sx={{
                    position: "absolute",
                    top: 0,
                    left: 0,
                    width: "100%",
                  }}
                >
                  <Chart
                    chartType="PieChart"
                    data={victimSex}
                    responsive={true}
                    weight="100%"
                    height="700px"
                    options={{
                      title: "Sexo de las victimas",
                      backgroundColor: "transparent",
                      titleTextStyle: {
                        color: theme.palette.text.TextCuston,
                      },

                      legend: {
                        textStyle: {
                          color: theme.palette.text.TextCuston,
                        },
                      },
                    }}
                    legendToggle
                  />
                  <Chart
                    chartType="ColumnChart"
                    data={orientationVictim}
                    responsive={true}
                    weight="100%"
                    height="700px"
                    options={{
                      pieSliceText: "value",
                      title: "Orientación sexual victimas",
                      backgroundColor: "transparent",
                      titleTextStyle: {
                        color: theme.palette.text.TextCuston,
                      },
                      hAxis: {
                        title: "Orientación sexual",
                        textStyle: {
                          color: theme.palette.text.textFields,
                        },
                      },
                      vAxis: {
                        title: "Cantidad",
                        textStyle: {
                          color: theme.palette.text.textFields,
                        },
                      },
                      legend: {
                        textStyle: {
                          color: theme.palette.text.TextCuston,
                        },
                      },
                    }}
                    legendToggle
                  />
                  <Chart
                    chartType="ColumnChart"
                    data={occupationVictim}
                    responsive={true}
                    weight="100%"
                    height="700px"
                    options={{
                      pieSliceText: "value",
                      title: "Ocupaciones de la victimas",
                      backgroundColor: "transparent",
                      titleTextStyle: {
                        color: theme.palette.text.TextCuston,
                      },
                      hAxis: {
                        title: "Ocupación",
                        textStyle: {
                          color: theme.palette.text.textFields,
                        },
                      },
                      vAxis: {
                        title: "Cantidad",
                        textStyle: {
                          color: theme.palette.text.textFields,
                        },
                      },
                      legend: {
                        textStyle: {
                          color: theme.palette.text.TextCuston,
                        },
                      },
                    }}
                    legendToggle
                  />
                  <Chart
                    chartType="ColumnChart"
                    data={instructionVictim}
                    responsive={true}
                    weight="100%"
                    height="700px"
                    options={{
                      pieSliceText: "value",
                      title: "Grado de instrucción",
                      backgroundColor: "transparent",
                      titleTextStyle: {
                        color: theme.palette.text.TextCuston,
                      },
                      hAxis: {
                        title: "Grado de instrucción de la victima",
                        textStyle: {
                          color: theme.palette.text.textFields,
                        },
                      },
                      vAxis: {
                        title: "Cantidad",
                        textStyle: {
                          color: theme.palette.text.textFields,
                        },
                      },
                      legend: {
                        textStyle: {
                          color: theme.palette.text.TextCuston,
                        },
                      },
                    }}
                    legendToggle
                  />
                </Grid>
              </Grid>

              <Grid container justifyContent="center" spacing={2}>
                <Grid item xs={12} lg={3}>
                  <Button
                    variant="contained"
                    onClick={() => {
                      generate();
                      handleOpenGeneralModal();
                    }}
                  >
                    Datos generales
                  </Button>
                </Grid>
                <Grid item xs={12} lg={3}>
                  <Button
                    variant="contained"
                    onClick={() => {
                      generateVictim();
                      handleOpenVictimModal();
                    }}
                  >
                    datos de la victima
                  </Button>
                </Grid>
              </Grid>
            </>
          )
        ) : null}
      </Paper>
      <Dialog open={modalVictimStatus} onClose={handleCloseVictimModal}>
        <Grid container sx={{ p: 4 }} justifyContent="center">
          <DialogContent>
            <DialogContentText
              id="alert-dialog-description"
              sx={{ textAlign: "center" }}
            >
              Esta seguro que desea descargar la información de la victima
            </DialogContentText>
          </DialogContent>
          <Grid
            item
            xs={12}
            container
            justifyContent="space-around"
            sx={{ py: 2 }}
          >
            <Button
              onClick={handleCloseVictimModal}
              variant="contained"
              color="error"
            >
              Cancelar
            </Button>
            <PDFDownloadLink
              document={<Victim images={image} info={dataSend} />}
              fileName="InformacionVictima.pdf"
            >
              <Button
                color="success"
                variant="contained"
                onClick={() => {
                  generateVictim();
                  handleCloseVictimModal();
                }}
              >
                descargar pdf
              </Button>
            </PDFDownloadLink>
          </Grid>
        </Grid>
      </Dialog>
      <Dialog open={modalGeneralStatus} onClose={handleCloseGeneralModal}>
        <Grid container sx={{ p: 4 }} justifyContent="center">
          <DialogContent>
            <DialogContentText
              id="alert-dialog-description"
              sx={{ textAlign: "center" }}
            >
              Esta seguro que desea descargar la información general
            </DialogContentText>
          </DialogContent>
          <Grid
            item
            xs={12}
            container
            justifyContent="space-around"
            sx={{ py: 2 }}
          >
            <Button
              onClick={handleCloseGeneralModal}
              variant="contained"
              color="error"
            >
              Cancelar
            </Button>
            <PDFDownloadLink
              document={
                <NewGenerate
                  images={image}
                  crimes={generalCrimes}
                  countCrimes={countDelits}
                  info={dataSend}
                />
              }
              fileName="datosGenerales.pdf"
            >
              <Button
                color="success"
                variant="contained"
                onClick={() => {
                  generate();
                  handleCloseGeneralModal();
                }}
              >
                descargar pdf
              </Button>
            </PDFDownloadLink>
          </Grid>
        </Grid>
      </Dialog>
    </>
  );
};

export default SpecificReports;
