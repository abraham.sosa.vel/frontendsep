import { Document, Page, View, Text, Image } from "@react-pdf/renderer";
import React from "react";

export function NewGenerate({ images, crimes, countCrimes, info }) {
  const styles = {
    table: {
      display: "table",
      width: "100%",
      borderStyle: "solid",
      borderWidth: 1,
      borderRightWidth: 0,
      borderBottomWidth: 0,
      borderColor: "#e0e0e0",
    },
    tableRow1: {
      backgroundColor: "#90caf9",
      margin: "auto",
      flexDirection: "row",
    },
    tableRow: {
      margin: "auto",
      flexDirection: "row",
      backgroundColor: "#f5f5f5",
    },
    tableRow3: {
      margin: "auto",
      flexDirection: "row",
      backgroundColor: "#c5e1a5",
    },
    tableCol1: {
      width: "5%",
      borderStyle: "solid",
      borderWidth: 1,
      borderLeftWidth: 0,
      borderTopWidth: 0,
      borderColor: "#e0e0e0",
    },
    tableCol2: {
      width: "65%",
      borderStyle: "solid",
      borderWidth: 1,
      borderLeftWidth: 0,
      borderTopWidth: 0,
      borderColor: "#e0e0e0",
    },
    tableCol3: {
      width: "15%",
      borderStyle: "solid",
      borderWidth: 1,
      borderLeftWidth: 0,
      borderTopWidth: 0,
      borderColor: "#e0e0e0",
    },
    tableCol4: {
      width: "15%",
      borderStyle: "solid",
      borderWidth: 1,
      borderLeftWidth: 0,
      borderTopWidth: 0,
      borderColor: "#e0e0e0",
    },
    tableCell: {
      margin: "auto",
      fontSize: 10,
      padding: 5,
    },
  };
  return (
    <Document>
      <Page size="A4">
        <View style={{ padding: "20pt" }}>
          <Text
            style={{
              textAlign: "center",
              fontWeight: 500,
              fontSize: 12,
            }}
          >
            SERVICIO PLURINACIONAL DE ASISTENCIA A LA VICTIMA
          </Text>
          <Text
            style={{
              textAlign: "center",
              fontWeight: 300,
              marginTop: "10pt",
              fontSize: 12,
            }}
          >
            Reporte de casos
          </Text>
          <Text
            style={{
              textAlign: "center",
              fontWeight: 300,
              marginTop: "10pt",
              fontSize: 12,
            }}
          >
            Año: {info?.year}
          </Text>
          {info?.month && (
            <Text
              style={{
                textAlign: "center",
                fontWeight: 300,
                marginTop: "10pt",
                fontSize: 12,
              }}
            >
              {info?.month}
            </Text>
          )}
          <Text style={{ fontSize: 10, marginTop: 9, marginBottom: 3 }}>
            1. Reporte de casos por Departamento
          </Text>
          <Image src={images.departament} />
        </View>
        <View style={{ paddingLeft: 20, paddingRight: 20 }}>
          <Text style={{ fontSize: 10, marginTop: 9, marginBottom: 3 }}>
            2. Reporte de casos por delito
          </Text>
          <View style={styles.table}>
            <View style={styles.tableRow1}>
              <View style={styles.tableCol1}>
                <Text style={styles.tableCell}>Id</Text>
              </View>
              <View style={styles.tableCol2}>
                <Text style={styles.tableCell}>Delito</Text>
              </View>
              <View style={styles.tableCol3}>
                <Text style={styles.tableCell}>N° de casos registrados</Text>
              </View>
              <View style={styles.tableCol4}>
                <Text style={styles.tableCell}>Porcentaje</Text>
              </View>
            </View>
            {crimes?.map((e, i) => {
              if (i !== 0) {
                if (i % 2 === 0) {
                  return (
                    <View key={i} style={styles.tableRow}>
                      <View style={styles.tableCol1}>
                        <Text style={styles.tableCell}>{i}</Text>
                      </View>
                      <View style={styles.tableCol2}>
                        <Text style={styles.tableCell}>{e[0]}</Text>
                      </View>
                      <View style={styles.tableCol3}>
                        <Text style={styles.tableCell}>{e[1]}</Text>
                      </View>
                      <View style={styles.tableCol4}>
                        <Text style={styles.tableCell}>
                          {Math.floor((e[1] / countCrimes) * 100)}%
                        </Text>
                      </View>
                    </View>
                  );
                } else {
                  return (
                    <View key={i} style={styles.tableRow3}>
                      <View style={styles.tableCol1}>
                        <Text style={styles.tableCell}>{i}</Text>
                      </View>
                      <View style={styles.tableCol2}>
                        <Text style={styles.tableCell}>{e[0]}</Text>
                      </View>
                      <View style={styles.tableCol3}>
                        <Text style={styles.tableCell}>{e[1]}</Text>
                      </View>
                      <View style={styles.tableCol4}>
                        <Text style={styles.tableCell}>
                          {Math.floor((e[1] / countCrimes) * 100)}%
                        </Text>
                      </View>
                    </View>
                  );
                }
              }
            })}
            <View style={styles.tableRow1}>
              <View style={styles.tableCol1}>
                <Text style={styles.tableCell}></Text>
              </View>
              <View style={styles.tableCol2}>
                <Text style={styles.tableCell}>TOTAL</Text>
              </View>
              <View style={styles.tableCol3}>
                <Text style={styles.tableCell}>{countCrimes}</Text>
              </View>
              <View style={styles.tableCol4}>
                <Text style={styles.tableCell}>100%</Text>
              </View>
            </View>
          </View>
        </View>
      </Page>
    </Document>
  );
}
