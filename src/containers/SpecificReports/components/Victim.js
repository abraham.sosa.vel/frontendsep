import { Document, Page, View, Text, Image } from "@react-pdf/renderer";
import React from "react";

export function Victim({ images, info }) {
  return (
    <Document>
      <Page size="A4">
        <View style={{ padding: "20pt" }}>
          <Text
            style={{
              textAlign: "center",
              fontWeight: 500,
              fontSize: 12,
            }}
          >
            SERVICIO PLURINACIONAL DE ASISTENCIA A LA VICTIMA
          </Text>
          <Text
            style={{
              textAlign: "center",
              fontWeight: 300,
              marginTop: "10pt",
              fontSize: 12,
            }}
          >
            Datos de las victimas
          </Text>
          <Text
            style={{
              textAlign: "center",
              fontWeight: 300,
              marginTop: "10pt",
              fontSize: 12,
            }}
          >
            Año: {info?.year}
          </Text>
          {info?.month && (
            <Text
              style={{
                textAlign: "center",
                fontWeight: 300,
                marginTop: "10pt",
                fontSize: 12,
              }}
            >
              {info?.month}
            </Text>
          )}
          <Text style={{ fontSize: 10, marginTop: 9, marginBottom: 3 }}></Text>
          <Image src={images.victim} />
        </View>
      </Page>
      <Page size="A4">
        <View style={{ padding: "20pt" }}>
          <Text
            style={{
              textAlign: "center",
              fontWeight: 500,
              fontSize: 12,
            }}
          >
            SERVICIO PLURINACIONAL DE ASISTENCIA A LA VICTIMA
          </Text>
          <Text
            style={{
              textAlign: "center",
              fontWeight: 300,
              marginTop: "10pt",
              fontSize: 12,
            }}
          >
            Datos de las victimas
          </Text>
          <Text
            style={{
              textAlign: "center",
              fontWeight: 300,
              marginTop: "10pt",
              fontSize: 12,
            }}
          >
            Año: {info?.year}
          </Text>
          {info?.month && (
            <Text
              style={{
                textAlign: "center",
                fontWeight: 300,
                marginTop: "10pt",
                fontSize: 12,
              }}
            >
              {info?.month}
            </Text>
          )}
          <Text style={{ fontSize: 10, marginTop: 9, marginBottom: 3 }}></Text>
          <Image src={images.victim2} />
        </View>
      </Page>
    </Document>
  );
}
