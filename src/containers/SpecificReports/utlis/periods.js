export const periods = () => {
  const mensual = [
    { id: "1", value: "Enero" },
    { id: "2", value: "Febrero" },
    { id: "3", value: "Marzo" },
    { id: "4", value: "Abril" },
    { id: "5", value: "Mayo" },
    { id: "6", value: "Junio" },
    { id: "7", value: "Julio" },
    { id: "8", value: "Agosto" },
    { id: "9", value: "Septiembre" },
    { id: "10", value: "Octubre" },
    { id: "11", value: "Noviembre" },
    { id: "12", value: "Diciembre" },
  ];
  const bimestral = [
    { id: "1,2", value: "Primer bimestre" },
    { id: "3,4", value: "Segundo bimestre" },
    { id: "5,6", value: "Tercer bimestre" },
    { id: "7,8", value: "Cuarto bimestre" },
    { id: "9,10", value: "Quinto bimestre" },
    { id: "11,12", value: "Sexto bimestre" },
  ];
  const trimestral = [
    { id: "1,2,3", value: "Primer trimestre" },
    { id: "4,5,6", value: "Segundo trimestre" },
    { id: "7,8,9", value: "Tercer trimestre" },
    { id: "10,11,12", value: "Cuarto trimestre" },
  ];
  const semestral = [
    { id: "1,2,3,4,5,6", value: "Primer semestre" },
    { id: "7,8,9,10,11,12", value: "Segundo semestre" },
  ];
  return {
    mensual,
    bimestral,
    trimestral,
    semestral,
  };
};
