export const formatSpecificReports = (data) => {
  const valuesCrimes = [];
  if (data.crimeSelect.length !== 0) {
    data.crimeSelect.forEach((e) => {
      valuesCrimes.push(e.value);
    });
  }
  const info = {
    age: data.age,
    period: data.period,
    month: data.month.split(","),
    crimes: valuesCrimes,
  };
  if (info.period === "anual") {
    info.month = "";
  }
  return info;
};

export const formatView = (data) => {
  console.log(data);
  const info = {
    year: data.age.join(", "),
    month: false,
  };
  if (data.period === "mensual") {
    switch (data.month) {
      case "1":
        info.month = "Enero";
        break;
      case "2":
        info.month = "Febrero";
        break;
      case "3":
        info.month = "Marzo";
        break;
      case "4":
        info.month = "Abril";
        break;
      case "5":
        info.month = "Mayo";
        break;
      case "6":
        info.month = "Junio";
        break;
      case "7":
        info.month = "Julio";
        break;
      case "8":
        info.month = "Agosto";
        break;
      case "9":
        info.month = "Septiembre";
        break;
      case "10":
        info.month = "Octubre";
        break;
      case "11":
        info.month = "Noviembre";
        break;
      case "12":
        info.month = "Diciembre";
        break;

      default:
        break;
    }
  }
  if (data.period === "bimestral") {
    switch (data.month) {
      case "1,2":
        info.month = "Primer bimestre";
        break;
      case "3,4":
        info.month = "Segundo bimestre";
        break;
      case "5,6":
        info.month = "Tercer bimestre";
        break;
      case "7,8":
        info.month = "Cuarto bimestre";
        break;
      case "9,10":
        info.month = "Quinto bimestre";
        break;
      case "11,12":
        info.month = "Sexto bimestre";
        break;
      default:
        break;
    }
  }
  if (data.period === "trimestral") {
    switch (data.month) {
      case "1,2,3":
        info.month = "Primer trimestre";
        break;
      case "4,5,6":
        info.month = "Segundo trimestre";
        break;
      case "7,8,9":
        info.month = "Tercer trimestre";
        break;
      case "10,11,12":
        info.month = "Cuarto trimestre";
        break;
      default:
        break;
    }
  }
  if (data.period === "semestral") {
    switch (data.month) {
      case "1,2,3,4,5,6":
        info.month = "Primer semestre";
        break;
      case "7,8,9,10,11,12":
        info.month = "Segundo semestre";
        break;
      default:
        break;
    }
  }
  return info;
};
