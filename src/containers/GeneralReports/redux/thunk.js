import { messageError, messageSuccess } from "../../../redux/globalRedux";
import request, { getOptionWithToken } from "../../../utils/request";
import { generateGeneralReportsSuccess, hideLoader, showLoader } from "./redux";

export const generateGeneralReportsStart = (data) => {
  return async (dispatch, getState) => {
    try {
      const colors = [
        "#ef5350",
        "#ec407a",
        "#ab47bc",
        "#7e57c2",
        "#5c6bc0",
        "#42a5f5",
        "#29b6f6",
        "#26c6da",
        "#26a69a",
        "#66bb6a",
        "#9ccc65",
        "#d4e157",
        "#ffee58",
        "#ffca28",
        "#ffa726",
        "#ff7043",
        "#8d6e63",
      ];
      const datas = {
        years: [["Año", "Casos atendidos", { role: "style" }]],
        status: [["Etapa procesal", "Cantidad", { role: "style" }]],
        victimSex: [["Sexo", "cantidad", { role: "style" }]],
        generalCrimes: [["Delito", "cantidad"]],
        instructionVictim: [
          ["Grado de instrucción", "cantidad", { role: "style" }],
        ],
        occupationVictim: [["Ocupación", "Cantidad", { role: "style" }]],
        orientationVictim: [
          ["Orientación sexual", "cantidad", { role: "style" }],
        ],
        departament: [["Departamento", "cantidad", { role: "style" }]],
        countDelits: 0,
      };
      dispatch(showLoader());
      const url = `${process.env.REACT_APP_URL_API}/reporter/general`;
      const options = getOptionWithToken();
      const getData = await request(url, options);
      getData.years?.forEach((x) =>
        datas.years.push([
          `${x.year}`,
          parseInt(x.count),
          colors[Math.floor(Math.random() * 16) + 1],
        ])
      );
      getData.departament?.forEach((x) =>
        datas.departament.push([
          x[0],
          x.length,
          colors[Math.floor(Math.random() * 16) + 1],
        ])
      );
      getData.sexVictim?.forEach((x) =>
        datas.victimSex.push([
          x[0] ? "Masculio" : "Femenino",
          x.length,
          colors[Math.floor(Math.random() * 16) + 1],
        ])
      );
      getData.status?.forEach((x) => {
        return datas.status.push([
          x?.nombre,
          x?.etapaCasos?.length,
          colors[Math.floor(Math.random() * 16) + 1],
        ]);
      });
      dispatch(generateGeneralReportsSuccess(datas));
      dispatch(messageSuccess("Se obtuvo los datos con éxito"));
    } catch (error) {
      let message = error?.message
        ? error.message
        : "No se pudo cargar los datos";
      dispatch(messageError(message));
    } finally {
      dispatch(hideLoader());
    }
  };
};
