import { createSlice } from "@reduxjs/toolkit";
const initialState = {
  data: {},
  loaderGeneral: false,
};
export const generalReportsSlice = createSlice({
  name: "generalReports",
  initialState,
  reducers: {
    generateGeneralReportsSuccess: (state, action) => {
      state.data = action.payload;
    },
    showLoader: (state) => {
      state.loaderGeneral = true;
    },
    hideLoader: (state) => {
      state.loaderGeneral = false;
    },
  },
});

export const { generateGeneralReportsSuccess, showLoader, hideLoader } =
  generalReportsSlice.actions;
export default generalReportsSlice.reducer;
