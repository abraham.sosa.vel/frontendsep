import { useTheme } from "@emotion/react";
import { Box, Grid, Paper, Skeleton } from "@mui/material";
import React from "react";
import Chart from "react-google-charts";
import { useTranslation } from "react-i18next";
import { useSelector } from "react-redux";
import TitlePage from "../../components/TitlePage";

const GeneralReports = () => {
  const { data, loaderGeneral } = useSelector((state) => state.generalRepors);
  const theme = useTheme();
  const [t] = useTranslation("global");
  return (
    <Grid container justifyContent="space-between" spacing={2}>
      <Grid item xs={12} lg={6}>
        <Paper
          sx={{ p: 2, pt: 3 }}
          elevation={theme.palette.mode === "dark" ? 0 : 5}
        >
          <Grid sx={{ mb: 1 }}>
            <TitlePage title={t("NavItems.principalPanel").toUpperCase()} />
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              {loaderGeneral ? (
                <Box width="100%" height={200}>
                  <Skeleton
                    variant="rectangular"
                    sx={{ width: "100%", height: "100%" }}
                  />
                </Box>
              ) : (
                <Chart
                  chartType="ColumnChart"
                  data={data.victimSex}
                  responsive={true}
                  width="100%"
                  height="500px"
                  options={{
                    title: "Sexo de victimas registradas en el ultimo mes",
                    backgroundColor: "transparent",
                    titleTextStyle: {
                      color: theme.palette.text.TextCuston,
                    },
                    hAxis: {
                      textStyle: {
                        color: theme.palette.text.textFields,
                      },
                    },
                    vAxis: {
                      textStyle: {
                        color: theme.palette.text.textFields,
                      },
                    },
                    legend: {
                      textStyle: {
                        color: theme.palette.text.TextCuston,
                      },
                    },
                  }}
                  legendToggle
                />
              )}
            </Grid>
          </Grid>
        </Paper>
      </Grid>
      <Grid item xs={12} lg={6}>
        <Paper
          sx={{ p: 2, pt: 3 }}
          elevation={theme.palette.mode === "dark" ? 0 : 5}
        >
          <Grid sx={{ mb: 1 }}>
            <TitlePage title={t("NavItems.principalPanel").toUpperCase()} />
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Grid item xs={12}>
                <Chart
                  responsive={true}
                  width="100%"
                  height="500px"
                  chartType="PieChart"
                  data={data.departament}
                  options={{
                    series: {
                      0: { color: "#e2431e" },
                      1: { color: "#e7711b" },
                      2: { color: "#f1ca3a" },
                      3: { color: "#6f9654" },
                      4: { color: "#1c91c0" },
                      5: { color: "#43459d" },
                    },
                    title: "Casos atendidos por departamento en el ultimo mes",
                    backgroundColor: "transparent",
                    titleTextStyle: {
                      color: theme.palette.text.TextCuston,
                    },

                    hAxis: {
                      textStyle: {
                        color: theme.palette.text.textFields,
                      },
                    },
                    vAxis: {
                      textStyle: {
                        color: theme.palette.text.textFields,
                      },
                    },
                    legend: {
                      textStyle: {
                        color: theme.palette.text.TextCuston,
                      },
                    },
                  }}
                  legendToggle
                />
              </Grid>
            </Grid>
          </Grid>
        </Paper>
      </Grid>
      {data.years !== 1 && (
        <Grid item xs={12}>
          <Paper
            sx={{ p: 2, pt: 3 }}
            elevation={theme.palette.mode === "dark" ? 0 : 5}
          >
            <Grid sx={{ mb: 1 }}>
              <TitlePage title={t("NavItems.principalPanel").toUpperCase()} />
            </Grid>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                {loaderGeneral ? (
                  <Box width="100%" height={200}>
                    <Skeleton
                      variant="rectangular"
                      sx={{ width: "100%", height: "100%" }}
                    />
                  </Box>
                ) : (
                  <Chart
                    chartType="SteppedAreaChart"
                    data={data?.years}
                    responsive={true}
                    width="100%"
                    height="500px"
                    options={{
                      title: "Cantidad de casos atendidos por año",
                      backgroundColor: "transparent",
                      titleTextStyle: {
                        color: theme.palette.text.TextCuston,
                      },
                      hAxis: {
                        textStyle: {
                          color: theme.palette.text.textFields,
                        },
                      },
                      vAxis: {
                        textStyle: {
                          color: theme.palette.text.textFields,
                        },
                      },
                      legend: {
                        textStyle: {
                          color: theme.palette.text.TextCuston,
                        },
                      },
                    }}
                    legendToggle
                  />
                )}
              </Grid>
            </Grid>
          </Paper>
        </Grid>
      )}
      <Grid item xs={12}>
        <Paper
          sx={{ p: 2, pt: 3 }}
          elevation={theme.palette.mode === "dark" ? 0 : 5}
        >
          <Grid sx={{ mb: 1 }}>
            <TitlePage title={"ESTADO DE LOS CASOS"} />
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              {loaderGeneral ? (
                <Box width="100%" height={200}>
                  <Skeleton
                    variant="rectangular"
                    sx={{ width: "100%", height: "100%" }}
                  />
                </Box>
              ) : (
                <Chart
                  chartType="SteppedAreaChart"
                  data={data?.status}
                  responsive={true}
                  width="100%"
                  height="500px"
                  options={{
                    title: "Estado de los casos registrados",
                    backgroundColor: "transparent",
                    titleTextStyle: {
                      color: theme.palette.text.TextCuston,
                    },
                    hAxis: {
                      textStyle: {
                        color: theme.palette.text.textFields,
                      },
                    },
                    vAxis: {
                      textStyle: {
                        color: theme.palette.text.textFields,
                      },
                    },
                    legend: {
                      textStyle: {
                        color: theme.palette.text.TextCuston,
                      },
                    },
                  }}
                  legendToggle
                />
              )}
            </Grid>
          </Grid>
        </Paper>
      </Grid>
    </Grid>
  );
};

export default GeneralReports;
