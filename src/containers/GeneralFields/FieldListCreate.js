import { Search } from "@mui/icons-material";
import {
  Button,
  Grid,
  InputAdornment,
  Paper,
  TextField,
  Typography,
  useTheme,
} from "@mui/material";
import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import BaseTable from "../../components/BaseTable";
import TitlePage from "../../components/TitlePage";
import ItemFieldsTable from "./components/ItemFieldsTable";
import { getGeneralFieldFormat } from "./utils/formatGeneralField";
import { useColumnsInputs } from "./utils/useColumnsInputs";
import { createOption, useSelectOption } from "./utils/useSelectOption";
const FieldListCreate = () => {
  const theme = useTheme();
  const params = useParams();
  const [create, setCreate] = useState("");
  const [error, setError] = useState(false);
  const [sumbit, setSubmit] = useState(false);
  const [t] = useTranslation("global");
  const { getOption } = useSelectOption(params.option);
  const Fields = useSelector((state) => state.generalFields);
  const dispatch = useDispatch();
  let valueLength = 20;
  if (params.option === "crimes") {
    valueLength = 100;
  }
  const handleInput = (e) => {
    setCreate(e.target.value);
    if (e.target.value.length > valueLength) return setError(true);
    setError(false);
    if (e.target.value.length > 0 && e.target.value.length < valueLength) {
      return setSubmit(true);
    }
    setSubmit(false);
  };

  const handleCreate = () => {
    if (!sumbit) return;
    createOption(create, params.option, dispatch);
    setCreate("");
  };
  return (
    <Paper
      sx={{ p: 2, pt: 3 }}
      elevation={theme.palette.mode === "dark" ? 0 : 5}
    >
      <Grid container direction="column">
        <Grid sx={{ mb: 1 }}>
          <TitlePage title={t(`${params.option}.title`).toUpperCase()} />
        </Grid>

        {/*Container search and button*/}
        <Grid container sx={{ my: 1 }}>
          <Typography color="text.secondary">
            Puede dar click al campo nombre para cambiar el dato
          </Typography>
        </Grid>
        <Grid container justifyContent="flex-end" spacing={2}>
          <Grid item xs={12} md={3}>
            <TextField
              value={create}
              error={error}
              helperText={
                error && `no puede sobrepasar los ${valueLength} caracteres`
              }
              onChange={handleInput}
              fullWidth
              label="nombre del nuevo registro"
            />
          </Grid>
          <Grid item xs={12} md={2} container justifyContent="flex-end">
            <Button
              variant="contained"
              fullWidth
              color="success"
              sx={{ maxHeight: "3.4rem" }}
              onClick={handleCreate}
            >
              Crear registro
            </Button>
          </Grid>
        </Grid>
        {/*Table users*/}
        <BaseTable
          columns={useColumnsInputs()}
          loading={Fields.loader}
          rowsLength={6}
        >
          {getGeneralFieldFormat(Fields[getOption])?.map((e) => (
            <ItemFieldsTable key={e.id} item={e} />
          ))}
        </BaseTable>
      </Grid>
    </Paper>
  );
};

export default FieldListCreate;
