import {
  Apartment,
  Autorenew,
  CrisisAlert,
  Explore,
  FamilyRestroom,
  School,
  Wc,
  Work,
} from "@mui/icons-material";
import { Grid, Paper, Typography, useTheme } from "@mui/material";
import React from "react";
import { useTranslation } from "react-i18next";
import BaseButtonInput from "../../components/BaseButtonInput";
import TitlePage from "../../components/TitlePage";

const GeneralFields = () => {
  const theme = useTheme();
  const [t] = useTranslation("global");

  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <Paper
          sx={{ p: 2, pt: 3 }}
          elevation={theme.palette.mode === "dark" ? 0 : 5}
        >
          <Grid container direction="column">
            <Grid item sx={{ mb: 1, textAlign: "center" }}>
              <TitlePage title={t("NavItems.generalFields").toUpperCase()} />
            </Grid>
            <Grid item sx={{ mb: 1, textAlign: "center" }}>
              <Typography variant="subtitle1" sx={{ opacity: ".9" }}>
                Mediante este apartado podrá seleccionar entre los diferentes
                listados que se manejan para ver el detalle de los campos y
                poder crear, editar, actualizar y eliminar un dato
              </Typography>
            </Grid>
          </Grid>
        </Paper>
      </Grid>
      <Grid item xs={12} sm={6} md={4} lg={3}>
        <BaseButtonInput
          title="OCUPACIONES"
          description="Ocupaciones que puede tener una victima, victimario o denunciante"
          color="green"
          redirect="/generalFields/occupation"
        >
          <Work
            sx={{
              width: "50%",
              height: "50%",
            }}
          />
        </BaseButtonInput>
      </Grid>
      <Grid item xs={12} sm={6} md={4} lg={3}>
        <BaseButtonInput
          title="ORIENTACIONES SEXUALES"
          description="La orientación sexual que puede tener una victima, victimario o denunciante"
          color="orange"
          redirect="/generalFields/sexualOrientation"
        >
          <Wc
            sx={{
              width: "50%",
              height: "50%",
            }}
          />
        </BaseButtonInput>
      </Grid>
      <Grid item xs={12} sm={6} md={4} lg={3}>
        <BaseButtonInput
          title="GRADOS DE INSTRUCCIÓN"
          description="El ultimo grado de instrucción que obtuvo la victima, victimario o denunciante"
          color="teal"
          redirect="/generalFields/instructionDegree"
        >
          <School
            sx={{
              width: "50%",
              height: "50%",
            }}
          />
        </BaseButtonInput>
      </Grid>
      <Grid item xs={12} sm={6} md={4} lg={3}>
        <BaseButtonInput
          title="RELACIÓN CON LA VICTIMA"
          description="La relación con la victima que pude tener un denunciante o un denunciado"
          color="lime"
          redirect="/generalFields/victimRelationship"
        >
          <FamilyRestroom
            sx={{
              width: "50%",
              height: "50%",
            }}
          />
        </BaseButtonInput>
      </Grid>
      <Grid item xs={12} sm={6} md={4} lg={3}>
        <BaseButtonInput
          title="DELITOS"
          description="Los diferentes delitos que se pueden cometer a una victima"
          color="red"
          redirect="/generalFields/crimes"
        >
          <CrisisAlert
            sx={{
              width: "50%",
              height: "50%",
            }}
          />
        </BaseButtonInput>
      </Grid>
      <Grid item xs={12} sm={6} md={4} lg={3}>
        <BaseButtonInput
          title="ETAPA DE LOS CASOS"
          description="Las diferentes etapas por las que puede pasar un caso registrado"
          color="cyan"
          redirect="/generalFields/proceduralStage"
        >
          <Autorenew
            sx={{
              width: "50%",
              height: "50%",
            }}
          />
        </BaseButtonInput>
      </Grid>
      <Grid item xs={12} sm={6} md={4} lg={3}>
        <BaseButtonInput
          title="SUCURSALES DEL SEPDAVI"
          description="Las diferentes ciudades donde se encuentra el SEPDAVI y pueden pertenecer los usuarios"
          color="lightBlue"
          redirect="/generalFields/branchesOffices"
        >
          <Apartment
            sx={{
              width: "50%",
              height: "50%",
            }}
          />
        </BaseButtonInput>
      </Grid>
      <Grid item xs={12} sm={6} md={4} lg={3}>
        <BaseButtonInput
          title="CIUDADES"
          description="Las diferentes ciudades donde se pudo haber cometido un delito"
          color="amber"
          redirect="/generalFields/cities"
        >
          <Explore
            sx={{
              width: "50%",
              height: "50%",
            }}
          />
        </BaseButtonInput>
      </Grid>
    </Grid>
  );
};

export default GeneralFields;
