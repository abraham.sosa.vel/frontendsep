export const getGeneralFieldFormat = (data = []) => {
  const send = data?.map((e) => {
    return {
      id: e?.id,
      name: e?.nombre,
      count: e?.count.length,
      initDate: e?.createdAt,
      updateDate: e?.updatedAt,
    };
  });
  return send;
};
