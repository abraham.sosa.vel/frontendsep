import { useTranslation } from "react-i18next";
import createColumns from "../../../components/BaseTable/createColumns";

export const useColumnsInputs = () => {
  const [t] = useTranslation("global");
  return [
    createColumns({ id: "id", title: t("tableInputs.id") }),
    createColumns({ id: "name", title: t("tableInputs.name") }),
    createColumns({ id: "registers", title: t("tableInputs.registers") }),
    createColumns({ id: "initDate", title: t("tableInputs.initDate") }),
    createColumns({ id: "updateDate", title: t("tableInputs.updateDate") }),
    createColumns({ id: "delete", title: t("global.delete") }),
  ];
};
