import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import {
  createBranchesOffices,
  createCitiesStart,
  createCrimesStart,
  createInstructionDegreeStart,
  createOccupationStart,
  createProceduralStageStart,
  createSexualOrientationStart,
  createVictimRelationship,
  deleteBranchesOffices,
  deleteCitiesStart,
  deleteCrimesStart,
  deleteInstructionDegreeStart,
  deleteOccupationStart,
  deleteProceduralStageStart,
  deleteSexualOrientationStart,
  deleteVictimRelationship,
  getbranchesOffices,
  getCities,
  getCrimesStart,
  getInstructionDegreeStart,
  getOccupationStart,
  getProceduralStage,
  getSexualOrientationStart,
  getVictimRelationship,
  updateBranchesOffices,
  updateCitiesStart,
  updateCrimesStart,
  updateInstructionDegreeStart,
  updateOccupationStart,
  updateProceduralStageStart,
  updateSexualOrientationStart,
  updateVictimRelationship,
} from "../redux/thunk";

export const useSelectOption = (type) => {
  const dispatch = useDispatch();
  const [getOption, setOption] = useState(false);
  useEffect(() => {
    if (type === "occupation") {
      setOption(type);
      dispatch(getOccupationStart());
    }
    if (type === "sexualOrientation") {
      setOption(type);
      dispatch(getSexualOrientationStart());
    }
    if (type === "instructionDegree") {
      setOption(type);
      dispatch(getInstructionDegreeStart());
    }
    if (type === "victimRelationship") {
      setOption(type);
      dispatch(getVictimRelationship());
    }
    if (type === "crimes") {
      setOption(type);
      dispatch(getCrimesStart());
    }
    if (type === "branchesOffices") {
      setOption(type);
      dispatch(getbranchesOffices());
    }
    if (type === "proceduralStage") {
      setOption(type);
      dispatch(getProceduralStage());
    }
    if (type === "cities") {
      setOption(type);
      dispatch(getCities());
    }
  }, [type, dispatch]);
  return { getOption };
};

export const dispatchOption = (id, name, type, dispatch) => {
  if (type === "occupation") {
    dispatch(updateOccupationStart(id, name));
  }
  if (type === "sexualOrientation") {
    dispatch(updateSexualOrientationStart(id, name));
  }
  if (type === "instructionDegree") {
    dispatch(updateInstructionDegreeStart(id, name));
  }
  if (type === "victimRelationship") {
    dispatch(updateVictimRelationship(id, name));
  }
  if (type === "crimes") {
    dispatch(updateCrimesStart(id, name));
  }
  if (type === "proceduralStage") {
    dispatch(updateProceduralStageStart(id, name));
  }
  if (type === "cities") {
    dispatch(updateCitiesStart(id, name));
  }
  if (type === "branchesOffices") {
    dispatch(updateBranchesOffices(id, name));
  }
};

export const deleteOption = (id, type, dispatch) => {
  if (type === "occupation") {
    dispatch(deleteOccupationStart(id));
  }
  if (type === "sexualOrientation") {
    dispatch(deleteSexualOrientationStart(id));
  }
  if (type === "instructionDegree") {
    dispatch(deleteInstructionDegreeStart(id));
  }
  if (type === "victimRelationship") {
    dispatch(deleteVictimRelationship(id));
  }
  if (type === "crimes") {
    dispatch(deleteCrimesStart(id));
  }
  if (type === "branchesOffices") {
    dispatch(deleteBranchesOffices(id));
  }
  if (type === "proceduralStage") {
    dispatch(deleteProceduralStageStart(id));
  }
  if (type === "cities") {
    dispatch(deleteCitiesStart(id));
  }
};
export const createOption = (data, type, dispatch) => {
  if (type === "occupation") {
    dispatch(createOccupationStart(data));
  }
  if (type === "sexualOrientation") {
    dispatch(createSexualOrientationStart(data));
  }
  if (type === "instructionDegree") {
    dispatch(createInstructionDegreeStart(data));
  }
  if (type === "victimRelationship") {
    dispatch(createVictimRelationship(data));
  }
  if (type === "crimes") {
    dispatch(createCrimesStart(data));
  }
  if (type === "branchesOffices") {
    dispatch(createBranchesOffices(data));
  }
  if (type === "proceduralStage") {
    dispatch(createProceduralStageStart(data));
  }
  if (type === "cities") {
    dispatch(createCitiesStart(data));
  }
};
