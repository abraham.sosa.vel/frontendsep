import { messageError, messageSuccess } from "../../../redux/globalRedux";
import request, {
  deleteOptionWithToken,
  getOptionWithoutToken,
  postOptionWithToken,
  putOptionWithToken,
} from "../../../utils/request";
import {
  getBranchesOfficesSuccess,
  getCitiesSuccess,
  getCrimesSuccess,
  getInstructionDegreeSuccess,
  getOccupatioSuccess,
  getProceduralStageSuccess,
  getSexualOrientationSuccess,
  getVictimRelationshipSuccess,
  hideLoader,
  showLoader,
} from "./redux";

//ocupacion
export const getOccupationStart = () => {
  return async (dispatch, getState) => {
    const { occupation } = getState().generalFields;
    try {
      dispatch(showLoader());
      if (occupation.length > 0) return;
      let url = `${process.env.REACT_APP_URL_API}/occupation`;
      let options = getOptionWithoutToken();
      let occupationsDegree = await request(url, options);
      dispatch(getOccupatioSuccess(occupationsDegree));
    } catch (error) {
      let message = error?.message
        ? error.message
        : "No se pudo cargar los datos";
      dispatch(messageError(message));
    } finally {
      dispatch(hideLoader());
    }
  };
};
export const getSexualOrientationStart = () => {
  return async (dispatch, getState) => {
    const { sexualOrientation } = getState().generalFields;
    try {
      dispatch(showLoader());
      if (sexualOrientation.length > 0) return;
      let url = `${process.env.REACT_APP_URL_API}/sexualOrientation`;
      let options = getOptionWithoutToken();
      let data = await request(url, options);
      dispatch(getSexualOrientationSuccess(data));
    } catch (error) {
      let message = error?.message
        ? error.message
        : "No se pudo cargar los datos";
      dispatch(messageError(message));
    } finally {
      dispatch(hideLoader());
    }
  };
};
//instruccion degree
export const getInstructionDegreeStart = () => {
  return async (dispatch, getState) => {
    const { instructionDegree } = getState().generalFields;
    try {
      dispatch(showLoader());
      if (instructionDegree.length > 0) return;
      let url = `${process.env.REACT_APP_URL_API}/instructionDegree`;
      let options = getOptionWithoutToken();
      let data = await request(url, options);
      dispatch(getInstructionDegreeSuccess(data));
    } catch (error) {
      let message = error?.message
        ? error.message
        : "No se pudo cargar los datos";
      dispatch(messageError(message));
    } finally {
      dispatch(hideLoader());
    }
  };
};
export const deleteInstructionDegreeStart = (id) => {
  return async (dispatch, getState) => {
    try {
      const { instructionDegree } = getState().generalFields;
      const format = instructionDegree.map((e) => {
        return {
          ...e,
          personas: e.count,
        };
      });
      let url = `${process.env.REACT_APP_URL_API}/instructionDegree/${id}`;
      let options = deleteOptionWithToken();
      let data = await request(url, options);
      dispatch(getInstructionDegreeSuccess(format.filter((e) => e.id !== id)));
      dispatch(messageSuccess("Se elimino con éxito"));
    } catch (error) {
      let message = error?.message ? error.message : "No se pudo eliminar";
      dispatch(messageError(message));
    } finally {
    }
  };
};
export const createInstructionDegreeStart = (name) => {
  return async (dispatch, getState) => {
    try {
      const { instructionDegree } = getState().generalFields;
      const format = instructionDegree.map((e) => {
        return {
          ...e,
          personas: e.count,
        };
      });
      let url = `${process.env.REACT_APP_URL_API}/instructionDegree`;
      let options = postOptionWithToken({ nombre: name });
      let data = await request(url, options);
      dispatch(getInstructionDegreeSuccess([...format, data]));
      dispatch(messageSuccess("Se creo con éxito"));
    } catch (error) {
      let message = error?.message ? error.message : "No se pudo crear";
      dispatch(messageError(message));
    } finally {
    }
  };
};
export const updateInstructionDegreeStart = (id, name) => {
  return async (dispatch, getState) => {
    try {
      const { instructionDegree } = getState().generalFields;
      const format = instructionDegree.map((e) => {
        return {
          ...e,
          personas: e.count,
        };
      });
      let url = `${process.env.REACT_APP_URL_API}/instructionDegree/${id}`;
      let options = putOptionWithToken({ name });
      let data = await request(url, options);
      dispatch(
        getInstructionDegreeSuccess(
          format.map((e) => (e.id === data.id ? data : e))
        )
      );
      dispatch(messageSuccess("Se actualizo con éxito"));
    } catch (error) {
      let message = error?.message ? error.message : "No se pudo actualizar";
      dispatch(messageError(message));
    } finally {
    }
  };
};
//crimes
export const getCrimesStart = () => {
  return async (dispatch, getState) => {
    const { crimes } = getState().generalFields;
    try {
      dispatch(showLoader());
      if (crimes.length > 0) return;
      let url = `${process.env.REACT_APP_URL_API}/crimes`;
      let options = getOptionWithoutToken();
      let data = await request(url, options);
      dispatch(getCrimesSuccess(data));
    } catch (error) {
      let message = error?.message
        ? error.message
        : "No se pudo cargar los datos";
      dispatch(messageError(message));
    } finally {
      dispatch(hideLoader());
    }
  };
};
export const deleteCrimesStart = (id) => {
  return async (dispatch, getState) => {
    try {
      const { crimes } = getState().generalFields;
      const format = crimes.map((e) => {
        return {
          ...e,
          registroDelitos: e.count,
        };
      });
      let url = `${process.env.REACT_APP_URL_API}/crimes/${id}`;
      let options = deleteOptionWithToken();
      let data = await request(url, options);
      dispatch(getCrimesSuccess(format.filter((e) => e.id !== id)));
      dispatch(messageSuccess("Se elimino con éxito"));
    } catch (error) {
      let message = error?.message ? error.message : "No se pudo eliminar";
      dispatch(messageError(message));
    } finally {
    }
  };
};
export const createCrimesStart = (name) => {
  return async (dispatch, getState) => {
    try {
      const { crimes } = getState().generalFields;
      const format = crimes.map((e) => {
        return {
          ...e,
          registroDelitos: e.count,
        };
      });
      let url = `${process.env.REACT_APP_URL_API}/crimes`;
      let options = postOptionWithToken({ nombre: name });
      let data = await request(url, options);
      dispatch(getCrimesSuccess([...format, data]));
      dispatch(messageSuccess("Se creo con éxito"));
    } catch (error) {
      let message = error?.message ? error.message : "No se pudo crear";
      dispatch(messageError(message));
    } finally {
    }
  };
};
export const updateCrimesStart = (id, name) => {
  return async (dispatch, getState) => {
    try {
      const { crimes } = getState().generalFields;
      const format = crimes.map((e) => {
        return {
          ...e,
          registroDelitos: e.count,
        };
      });
      let url = `${process.env.REACT_APP_URL_API}/crimes/${id}`;
      let options = putOptionWithToken({ name });
      let data = await request(url, options);
      dispatch(
        getCrimesSuccess(format.map((e) => (e.id === data.id ? data : e)))
      );
      dispatch(messageSuccess("Se actualizo con éxito"));
    } catch (error) {
      let message = error?.message ? error.message : "No se pudo actualizar";
      dispatch(messageError(message));
    } finally {
    }
  };
};
//proceduralStage
export const getProceduralStage = () => {
  return async (dispatch, getState) => {
    const { proceduralStage } = getState().generalFields;
    try {
      dispatch(showLoader());
      if (proceduralStage.length > 0) return;
      let url = `${process.env.REACT_APP_URL_API}/proceduralStage`;
      let options = getOptionWithoutToken();
      let data = await request(url, options);
      dispatch(getProceduralStageSuccess(data));
    } catch (error) {
      let message = error?.message
        ? error.message
        : "No se pudo cargar los datos";
      dispatch(messageError(message));
    } finally {
      dispatch(hideLoader());
    }
  };
};
//VICTIMrELATION
export const getVictimRelationship = () => {
  return async (dispatch, getState) => {
    const { victimRelationship } = getState().generalFields;
    try {
      dispatch(showLoader());
      if (victimRelationship.length > 0) return;
      let url = `${process.env.REACT_APP_URL_API}/victimRelationship`;
      let options = getOptionWithoutToken();
      let data = await request(url, options);
      dispatch(getVictimRelationshipSuccess(data));
    } catch (error) {
      let message = error?.message
        ? error.message
        : "No se pudo cargar los datos";
      dispatch(messageError(message));
    } finally {
      dispatch(hideLoader());
    }
  };
};
export const deleteVictimRelationship = (id) => {
  return async (dispatch, getState) => {
    try {
      const { victimRelationship } = getState().generalFields;
      const format = victimRelationship.map((e) => {
        return {
          ...e,
          personas: e.count,
        };
      });
      let url = `${process.env.REACT_APP_URL_API}/victimRelationship/${id}`;
      let options = deleteOptionWithToken();
      let data = await request(url, options);
      dispatch(getVictimRelationshipSuccess(format.filter((e) => e.id !== id)));
      dispatch(messageSuccess("Se elimino con éxito"));
    } catch (error) {
      let message = error?.message ? error.message : "No se pudo eliminar";
      dispatch(messageError(message));
    } finally {
    }
  };
};
export const createVictimRelationship = (name) => {
  return async (dispatch, getState) => {
    try {
      const { victimRelationship } = getState().generalFields;
      const format = victimRelationship.map((e) => {
        return {
          ...e,
          personas: e.count,
        };
      });
      let url = `${process.env.REACT_APP_URL_API}/victimRelationship`;
      let options = postOptionWithToken({ nombre: name });
      let data = await request(url, options);
      dispatch(getVictimRelationshipSuccess([...format, data]));
      dispatch(messageSuccess("Se creo con éxito"));
    } catch (error) {
      let message = error?.message ? error.message : "No se pudo crear";
      dispatch(messageError(message));
    } finally {
    }
  };
};
export const updateVictimRelationship = (id, name) => {
  return async (dispatch, getState) => {
    try {
      const { victimRelationship } = getState().generalFields;

      let url = `${process.env.REACT_APP_URL_API}/victimRelationship/${id}`;
      let options = putOptionWithToken({ name });
      let data = await request(url, options);
      dispatch(
        getVictimRelationshipSuccess(
          victimRelationship.map((e) => (e.id === data.id ? data : e))
        )
      );
      dispatch(messageSuccess("Se actualizo con éxito"));
    } catch (error) {
      let message = error?.message ? error.message : "No se pudo actualizar";
      dispatch(messageError(message));
    } finally {
    }
  };
};
//oficinas
export const getbranchesOffices = () => {
  return async (dispatch, getState) => {
    const { branchesOffices } = getState().generalFields;
    try {
      dispatch(showLoader());
      if (branchesOffices.length > 0) return;
      let url = `${process.env.REACT_APP_URL_API}/branchesOffices`;
      let options = getOptionWithoutToken();
      let data = await request(url, options);
      dispatch(getBranchesOfficesSuccess(data));
    } catch (error) {
      let message = error?.message
        ? error.message
        : "No se pudo cargar los datos";
      dispatch(messageError(message));
    } finally {
      dispatch(hideLoader());
    }
  };
};
export const updateBranchesOffices = (id, name) => {
  return async (dispatch, getState) => {
    try {
      const { branchesOffices } = getState().generalFields;
      const format = branchesOffices.map((e) => {
        return {
          ...e,
          usuarios: e.count,
        };
      });
      let url = `${process.env.REACT_APP_URL_API}/branchesOffices/${id}`;
      let options = putOptionWithToken({ name });
      let data = await request(url, options);
      dispatch(
        getBranchesOfficesSuccess(
          format.map((e) => (e.id === data.id ? data : e))
        )
      );
      dispatch(messageSuccess("Se actualizo con éxito"));
    } catch (error) {
      let message = error?.message ? error.message : "No se pudo actualizar";
      dispatch(messageError(message));
    } finally {
    }
  };
};
export const deleteBranchesOffices = (id) => {
  return async (dispatch, getState) => {
    try {
      const { branchesOffices } = getState().generalFields;
      const format = branchesOffices.map((e) => {
        return {
          ...e,
          usuarios: e.count,
        };
      });
      let url = `${process.env.REACT_APP_URL_API}/branchesOffices/${id}`;
      let options = deleteOptionWithToken();
      let data = await request(url, options);
      dispatch(getBranchesOfficesSuccess(format.filter((e) => e.id !== id)));
      dispatch(messageSuccess("Se elimino con éxito"));
    } catch (error) {
      let message = error?.message ? error.message : "No se pudo eliminar";
      dispatch(messageError(message));
    } finally {
    }
  };
};
export const createBranchesOffices = (name) => {
  return async (dispatch, getState) => {
    try {
      const { branchesOffices } = getState().generalFields;
      const format = branchesOffices.map((e) => {
        return {
          ...e,
          usuarios: e.count,
        };
      });
      let url = `${process.env.REACT_APP_URL_API}/branchesOffices`;
      let options = postOptionWithToken({ nombre: name });
      let data = await request(url, options);
      dispatch(getBranchesOfficesSuccess([...format, data]));
      dispatch(messageSuccess("Se creo con éxito"));
    } catch (error) {
      let message = error?.message ? error.message : "No se pudo crear";
      dispatch(messageError(message));
    } finally {
    }
  };
};
//cities
export const getCities = () => {
  return async (dispatch, getState) => {
    const { cities } = getState().generalFields;
    try {
      dispatch(showLoader());
      if (cities.length > 0) return;
      let url = `${process.env.REACT_APP_URL_API}/cities`;
      let options = getOptionWithoutToken();
      let data = await request(url, options);
      dispatch(getCitiesSuccess(data));
    } catch (error) {
      let message = error?.message
        ? error.message
        : "No se pudo cargar los datos";
      dispatch(messageError(message));
    } finally {
      dispatch(hideLoader());
    }
  };
};
export const updateCitiesStart = (id, name) => {
  return async (dispatch, getState) => {
    try {
      const { cities } = getState().generalFields;
      const format = cities.map((e) => {
        return {
          ...e,
          registros: e.count,
        };
      });
      let url = `${process.env.REACT_APP_URL_API}/cities/${id}`;
      let options = putOptionWithToken({ name });
      let data = await request(url, options);
      dispatch(
        getCitiesSuccess(format.map((e) => (e.id === data.id ? data : e)))
      );
      dispatch(messageSuccess("Se actualizo con éxito"));
    } catch (error) {
      let message = error?.message ? error.message : "No se pudo actualizar";
      dispatch(messageError(message));
    } finally {
    }
  };
};
export const deleteCitiesStart = (id) => {
  return async (dispatch, getState) => {
    try {
      const { cities } = getState().generalFields;
      const format = cities.map((e) => {
        return {
          ...e,
          registros: e.count,
        };
      });
      let url = `${process.env.REACT_APP_URL_API}/cities/${id}`;
      let options = deleteOptionWithToken();
      let data = await request(url, options);
      dispatch(getCitiesSuccess(format.filter((e) => e.id !== id)));
      dispatch(messageSuccess("Se elimino con éxito"));
    } catch (error) {
      let message = error?.message ? error.message : "No se pudo eliminar";
      dispatch(messageError(message));
    } finally {
    }
  };
};
export const createCitiesStart = (name) => {
  return async (dispatch, getState) => {
    try {
      const { cities } = getState().generalFields;
      const format = cities.map((e) => {
        return {
          ...e,
          registros: e.count,
        };
      });
      let url = `${process.env.REACT_APP_URL_API}/cities`;
      let options = postOptionWithToken({ nombre: name });
      let data = await request(url, options);
      dispatch(getCitiesSuccess([...format, data]));
      dispatch(messageSuccess("Se creo con éxito"));
    } catch (error) {
      let message = error?.message ? error.message : "No se pudo crear";
      dispatch(messageError(message));
    } finally {
    }
  };
};

//procedural
export const updateProceduralStageStart = (id, name) => {
  return async (dispatch, getState) => {
    try {
      const { proceduralStage } = getState().generalFields;
      const format = proceduralStage.map((e) => {
        return {
          ...e,
          etapaCasos: e.count,
        };
      });
      let url = `${process.env.REACT_APP_URL_API}/proceduralStage/${id}`;
      let options = putOptionWithToken({ name });
      let data = await request(url, options);
      dispatch(
        getProceduralStageSuccess(
          format.map((e) => (e.id === data.id ? data : e))
        )
      );
      dispatch(messageSuccess("Se actualizo con éxito"));
    } catch (error) {
      let message = error?.message ? error.message : "No se pudo actualizar";
      dispatch(messageError(message));
    } finally {
    }
  };
};
export const deleteProceduralStageStart = (id) => {
  return async (dispatch, getState) => {
    try {
      const { proceduralStage } = getState().generalFields;
      const format = proceduralStage.map((e) => {
        return {
          ...e,
          etapaCasos: e.count,
        };
      });
      let url = `${process.env.REACT_APP_URL_API}/proceduralStage/${id}`;
      let options = deleteOptionWithToken();
      let data = await request(url, options);
      dispatch(getProceduralStageSuccess(format.filter((e) => e.id !== id)));
      dispatch(messageSuccess("Se elimino con éxito"));
    } catch (error) {
      let message = error?.message ? error.message : "No se pudo eliminar";
      dispatch(messageError(message));
    } finally {
    }
  };
};
export const createProceduralStageStart = (name) => {
  return async (dispatch, getState) => {
    try {
      const { proceduralStage } = getState().generalFields;
      const format = proceduralStage.map((e) => {
        return {
          ...e,
          etapaCasos: e.count,
        };
      });
      let url = `${process.env.REACT_APP_URL_API}/proceduralStage`;
      let options = postOptionWithToken({ nombre: name });
      let data = await request(url, options);
      dispatch(getProceduralStageSuccess([...format, data]));
      dispatch(messageSuccess("Se creo con éxito"));
    } catch (error) {
      let message = error?.message ? error.message : "No se pudo crear";
      dispatch(messageError(message));
    } finally {
    }
  };
};

//ocupation
export const updateOccupationStart = (id, name) => {
  return async (dispatch, getState) => {
    try {
      const { occupation } = getState().generalFields;
      const format = occupation.map((e) => {
        return {
          ...e,
          personas: e.count,
        };
      });
      let url = `${process.env.REACT_APP_URL_API}/occupation/${id}`;
      let options = putOptionWithToken({ name });
      let data = await request(url, options);
      dispatch(
        getOccupatioSuccess(format.map((e) => (e.id === data.id ? data : e)))
      );
      dispatch(messageSuccess("Se actualizo con éxito"));
    } catch (error) {
      let message = error?.message ? error.message : "No se pudo actualizar";
      dispatch(messageError(message));
    } finally {
    }
  };
};

export const deleteOccupationStart = (id) => {
  return async (dispatch, getState) => {
    try {
      const { occupation } = getState().generalFields;
      const format = occupation.map((e) => {
        return {
          ...e,
          personas: e.count,
        };
      });
      let url = `${process.env.REACT_APP_URL_API}/occupation/${id}`;
      let options = deleteOptionWithToken();
      let data = await request(url, options);
      dispatch(getOccupatioSuccess(format.filter((e) => e.id !== id)));
      dispatch(messageSuccess("Se actualizo con éxito"));
    } catch (error) {
      let message = error?.message ? error.message : "No se pudo eliminar";
      dispatch(messageError(message));
    } finally {
    }
  };
};
export const createOccupationStart = (name) => {
  return async (dispatch, getState) => {
    try {
      const { occupation } = getState().generalFields;
      const format = occupation.map((e) => {
        return {
          ...e,
          personas: e.count,
        };
      });
      let url = `${process.env.REACT_APP_URL_API}/occupation`;
      let options = postOptionWithToken({ nombre: name });
      let data = await request(url, options);
      dispatch(getOccupatioSuccess([...format, data]));
      dispatch(messageSuccess("Se creo con éxito"));
    } catch (error) {
      let message = error?.message ? error.message : "No se pudo crear";
      dispatch(messageError(message));
    } finally {
    }
  };
};

export const deleteSexualOrientationStart = (id) => {
  return async (dispatch, getState) => {
    try {
      const { sexualOrientation } = getState().generalFields;
      const format = sexualOrientation.map((e) => {
        return {
          ...e,
          personas: e.count,
        };
      });
      let url = `${process.env.REACT_APP_URL_API}/sexualOrientation/${id}`;
      let options = deleteOptionWithToken();
      let data = await request(url, options);
      dispatch(getSexualOrientationSuccess(format.filter((e) => e.id !== id)));
      dispatch(messageSuccess("Se elimino con éxito"));
    } catch (error) {
      let message = error?.message ? error.message : "No se pudo eliminar";
      dispatch(messageError(message));
    } finally {
    }
  };
};
export const createSexualOrientationStart = (name) => {
  return async (dispatch, getState) => {
    try {
      const { sexualOrientation } = getState().generalFields;
      const format = sexualOrientation.map((e) => {
        return {
          ...e,
          personas: e.count,
        };
      });
      let url = `${process.env.REACT_APP_URL_API}/sexualOrientation`;
      let options = postOptionWithToken({ nombre: name });
      let data = await request(url, options);
      dispatch(getSexualOrientationSuccess([...format, data]));
      dispatch(messageSuccess("Se creo con éxito"));
    } catch (error) {
      let message = error?.message ? error.message : "No se pudo crear";
      dispatch(messageError(message));
    } finally {
    }
  };
};
export const updateSexualOrientationStart = (id, name) => {
  return async (dispatch, getState) => {
    try {
      const { sexualOrientation } = getState().generalFields;
      const format = sexualOrientation.map((e) => {
        return {
          ...e,
          personas: e.count,
        };
      });
      let url = `${process.env.REACT_APP_URL_API}/sexualOrientation/${id}`;
      let options = putOptionWithToken({ name });
      let data = await request(url, options);
      dispatch(
        getSexualOrientationSuccess(
          format.map((e) => (e.id === data.id ? data : e))
        )
      );
      dispatch(messageSuccess("Se actualizo con éxito"));
    } catch (error) {
      let message = error?.message ? error.message : "No se pudo actualizar";
      dispatch(messageError(message));
    } finally {
    }
  };
};
