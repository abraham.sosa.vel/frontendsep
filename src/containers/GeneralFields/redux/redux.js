import { createSlice } from "@reduxjs/toolkit";
const initialState = {
  occupation: [],
  sexualOrientation: [],
  instructionDegree: [],
  victimRelationship: [],
  crimes: [],
  proceduralStage: [],
  branchesOffices: [],
  cities: [],
  loader: false,
};
export const generalFieldSlice = createSlice({
  name: "generalFields",
  initialState,
  reducers: {
    getOccupatioSuccess: (state, action) => {
      state.occupation = action.payload
        .map((e) => {
          return {
            createdAt: e.createdAt,
            id: e.id,
            nombre: e.nombre,
            count: e.personas,
            updatedAt: e.updatedAt,
          };
        })
        .sort(function (a, b) {
          return b.id - a.id;
        });
    },
    getSexualOrientationSuccess: (state, action) => {
      state.sexualOrientation = action.payload
        .map((e) => {
          return {
            createdAt: e.createdAt,
            id: e.id,
            nombre: e.nombre,
            count: e.personas,
            updatedAt: e.updatedAt,
          };
        })
        .sort(function (a, b) {
          return b.id - a.id;
        });
    },
    getInstructionDegreeSuccess: (state, action) => {
      state.instructionDegree = action.payload
        .map((e) => {
          return {
            createdAt: e.createdAt,
            id: e.id,
            nombre: e.nombre,
            count: e.personas,
            updatedAt: e.updatedAt,
          };
        })
        .sort(function (a, b) {
          return b.id - a.id;
        });
    },
    getVictimRelationshipSuccess: (state, action) => {
      state.victimRelationship = action.payload
        .map((e) => {
          return {
            createdAt: e.createdAt,
            id: e.id,
            nombre: e.nombre,
            count: e.count
              ? e.count
              : [...e.informacionDenunciados, ...e.informacionDenunciantes],
            updatedAt: e.updatedAt,
          };
        })
        .sort(function (a, b) {
          return b.id - a.id;
        });
    },
    getCrimesSuccess: (state, action) => {
      state.crimes = action.payload
        .map((e) => {
          return {
            createdAt: e.createdAt,
            id: e.id,
            nombre: e.nombre,
            count: e.registroDelitos,
            updatedAt: e.updatedAt,
          };
        })
        .sort(function (a, b) {
          return b.id - a.id;
        });
    },
    getProceduralStageSuccess: (state, action) => {
      state.proceduralStage = action.payload
        .map((e) => {
          return {
            createdAt: e.createdAt,
            id: e.id,
            nombre: e.nombre,
            count: e.etapaCasos,
            updatedAt: e.updatedAt,
          };
        })
        .sort(function (a, b) {
          return b.id - a.id;
        });
    },
    getBranchesOfficesSuccess: (state, action) => {
      state.branchesOffices = action.payload
        .map((e) => {
          return {
            createdAt: e.createdAt,
            id: e.id,
            nombre: e.nombre,
            count: e.usuarios,
            updatedAt: e.updatedAt,
          };
        })
        .sort(function (a, b) {
          return b.id - a.id;
        });
    },
    getCitiesSuccess: (state, action) => {
      state.cities = action.payload
        .map((e) => {
          return {
            createdAt: e.createdAt,
            id: e.id,
            nombre: e.nombre,
            count: e.registros,
            updatedAt: e.updatedAt,
          };
        })
        .sort(function (a, b) {
          return b.id - a.id;
        });
    },
    showLoader: (state) => {
      state.loader = true;
    },
    hideLoader: (state) => {
      state.loader = false;
    },
  },
});

export const {
  getCitiesSuccess,
  getOccupatioSuccess,
  getSexualOrientationSuccess,
  getInstructionDegreeSuccess,
  getVictimRelationshipSuccess,
  getCrimesSuccess,
  getProceduralStageSuccess,
  getBranchesOfficesSuccess,
  showLoader,
  hideLoader,
} = generalFieldSlice.actions;
export default generalFieldSlice.reducer;
