import { TableCell, TextField } from "@mui/material";
import { useEffect, useState } from "react";
import { useParams } from "react-router";

function LoadOrderCustomizablDispatch({
  open,
  handleOpen,
  handleClose: onClose,
  value,
}) {
  const [valueInput, setValueInput] = useState("");
  const [error, setError] = useState(false);
  const params = useParams();

  const handleClose = () => {
    if (error) return;
    onClose(valueInput);
  };
  useEffect(() => {
    setValueInput(value);
  }, [open]);
  let valueLength = 20;
  if (params.option === "crimes") {
    valueLength = 100;
  }
  const changeValue = (e) => {
    setValueInput(e.target.value);
    if (e.target.value === "") {
      return setError(true);
    }
    if (e.target.value.length > valueLength) {
      return setError(true);
    }
    setError(false);
  };
  return (
    <TableCell
      onClick={handleOpen}
      style={{ maxWidth: "200px", textAlign: "center" }}
    >
      <>
        {open ? (
          <TextField
            value={valueInput}
            onChange={changeValue}
            error={error}
            helperText={
              error &&
              `No puede estar vació  y no puede ser mayor a ${valueLength}`
            }
            onBlur={handleClose}
          />
        ) : (
          value
        )}
      </>
    </TableCell>
  );
}

export default LoadOrderCustomizablDispatch;
