import { Button, TableCell } from "@mui/material";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { useParams } from "react-router";
import { deleteOption, dispatchOption } from "../utils/useSelectOption";
import LoadOrderCustomizablDispatch from "./LoadOrderCustomizablDispatch";

const ItemFieldsTable = ({ item = {} }) => {
  const [open, setOpen] = useState(false);
  const dispatch = useDispatch();

  const params = useParams();
  const handleOpenNumberDispatch = () => {
    setOpen(true);
  };
  const handleCloseNumberDispatch = (e) => {
    dispatchOption(item.id, e, params.option, dispatch);
    setOpen(false);
  };
  const handleDelete = () => {
    deleteOption(item.id, params.option, dispatch);
  };
  return (
    <>
      <TableCell sx={{ width: 100, textAlign: "center" }}>{item.id}</TableCell>
      <LoadOrderCustomizablDispatch
        open={open}
        value={item.name}
        handleOpen={handleOpenNumberDispatch}
        handleClose={handleCloseNumberDispatch}
      />
      <TableCell sx={{ textAlign: "center" }}>{item.count}</TableCell>
      <TableCell sx={{ textAlign: "center" }}>
        {new Date(item.initDate).toLocaleDateString()}
      </TableCell>
      <TableCell sx={{ textAlign: "center" }}>
        {new Date(item.updateDate).toLocaleDateString()}
      </TableCell>

      <TableCell sx={{ textAlign: "center" }}>
        {item.count === 0 ? (
          <Button variant="outlined" color="error" onClick={handleDelete}>
            Eliminar
          </Button>
        ) : (
          <Button variant="outlined" disabled>
            No se puede eliminar
          </Button>
        )}
      </TableCell>
    </>
  );
};

export default ItemFieldsTable;
