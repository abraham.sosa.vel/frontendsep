export const getformatUsers = (users = []) => {
  let data = users
    ?.map((e) => {
      return {
        id: e?.id,
        name: e?.nombre,
        lastName: e?.apellido,
        email: e?.correoElectronico,
        branch: e?.sucursale?.nombre,
        position: e.cargo,
        state: e?.estado,
        rol: e?.role?.nombre,
      };
    })
    .sort((a, b) => b.id - a.id);
  return data;
};
export const createFormatUser = (user = {}) => {
  return {
    nombre: user.name,
    apellido: user.lastName,
    correoElectronico: user.email,
    contrasenia: user.password,
    estado: user.state === "Activo",
    rolId: user.rol,
    sucursalId: user.branch,
    cargo: user.position,
  };
};
export const getFormatUserById = (user = {}) => {
  return {
    id: user.id,
    rol: user.rolId,
    name: user.nombre,
    lastName: user.apellido,
    state: user.estado ? "Activo" : "Inactivo",
    email: user.correoElectronico,
    position: user.cargo,
    branch: user.sucursalId,
  };
};

export const updateFormatUser = (user = {}) => {
  if (user?.password || user.password !== "") {
    return {
      nombre: user.name,
      apellido: user.lastName,
      correoElectronico: user.email,
      contrasenia: user.password,
      estado: user.state === "Activo",
      rolId: user.rol,
      sucursalId: user.branch,
      cargo: user.position,
    };
  }
  return {
    nombre: user.name,
    apellido: user.lastName,
    correoElectronico: user.email,
    estado: user.state === "Activo",
    rolId: user.rol,
    sucursalId: user.branch,
    cargo: user.position,
  };
};
