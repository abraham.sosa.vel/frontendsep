import { useTranslation } from "react-i18next";
import createColumns from "../../../components/BaseTable/createColumns";

export const useColumnsUsers = () => {
  const [t] = useTranslation("global");
  return [
    createColumns({ id: "action", title: t("tableUsers.action") }),
    createColumns({ id: "id", title: t("tableUsers.id") }),
    createColumns({ id: "name", title: t("tableUsers.name") }),
    createColumns({ id: "lastName", title: t("tableUsers.lastName") }),
    createColumns({ id: "email", title: t("tableUsers.email") }),
    createColumns({ id: "department", title: t("tableUsers.department") }),
    createColumns({ id: "userRole", title: t("tableUsers.userRole") }),
    createColumns({ id: "position", title: t("tableUsers.position") }),
    createColumns({ id: "state", title: t("tableUsers.state") }),
  ];
};
