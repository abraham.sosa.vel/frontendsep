import { useState } from "react";
import { useTranslation } from "react-i18next";
import {
  regexEmail,
  regexOnlyLetters,
  regexPassword,
} from "../../../utils/regularExpresions";

const useValidations = () => {
  const [message, setMessages] = useState({
    name: "",
    lastName: "",
    email: "",
    password: "",
  });
  const [t] = useTranslation("global");
  //validate name
  const validationName = (value) => {
    if (value === "") {
      setMessages({ ...message, name: t("errorMesages.required") });
      return false;
    }
    if (!regexOnlyLetters.test(value)) {
      setMessages({ ...message, name: t("errorMesages.onlyLetters") });
      return false;
    }
    if (value.length > 45) {
      setMessages({
        ...message,
        name: `${t("errorMesages.maxLength")} 45 ${t(
          "errorMesages.caracters"
        )}`,
      });
      return false;
    }
    return true;
  };
  const validationLastName = (value) => {
    if (value === "") {
      setMessages({ ...message, lastName: t("errorMesages.required") });
      return false;
    }
    if (!regexOnlyLetters.test(value)) {
      setMessages({ ...message, lastName: t("errorMesages.onlyLetters") });
      return false;
    }
    if (value.length > 45) {
      setMessages({
        ...message,
        lastName: `${t("errorMesages.maxLength")} 45 ${t(
          "errorMesages.caracters"
        )}`,
      });
      return false;
    }
    return true;
  };
  const validationEmail = (value) => {
    if (value === "") {
      setMessages({ ...message, email: t("errorMesages.required") });
      return false;
    }
    if (!regexEmail.test(value)) {
      setMessages({ ...message, email: t("errorMesages.email") });
      return false;
    }
    return true;
  };

  const validatePassword = (value) => {
    if (value === "") {
      setMessages({ ...message, password: t("errorMesages.required") });
      return false;
    }
    if (value.length < 8 || value.length > 15) {
      setMessages({ ...message, password: t("errorMesages.passwordLength") });
      return false;
    }
    if (!regexPassword.test(value)) {
      setMessages({ ...message, password: t("errorMesages.password") });
      return false;
    }
    return true;
  };
  return {
    validationName,
    message,
    validationLastName,
    validationEmail,
    validatePassword,
  };
};
export default useValidations;
