import { Edit, Leaderboard, Person, PersonOff } from "@mui/icons-material";
import {
  Box,
  ListItemIcon,
  ListItemText,
  MenuItem,
  TableCell,
  Typography,
  useTheme,
} from "@mui/material";
import { green, red } from "@mui/material/colors";
import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import MenuActions from "../../../components/MenuActions.js";

const ItemUsersTable = ({
  item = {},
  handleEditUser,
  handleDeleteUser,
  handleDisable,
}) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const theme = useTheme();
  const [t] = useTranslation("global");
  const handleEdit = () => {
    setAnchorEl(false);
    handleEditUser(item);
  };

  return (
    <>
      <TableCell sx={{ width: 50 }}>
        <MenuActions
          anchorEl={anchorEl}
          setAnchorEl={setAnchorEl}
          sx={{ width: "auto" }}
        >
          <Box>
            <MenuItem onClick={handleEdit}>
              <ListItemIcon>
                <Edit />
              </ListItemIcon>
              <ListItemText primary={t("global.edit")} />
            </MenuItem>
            {item.state ? (
              <MenuItem
                onClick={(e) => {
                  handleDisable(false, item.id);
                }}
              >
                <ListItemIcon>
                  <PersonOff />
                </ListItemIcon>
                <ListItemText primary={t("global.disableUser")} />
              </MenuItem>
            ) : (
              <MenuItem
                onClick={(e) => {
                  handleDisable(true, item.id);
                }}
              >
                <ListItemIcon>
                  <Person />
                </ListItemIcon>
                <ListItemText primary={t("global.activeUser")} />
              </MenuItem>
            )}
          </Box>
        </MenuActions>
      </TableCell>
      <TableCell>{item.id}</TableCell>
      <TableCell>{item.name}</TableCell>
      <TableCell>{item.lastName}</TableCell>
      <TableCell>{item.email}</TableCell>
      <TableCell>{item.branch}</TableCell>
      <TableCell>{item.rol}</TableCell>
      <TableCell>{item.position}</TableCell>
      <TableCell>
        {item.state ? (
          <Box
            sx={{
              bgcolor: theme.palette.mode === "dark" ? green[800] : green[500],
              borderRadius: 6,
              py: ".5rem",
              px: ".5rem",
            }}
          >
            <Typography align="center" sx={{ color: "#ffff" }}>
              {t("createUser.active")}
            </Typography>
          </Box>
        ) : (
          <Box
            sx={{
              bgcolor: theme.palette.mode === "dark" ? red[800] : red[500],
              borderRadius: 6,
              py: ".5rem",
              px: ".5rem",
            }}
          >
            <Typography align="center" sx={{ color: "#ffff" }}>
              {t("createUser.inactive")}
            </Typography>
          </Box>
        )}
      </TableCell>
    </>
  );
};

export default ItemUsersTable;
