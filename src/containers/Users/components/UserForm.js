import {
  Button,
  FormControl,
  FormControlLabel,
  FormLabel,
  Grid,
  InputLabel,
  MenuItem,
  Radio,
  RadioGroup,
  Select,
  Skeleton,
  TextField,
  Typography,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import BaseModal from "../../../components/BaseModal/BaseModal";
import SelectRol from "../../../components/SelectRol/SelectRol";
import { roles } from "../../../utils/roles";
import { getBranchesOfficesStart } from "../redux/thunk";
import useValidations from "../utils/validations";

const UserForm = ({ status, closeModal, defaultValues, onSubmit }) => {
  const dispatch = useDispatch();
  const [resetPassword, setResetPassword] = useState("no");
  const { user } = useSelector((state) => state);
  useEffect((e) => {
    dispatch(getBranchesOfficesStart());
  }, []);
  useEffect(() => {
    if (!defaultValues?.id) setResetPassword("si");
    if (defaultValues?.id) setResetPassword("no");
  }, [defaultValues?.id]);

  const {
    validationName,
    message,
    validationLastName,
    validationEmail,
    validatePassword,
  } = useValidations();
  const [t] = useTranslation("global");
  const {
    register,
    handleSubmit,
    control,
    formState: { errors },
    setValue,
    reset,
  } = useForm({
    mode: "onChange",
    defaultValues: defaultValues,
  });
  useEffect(() => {
    reset(defaultValues);
  }, [defaultValues, reset, user.branchesOffices]);
  const handleChangePassword = (e) => {
    setResetPassword(e.target.value);
    if (e.target.value === "no") setValue("password", "");
  };
  return (
    <BaseModal
      title={defaultValues?.id ? t("global.editUser") : t("global.createUser")}
      statusModal={status}
      closeModal={closeModal}
    >
      <form onSubmit={handleSubmit(onSubmit)}>
        <input type="hidden" {...register("id")} />
        <Grid item xs={12} container spacing={1}>
          <Grid item xs={12} md={6}>
            <TextField
              label={t("createUser.name")}
              variant="outlined"
              margin="normal"
              fullWidth
              {...register("name", {
                validate: validationName,
              })}
              type="text"
              autoComplete="name"
              error={!!errors?.name}
              helperText={errors.name && message.name}
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <TextField
              fullWidth
              label={t("createUser.lastName")}
              {...register("lastName", {
                validate: validationLastName,
              })}
              variant="outlined"
              margin="normal"
              autoComplete="lastName"
              error={!!errors?.lastName}
              helperText={errors.lastName && message.lastName}
            />
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <TextField
            type="email"
            fullWidth
            label={t("createUser.email")}
            {...register("email", {
              validate: validationEmail,
            })}
            variant="outlined"
            margin="normal"
            autoComplete="email"
            error={!!errors?.email}
            helperText={errors.email && message.email}
          />
        </Grid>
        {defaultValues.id && (
          <Grid container sx={{ my: 1 }} alignItems="center">
            <Typography variant="body2" color="text.secondary" sx={{ mt: 1 }}>
              {t("createUser.messagePassword")}
            </Typography>
            <FormControl sx={{ mt: 1, ml: 2 }}>
              <RadioGroup
                value={resetPassword}
                onChange={handleChangePassword}
                row
              >
                <FormControlLabel value={"si"} control={<Radio />} label="Si" />
                <FormControlLabel value={"no"} control={<Radio />} label="No" />
              </RadioGroup>
            </FormControl>
          </Grid>
        )}
        <Grid container spacing={1}>
          {resetPassword === "si" && (
            <Grid item xs={12} md={6}>
              <TextField
                margin="normal"
                fullWidth
                name="password"
                label={t("createUser.password")}
                {...register("password", {
                  validate: validatePassword,
                })}
                type="password"
                autoComplete="current-password"
                error={!!errors?.password}
                helperText={errors.password && message.password}
              />
            </Grid>
          )}
          <Grid item xs={12} md={6}>
            <Controller
              control={control}
              name="position"
              render={({ field: { onChange, value } }) => (
                <FormControl fullWidth margin="normal">
                  <InputLabel>{t("createUser.position")}</InputLabel>
                  <Select
                    fullWidth
                    onChange={onChange}
                    value={value}
                    label={t("createUser.position")}
                  >
                    <MenuItem value={"Abogado"}>Abogado</MenuItem>
                    <MenuItem value={"Coordinador"}>Coordinador</MenuItem>
                    <MenuItem value={"Auxiliar"}>Auxiliar</MenuItem>
                    <MenuItem value={"Area de TI"}>Area de TI</MenuItem>
                    <MenuItem value={"Externo"}>Externo</MenuItem>
                  </Select>
                </FormControl>
              )}
            />
          </Grid>
        </Grid>
        <Grid container>
          <Grid item xs={12} md={6}>
            <Controller
              control={control}
              name="branch"
              render={({ field: { onChange, value } }) => (
                <FormControl fullWidth margin="normal">
                  <InputLabel>{t("createUser.branch")}</InputLabel>
                  {user.loaderBranches ? (
                    <Skeleton variant="rounded" height={55} />
                  ) : (
                    <Select
                      fullWidth
                      onChange={onChange}
                      value={value}
                      label={t("createUser.branch")}
                    >
                      {user.branchesOffices?.map((e) => (
                        <MenuItem key={e.id} value={e.id}>
                          {e.nombre}
                        </MenuItem>
                      ))}
                    </Select>
                  )}
                </FormControl>
              )}
            />
          </Grid>
          <Grid
            item
            xs={12}
            md={6}
            container
            alignItems="center"
            justifyContent="center"
          >
            <FormControl sx={{ mt: 2 }}>
              <FormLabel sx={{ textAlign: "center" }}>
                {t("createUser.state")}
              </FormLabel>
              <Controller
                name="state"
                control={control}
                render={({ field: { onChange, value } }) => (
                  <RadioGroup row value={value} onChange={onChange}>
                    <FormControlLabel
                      value="Activo"
                      control={<Radio />}
                      label={t("createUser.active")}
                    />
                    <FormControlLabel
                      value="Inactivo"
                      control={<Radio />}
                      label={t("createUser.inactive")}
                    />
                  </RadioGroup>
                )}
              />
            </FormControl>
          </Grid>
        </Grid>
        <Grid item xs={12} sx={{ mt: 2 }}>
          <Typography variant="h5" color="text.secondary">
            {t("createUser.rol")}
          </Typography>
          <Typography variant="body2" color="text.secondary" sx={{ mt: 1 }}>
            {t("createUser.descriptionRol")}
          </Typography>
          <Grid container direction="column">
            <FormControl>
              <Controller
                name="rol"
                control={control}
                render={({ field: { onChange, value } }) => (
                  <RadioGroup row value={value} onChange={onChange}>
                    {roles?.map((e) => (
                      <SelectRol
                        title={e.name}
                        description={e.description}
                        key={e.name}
                        icon={e.icon}
                        id={e.id}
                        setValue={setValue}
                        nameControl="rol"
                      >
                        <FormControlLabel value={e.id} control={<Radio />} />
                      </SelectRol>
                    ))}
                  </RadioGroup>
                )}
              />
            </FormControl>
          </Grid>
        </Grid>
        <Grid
          container
          justifyContent="center"
          sx={{ marginTop: 1 }}
          spacing={1}
        >
          <Grid item>
            <Button variant="outlined" onClick={closeModal}>
              {t("global.cancel").toUpperCase()}
            </Button>
          </Grid>
          <Grid item>
            <Button type="submit" variant="contained">
              {defaultValues?.id
                ? t("global.editUser").toUpperCase()
                : t("global.createUser").toUpperCase()}
            </Button>
          </Grid>
        </Grid>
      </form>
    </BaseModal>
  );
};

export default UserForm;
