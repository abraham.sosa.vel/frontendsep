import React, { useState } from "react";
import {
  Grid,
  InputAdornment,
  Paper,
  TextField,
  useTheme,
} from "@mui/material";
import TitlePage from "../../components/TitlePage";
import { Search } from "@mui/icons-material";
import BaseTable from "../../components/BaseTable";
import { useTranslation } from "react-i18next";
import ItemUsersTable from "./components/ItemUsersTable";
import { BaseCreateButton } from "../../components/BaseCreateButton/BaseCreateButton";
import UserForm from "./components/UserForm";
import { useModal } from "../../utils/useModal";
import { useColumnsUsers } from "./utils/useColumnsUsers";
import { useDispatch, useSelector } from "react-redux";
import {
  createFormatUser,
  getFormatUserById,
  getformatUsers,
  updateFormatUser,
} from "./utils/formatUsers";
import {
  changeStatusUserStart,
  createUserStart,
  getUserById,
  searchRequestUsersStart,
  updaterUserStart,
} from "./redux/thunk";

const Users = () => {
  const [statusModal, handleCloseModal, handleOpenModal] = useModal();
  const { users, loader } = useSelector((state) => state.user);
  const dispatch = useDispatch();
  const [defaultValues, setDefaultValue] = useState({});
  const [t] = useTranslation("global");
  const theme = useTheme();

  const handleSubmit = (data) => {
    return new Promise((resolve) => {
      data?.id
        ? dispatch(updaterUserStart(data?.id, updateFormatUser(data), resolve))
        : dispatch(createUserStart(createFormatUser(data), resolve));
    }).then((e) => {
      handleCloseModal();
    });
  };
  const openModalEdit = (user) => {
    return new Promise((resolve) => {
      dispatch(getUserById(user.id, resolve));
    }).then((e) => {
      setDefaultValue(getFormatUserById(e));
      handleOpenModal();
    });
  };
  const handleDelete = (id) => {
    console.log(id);
  };
  const changeStatusUser = (status, id) => {
    dispatch(changeStatusUserStart(id, status));
  };
  const searchUsers = (e) => {
    dispatch(searchRequestUsersStart(e.target.value));
  };
  return (
    <Paper
      sx={{ p: 2, pt: 3 }}
      elevation={theme.palette.mode === "dark" ? 0 : 5}
    >
      <Grid container direction="column">
        <Grid sx={{ mb: 1 }}>
          <TitlePage title={t("NavItems.users").toUpperCase()} />
        </Grid>
        {/*Container search and button*/}
        <Grid
          item
          container
          xs={12}
          spacing={2}
          justifyContent="space-between"
          direction={{ xs: "column-reverse", md: "row" }}
        >
          {/*Input Search*/}
          <Grid item xs={12} md={6}>
            <TextField
              size="small"
              fullWidth
              type="search"
              id="input-search"
              placeholder={t("users.search")}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <Search />
                  </InputAdornment>
                ),
              }}
              variant="outlined"
              onChange={searchUsers}
            />
          </Grid>
          {/*Button create*/}
          <Grid item container xs={12} md={4} justifyContent="flex-end">
            <BaseCreateButton
              onClick={(e) => {
                handleOpenModal();
                setDefaultValue({
                  rol: "2",
                  state: "Activo",
                  position: "Abogado",
                  id: "",
                  branch: "1",
                });
              }}
            />
            <UserForm
              status={statusModal}
              closeModal={handleCloseModal}
              onSubmit={handleSubmit}
              defaultValues={defaultValues}
            />
          </Grid>
        </Grid>
        {/*Table users*/}
        <BaseTable columns={useColumnsUsers()} loading={loader} rowsLength={6}>
          {getformatUsers(users)?.map((e) => (
            <ItemUsersTable
              key={e.id}
              item={e}
              handleEditUser={openModalEdit}
              handleDeleteUser={handleDelete}
              handleDisable={changeStatusUser}
            />
          ))}
        </BaseTable>
      </Grid>
    </Paper>
  );
};

export default Users;
