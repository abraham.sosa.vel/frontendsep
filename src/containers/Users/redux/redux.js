import { createSlice } from "@reduxjs/toolkit";
const initialState = {
  users: [],
  loader: false,
  branchesOffices: [],
  loaderBranches: false,
};
export const userSlice = createSlice({
  name: "userSlice",
  initialState,
  reducers: {
    initialRequestUsersSuccess: (state, action) => {
      state.users = action.payload;
    },
    showLoader: (state, action) => {
      state.loader = true;
    },
    hideLoader: (state, action) => {
      state.loader = false;
    },
    getbranchesOfficesSuccess: (state, action) => {
      state.branchesOffices = action.payload;
    },
    showLoaderBranches: (state, action) => {
      state.loaderBranches = true;
    },
    hideLoaderBranches: (state, action) => {
      state.loaderBranches = false;
    },
  },
});

export const {
  initialRequestUsersSuccess,
  showLoader,
  hideLoader,
  getbranchesOfficesSuccess,
  showLoaderBranches,
  hideLoaderBranches,
} = userSlice.actions;
export default userSlice.reducer;
