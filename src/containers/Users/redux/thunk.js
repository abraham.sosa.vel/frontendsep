import {
  hideGlobalLoader,
  messageError,
  messageSuccess,
  showGlobalLoader,
} from "../../../redux/globalRedux";
import request, {
  getOptionWithToken,
  postOptionWithToken,
  putOptionWithToken,
} from "../../../utils/request";
import {
  createUserSuccess,
  getbranchesOfficesSuccess,
  hideLoader,
  hideLoaderBranches,
  initialRequestUsersSuccess,
  showLoader,
  showLoaderBranches,
} from "./redux";

export const initialRequestUsersStart = () => {
  return async (dispatch, getState) => {
    try {
      dispatch(showLoader());
      const url = `${process.env.REACT_APP_URL_API}/users`;
      const options = getOptionWithToken();
      const data = await request(url, options);
      dispatch(initialRequestUsersSuccess(data));
    } catch (error) {
      dispatch(messageError(error?.message));
    } finally {
      dispatch(hideLoader());
    }
  };
};
export const searchRequestUsersStart = (search) => {
  return async (dispatch, getState) => {
    try {
      dispatch(showLoader());
      const url = `${process.env.REACT_APP_URL_API}/users/search`;
      const options = postOptionWithToken({ name: search });
      const data = await request(url, options);
      dispatch(initialRequestUsersSuccess(data));
    } catch (error) {
      dispatch(messageError(error?.message));
    } finally {
      dispatch(hideLoader());
    }
  };
};
export const getBranchesOfficesStart = () => {
  return async (dispatch, getState) => {
    const { branchesOffices } = getState().user;
    try {
      if (branchesOffices.length > 0) return;
      dispatch(showLoaderBranches);
      const url = `${process.env.REACT_APP_URL_API}/branchesOffices`;
      const options = getOptionWithToken();
      const data = await request(url, options);
      dispatch(getbranchesOfficesSuccess(data));
    } catch (error) {
      dispatch(messageError(error?.message));
    } finally {
      dispatch(hideLoaderBranches());
    }
  };
};

export const createUserStart = (data, resolve) => {
  return async (dispatch, getState) => {
    const { users } = getState().user;
    try {
      dispatch(showGlobalLoader());
      const url = `${process.env.REACT_APP_URL_API}/users`;
      const options = postOptionWithToken(data);
      const userCreate = await request(url, options);
      dispatch(initialRequestUsersSuccess([userCreate, ...users]));
      dispatch(messageSuccess("Se creo el usuario con éxito"));
      resolve();
    } catch (error) {
      let message = error?.message
        ? error.message
        : "No se pudo crear el usuario";
      dispatch(messageError(message));
    } finally {
      dispatch(hideGlobalLoader());
    }
  };
};
export const getUserById = (id, resolve) => {
  return async (dispatch, getState) => {
    try {
      dispatch(showGlobalLoader());
      const url = `${process.env.REACT_APP_URL_API}/users/${id}`;
      const options = getOptionWithToken();
      const user = await request(url, options);
      resolve(user);
    } catch (error) {
      let message = error?.message
        ? error.message
        : "No se pudo traer los datos del usuario";
      dispatch(messageError(message));
    } finally {
      dispatch(hideGlobalLoader());
    }
  };
};

export const updaterUserStart = (id, data, resolve) => {
  return async (dispatch, getState) => {
    const { users } = getState().user;
    try {
      dispatch(showGlobalLoader());
      const url = `${process.env.REACT_APP_URL_API}/users/${id}`;
      const options = putOptionWithToken(data);
      const user = await request(url, options);
      dispatch(
        initialRequestUsersSuccess(
          users.map((e) => (e.id === user.id ? user : e))
        )
      );
      dispatch(messageSuccess("Se edito el usuario con éxito"));
      resolve();
    } catch (error) {
      let message = error?.message
        ? error.message
        : "No se pudo editar los datos del usuario";
      dispatch(messageError(message));
    } finally {
      dispatch(hideGlobalLoader());
    }
  };
};

export const changeStatusUserStart = (id, data) => {
  return async (dispatch, getState) => {
    const { users } = getState().user;
    try {
      dispatch(showLoader());
      const url = `${process.env.REACT_APP_URL_API}/users/changeStatus/${id}`;
      const options = putOptionWithToken({ estado: data });
      const user = await request(url, options);
      dispatch(
        initialRequestUsersSuccess(
          users.map((e) => (e.id === user.id ? user : e))
        )
      );
      dispatch(messageSuccess("Se cambio el estado con éxito"));
    } catch (error) {
      let message = error?.message
        ? error.message
        : "No se pudo editar los datos del usuario";
      dispatch(messageError(message));
    } finally {
      dispatch(hideLoader());
    }
  };
};
