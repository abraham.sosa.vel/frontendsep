import { useTheme } from "@emotion/react";
import { Typography } from "@mui/material";
import React from "react";

const TitlePage = ({ title }) => {
  const theme = useTheme();
  return (
    <Typography
      variant="subtitle1"
      component="h1"
      sx={{ fontWeight: "bold", color: theme.palette.text.textSepdavi }}
    >
      {title}
    </Typography>
  );
};

export default TitlePage;
