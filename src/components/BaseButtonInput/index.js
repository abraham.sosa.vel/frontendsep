import { Grid, Paper, Typography, useTheme } from "@mui/material";
import {
  lightBlue,
  cyan,
  green,
  orange,
  lime,
  red,
  teal,
  amber,
} from "@mui/material/colors";
import React from "react";
import { useNavigate } from "react-router";

const BaseButtonInput = ({ children, title, description, color, redirect }) => {
  const navigate = useNavigate();
  const theme = useTheme();
  let colorSelect;
  switch (color) {
    case "green":
      colorSelect = green;
      break;
    case "cyan":
      colorSelect = cyan;
      break;
    case "lightBlue":
      colorSelect = lightBlue;
      break;
    case "orange":
      colorSelect = orange;
      break;
    case "lime":
      colorSelect = lime;
      break;
    case "red":
      colorSelect = red;
      break;
    case "teal":
      colorSelect = teal;
      break;
    case "amber":
      colorSelect = amber;
      break;
    default:
      colorSelect = red;
      break;
  }
  return (
    <Paper
      elevation={theme.palette.mode === "dark" ? 0 : 5}
      sx={{
        p: 2,
        pt: 3,
        color:
          theme.palette.mode === "dark" ? colorSelect[900] : colorSelect[600],
      }}
      onClick={(e) => {
        navigate(redirect);
      }}
    >
      <Grid
        container
        direction="column"
        alignItems="center"
        sx={{ cursor: "pointer" }}
      >
        {children}
        <Typography sx={{ opacity: ".95" }} fontWeight={600}>
          {title}
        </Typography>
        <Typography
          sx={{ mt: 1, opacity: ".8" }}
          color={theme.palette.text.TextCuston}
          fontWeight={600}
          variant="subtitle2"
          textAlign="center"
        >
          {description}
        </Typography>
      </Grid>
    </Paper>
  );
};

export default BaseButtonInput;
