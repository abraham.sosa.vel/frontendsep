import {
  FormControlLabel,
  FormGroup,
  MenuItem,
  Typography,
} from "@mui/material";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { changeTheme } from "../../redux/globalRedux";
import MaterialUISwitch from "./MaterialUISwitch";

const ButtonTheme = () => {
  const dispatch = useDispatch();
  const { themeSelect } = useSelector((state) => state.global);
  const [stateSwitch, setStateSwitch] = useState(themeSelect === "dark");
  const handleChangeTheme = (e) => {
    dispatch(changeTheme(e.target.checked ? "dark" : "light"));
    localStorage.setItem("themeSelected", e.target.checked ? "dark" : "light");
    setStateSwitch(e.target.checked);
  };

  return (
    <FormGroup>
      <FormControlLabel
        sx={{
          color:
            themeSelect === "dark" ? "#ffff !important" : "#212121 !important",
        }}
        control={
          <MaterialUISwitch
            sx={{ m: 1 }}
            checked={stateSwitch}
            onChange={handleChangeTheme}
          />
        }
        label="Tema oscuro"
      />
    </FormGroup>
  );
};

export default ButtonTheme;
