import {
  Box,
  Grid,
  Skeleton,
  styled,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  useTheme,
} from "@mui/material";
import { grey } from "@mui/material/colors";
import React from "react";
import TableHeader from "./TableHeader";

const BaseTable = ({ columns = [], children, loading, rowsLength }) => {
  const theme = useTheme();
  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
    "&:last-child th": {
      border: 0,
    },
    td: {
      borderTop: `1px solid ${
        theme.palette.mode === "dark" ? grey[600] : grey[400]
      }`,
    },
  }));
  const skeletonArrayRows = Array(rowsLength).fill("");
  return (
    <Grid container spacing={2} sx={{ my: 2 }}>
      <Grid item xs={12}>
        <Box sx={{ overflow: "auto" }}>
          <Box sx={{ width: "100%", display: "table", tableLayout: "fixed" }}>
            <TableContainer
              sx={{
                maxHeight: "60vh",
                "&::-webkit-scrollbar": {
                  width: "0.35em",
                  height: "0.35em",
                },
                "&::-webkit-scrollbar-track": {
                  "-webkit-box-shadow": "inset 0 0 6px rgba(0,0,0,0.00)",
                },
                "&::-webkit-scrollbar-thumb": {
                  backgroundColor:
                    theme.palette.mode === "dark" ? "#595959" : "#d9d9d9",
                  borderRadius: 5,
                },
              }}
            >
              <Table stickyHeader>
                <TableHeader columns={columns} />
                <TableBody>
                  {loading
                    ? skeletonArrayRows.map((e, i) => (
                        <StyledTableRow key={i}>
                          {columns.map((n, j) => (
                            <TableCell key={j}>
                              <Skeleton
                                variant="text"
                                sx={{ fontSize: "1.2rem" }}
                              />
                            </TableCell>
                          ))}
                        </StyledTableRow>
                      ))
                    : children.map((item) => (
                        <StyledTableRow key={item.key}>{item}</StyledTableRow>
                      ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Box>
        </Box>
      </Grid>
    </Grid>
  );
};

export default BaseTable;
