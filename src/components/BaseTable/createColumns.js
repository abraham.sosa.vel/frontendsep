const createColumns = ({ title = "", id = "" }) => ({
  title: title.toUpperCase(),
  id,
});

export default createColumns;
