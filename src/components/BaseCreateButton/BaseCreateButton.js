import PropTypes from "prop-types";
import { Button, Grid } from "@mui/material";
import { Link } from "react-router-dom";
import { AddRounded } from "@mui/icons-material";
import { useTranslation } from "react-i18next";

export function BaseCreateButton({ link, onClick }) {
  const [t] = useTranslation("global");

  return (
    <Grid container item xs={12} justifyContent="end" sx={{ marginY: 1 }}>
      <Grid item>
        {link && (
          <Link to={link} style={{ textDecoration: "none", color: "success" }}>
            <Button
              variant="contained"
              color="success"
              startIcon={<AddRounded />}
            >
              {t("global.createNew")}
            </Button>
          </Link>
        )}
        {onClick && (
          <Button
            color="success"
            onClick={onClick}
            variant="contained"
            startIcon={<AddRounded />}
          >
            {t("global.createNew")}
          </Button>
        )}
      </Grid>
    </Grid>
  );
}

BaseCreateButton.propTypes = {
  link: PropTypes.string,
  onClick: PropTypes.func,
};

BaseCreateButton.defaultProps = {
  link: null,
  onClick: null,
};
