import * as React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import { Grid } from "@mui/material";

export default function BaseModalConfirm({
  title,
  status,
  handleClose,
  confirmModal,
}) {
  return (
    <Dialog open={status} onClose={handleClose}>
      <Grid container sx={{ p: 4 }} justifyContent="center">
        <DialogContent>
          <DialogContentText
            id="alert-dialog-description"
            sx={{ textAlign: "center" }}
          >
            {title.toUpperCase()}
          </DialogContentText>
        </DialogContent>
        <Grid
          item
          xs={12}
          container
          justifyContent="space-around"
          sx={{ py: 2 }}
        >
          <Button onClick={handleClose} variant="contained" color="error">
            Cancelar
          </Button>
          <Button onClick={confirmModal} variant="contained" color="success">
            Confirmar
          </Button>
        </Grid>
      </Grid>
    </Dialog>
  );
}
