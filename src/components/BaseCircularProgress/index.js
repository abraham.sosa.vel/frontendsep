import { Backdrop, CircularProgress } from "@mui/material";
import React from "react";

const BaseCircularProgress = ({ loader }) => {
  return (
    <Backdrop sx={{ color: "#fff", zIndex: 400 }} open={loader}>
      <CircularProgress color="inherit" />
    </Backdrop>
  );
};

export default BaseCircularProgress;
