import { Close } from "@mui/icons-material";
import {
  Grid,
  IconButton,
  Modal,
  Typography,
  useMediaQuery,
  useTheme,
} from "@mui/material";
import { Box } from "@mui/system";
import React from "react";

const BaseModal = ({ title, children, statusModal, closeModal }) => {
  const theme = useTheme();
  const matchDownMD = useMediaQuery(theme.breakpoints.down("md"));
  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: matchDownMD ? "90%" : "40%",
    bgcolor: "background.modal",
    borderRadius: "1.5rem",
  };
  return (
    <Modal open={statusModal}>
      <Box sx={style}>
        <Grid
          container
          sx={{
            p: matchDownMD ? "1rem 1rem" : "1.2rem 2rem",
          }}
        >
          <Grid item xs={11} container alignItems="center">
            <Typography
              variant="h5"
              component="h2"
              align="center"
              sx={{ color: "text.TextCuston" }}
            >
              {title}
            </Typography>
          </Grid>
          <Grid item xs={1} container justifyContent="flex-end">
            <IconButton
              onClick={closeModal}
              color="primary"
              aria-label="upload picture"
              component="label"
              sx={{ color: "text.TextCuston" }}
            >
              <Close />
            </IconButton>
          </Grid>
        </Grid>
        <Grid
          container
          direction="column"
          sx={{
            maxHeight: "90vh",
            borderTop:
              theme.palette.mode === "dark"
                ? "1px solid rgba(255, 255, 255, 0.12)"
                : "1px solid rgba(0,0,0,0.12)",
            p: matchDownMD ? "1.2rem 1rem" : "1.2rem 2rem",
            overflowY: "auto",
          }}
        >
          {children}
        </Grid>
      </Box>
    </Modal>
  );
};

export default BaseModal;
