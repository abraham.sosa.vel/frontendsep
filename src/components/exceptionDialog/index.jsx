import { useEffect } from "react";
import { ErrorRounded } from "@mui/icons-material";
import { useLocation } from "react-router-dom";
import { useRef } from "react";

const FallbackComponent = ({ resetError = () => {} }) => {
  const location = useLocation();
  const errorLocation = useRef(location.pathname);

  useEffect(() => {
    if (location.pathname !== errorLocation.current) {
      resetError();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [location.pathname]);

  return (
    <div style={{ textAlign: "center", margin: "5rem" }}>
      <ErrorRounded
        sx={{
          fontSize: 86,
          marginBottom: "20px",
          color: (theme) => theme.palette.primary.main,
        }}
        color="primary"
      />
      <p style={{ fontSize: 16, fontWeight: "medium" }}>
        Acaba de ocurrir un error, por favor contáctese con soporte.
      </p>
    </div>
  );
};
export const exceptionDialog = (props) => <FallbackComponent {...props} />;
