import { Box, Toolbar, useTheme } from "@mui/material";
import React, { useState } from "react";
import { Outlet } from "react-router";
import MainDrawer from "./Drawer";
import Header from "./Header";

const LayoutAdmin = ({ children, loggedIn }) => {
  const theme = useTheme();
  const [openDrawer, setOpenDrawer] = useState(true);
  const handleDrawerToggle = () => {
    setOpenDrawer(!openDrawer);
  };
  if (loggedIn) {
    return (
      <Box
        sx={{
          display: "flex",
          width: "100%",
          height: "100%",
          minHeight: "100vh",
          bgcolor: theme.palette.background.default,
        }}
      >
        <Header open={openDrawer} handleDrawerToggle={handleDrawerToggle} />
        <MainDrawer open={openDrawer} handleDrawerToggle={handleDrawerToggle} />
        <Box
          component="main"
          sx={{
            width: "100%",
            flexGrow: 1,
            p: { xs: 2, sm: 3 },
          }}
        >
          <Toolbar />
          {children}
          <Outlet />
        </Box>
      </Box>
    );
  }
  return <>{children}</>;
};

export default LayoutAdmin;
