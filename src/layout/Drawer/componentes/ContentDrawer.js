import { Grid, Typography, useTheme } from "@mui/material";
import React from "react";
import style from "./contentDrawer.module.css";
import ListLink from "./ListLink";
const ContentDrawer = () => {
  const theme = useTheme();
  return (
    <Grid container item xs={12} direction="column">
      <Grid item container xs={1} justifyContent="center" alignContent="center">
        <img
          className={style.logo}
          src={
            theme?.palette?.mode === "light"
              ? "/logos/logoWhite.png"
              : "/logos/logoDark.png"
          }
          alt="iamgen a"
        />
        <Typography
          variant="subtitle1"
          sx={{
            fontWeight: "bold",
            fontSize: "29px",
            color: theme.palette.text.textSepdavi,
            ml: 1,
          }}
        >
          SEPDAVI
        </Typography>
      </Grid>
      <Grid item container>
        <ListLink />
      </Grid>
    </Grid>
  );
};

export default ContentDrawer;
