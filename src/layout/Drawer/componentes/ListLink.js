import {
  Equalizer,
  FormatListBulleted,
  Group,
  QueryStats,
  SaveAs,
  Summarize,
} from "@mui/icons-material";
import {
  List,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  ListSubheader,
} from "@mui/material";
import React from "react";
import { useTranslation } from "react-i18next";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

const ListLink = () => {
  const [t] = useTranslation("global");
  const { auth } = useSelector((state) => state.auth);
  const rolId = auth.user.rolId;
  return (
    <List
      sx={{
        width: "100%",
        maxWidth: 360,
        bgcolor: "background.paper",
      }}
      subheader={
        <ListSubheader component="div" id="nested-list-subheader">
          {t("NavItems.titleAdmin")}
        </ListSubheader>
      }
    >
      <ListItemButton component={Link} to="/">
        <ListItemIcon>
          <Equalizer />
        </ListItemIcon>
        <ListItemText primary={t("NavItems.principalPanel")} />
      </ListItemButton>
      {rolId === 1 || rolId === 2 ? (
        <ListItemButton component={Link} to="/registers">
          <ListItemIcon>
            <FormatListBulleted />
          </ListItemIcon>
          <ListItemText primary={t("NavItems.registers")} />
        </ListItemButton>
      ) : null}
      {rolId === 1 || rolId === 2 ? (
        <ListItemButton component={Link} to="/tracingReports">
          <ListItemIcon>
            <QueryStats />
          </ListItemIcon>
          <ListItemText primary={t("NavItems.tracingReports")} />
        </ListItemButton>
      ) : null}
      {rolId === 1 ? (
        <ListItemButton component={Link} to="/users">
          <ListItemIcon>
            <Group />
          </ListItemIcon>
          <ListItemText primary={t("NavItems.users")} />
        </ListItemButton>
      ) : null}

      <ListItemButton component={Link} to="/SpecificReports">
        <ListItemIcon>
          <Summarize />
        </ListItemIcon>
        <ListItemText primary={t("NavItems.specificReports")} />
      </ListItemButton>

      {rolId === 1 ? (
        <ListItemButton component={Link} to="/generalFields">
          <ListItemIcon>
            <SaveAs />
          </ListItemIcon>
          <ListItemText primary={t("NavItems.generalFields")} />
        </ListItemButton>
      ) : null}
    </List>
  );
};

export default ListLink;
