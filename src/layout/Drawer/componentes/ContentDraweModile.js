import { Grid, Typography, useTheme } from "@mui/material";
import { blue } from "@mui/material/colors";
import React from "react";
import style from "./contentDrawer.module.css";
import ListLink from "./ListLink";
const ContentDrawerMobile = () => {
  const theme = useTheme();
  return (
    <Grid container item xs={12} direction="column">
      <Grid item container xs={1} justifyContent="center" alignContent="center">
        <img
          className={style.logo}
          src={
            theme?.palette?.mode === "light"
              ? "/logos/logoWhite.png"
              : "/logos/logoDark.png"
          }
          alt="iamgen a"
        />
        <Typography
          variant="subtitle1"
          sx={{
            fontWeight: "bold",
            fontSize: "24px",
            color: blue["500"],
            ml: 1,
          }}
        >
          SEPDAVI
        </Typography>
      </Grid>
      <Grid item container xs={11} sx={{ position: "absolute !important" }}>
        <ListLink />
      </Grid>
    </Grid>
  );
};

export default ContentDrawerMobile;
