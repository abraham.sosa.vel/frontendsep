import { useTheme } from "@mui/material/styles";
import {
  AppBar,
  Grid,
  IconButton,
  Toolbar,
  useMediaQuery,
} from "@mui/material";

// assets
import {
  FormatIndentDecrease,
  FormatIndentIncrease,
} from "@mui/icons-material";
import AppBarStyled from "./AppBarStyled";
import HeaderContent from "./components/HeaderContent";

const Header = ({ open, handleDrawerToggle }) => {
  const theme = useTheme();
  const matchDownMD = useMediaQuery(theme.breakpoints.down("lg"));
  const iconBackColor = theme.palette.action.disabled;
  const iconBackColorOpen = theme.palette.action.disabledBackground;

  // common header
  const mainHeader = (
    <Toolbar>
      <Grid item container xs={12}>
        <Grid container justifyContent="start" alignItems="center" item xs={1}>
          <IconButton
            disableRipple
            onClick={handleDrawerToggle}
            edge="start"
            sx={{
              color: "text.primary",
              bgcolor: open ? iconBackColorOpen : iconBackColor,
              ml: { xs: 0, lg: -2 },
            }}
          >
            {!open ? <FormatIndentIncrease /> : <FormatIndentDecrease />}
          </IconButton>
        </Grid>
        <Grid item xs={11} container justifyContent="end">
          <HeaderContent />
        </Grid>
      </Grid>
    </Toolbar>
  );

  const appBar = {
    position: "fixed",
    color: "inherit",
    elevation: 0,
    sx: {
      borderBottom: `1px solid ${theme.palette.divider}`,
    },
  };

  return (
    <>
      {!matchDownMD ? (
        <AppBarStyled open={open} {...appBar}>
          {mainHeader}
        </AppBarStyled>
      ) : (
        <AppBar {...appBar}>{mainHeader}</AppBar>
      )}
    </>
  );
};

export default Header;
