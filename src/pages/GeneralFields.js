import React from "react";
import GeneralFields from "../containers/GeneralFields";

const GeneralFieldsPage = () => {
  return <GeneralFields />;
};

export default GeneralFieldsPage;
