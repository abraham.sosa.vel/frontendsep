import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import Users from "../containers/Users";
import { initialRequestUsersStart } from "../containers/Users/redux/thunk";

const UsersPage = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(initialRequestUsersStart());
  }, [dispatch]);

  return <Users />;
};
export default UsersPage;
