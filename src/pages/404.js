import { Button, Grid, Typography } from "@mui/material";
import { red } from "@mui/material/colors";
import React from "react";

const Error404 = () => {
  return (
    <div
      style={{
        width: "100%",
        maxHeight: "60%",
      }}
    >
      <Grid
        container
        justifyContent="center"
        alignItems="center"
        direction="column"
        sx={{ mt: 4 }}
      >
        <Typography
          sx={{
            fontSize: { xs: 40, lg: 60 },
            color: red[800],
            fontWeight: 600,
          }}
        >
          Error: 404
        </Typography>
        <Typography
          sx={{
            fontSize: { xs: 30, lg: 50 },
            color: red[700],
            fontWeight: 500,
          }}
        >
          pagina no encontrada
        </Typography>
      </Grid>
    </div>
  );
};

export default Error404;
