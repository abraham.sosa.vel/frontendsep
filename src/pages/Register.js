import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import Register from "../containers/Registers";
import { getAllRegistersStart } from "../containers/Registers/redux/thunk";

const RegisterPage = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getAllRegistersStart());
  }, [dispatch]);

  return <Register />;
};

export default RegisterPage;
