import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import SpecificReports from "../containers/SpecificReports";
import {
  getAllCrimesStart,
  getAllYearRegiterStart,
} from "../containers/SpecificReports/redux/thunk";

const SpecificReportsPage = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getAllYearRegiterStart());
    dispatch(getAllCrimesStart());
  }, [dispatch]);
  return <SpecificReports />;
};

export default SpecificReportsPage;
