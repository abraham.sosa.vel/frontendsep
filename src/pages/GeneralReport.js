import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import GeneralReports from "../containers/GeneralReports";
import { generateGeneralReportsStart } from "../containers/GeneralReports/redux/thunk";

const GeneralReport = () => {
  var currentDate = new Date();

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(
      generateGeneralReportsStart({
        age: [currentDate.getFullYear()],
        month: [currentDate.getMonth() + 1],
        crimes: [],
      })
    );
  }, [dispatch]);

  return <GeneralReports />;
};

export default GeneralReport;
