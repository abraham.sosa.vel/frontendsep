import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import TracingReport from "../containers/TracingReport";
import { getAllTracingReportsStart } from "../containers/TracingReport/redux/thunk";

const TracingReportPage = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getAllTracingReportsStart());
  }, [dispatch]);
  return <TracingReport />;
};

export default TracingReportPage;
