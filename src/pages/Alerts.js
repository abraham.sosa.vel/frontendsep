import { Alert, Snackbar } from "@mui/material";
import React, { useEffect } from "react";
import { useSelector } from "react-redux";

const Alerts = () => {
  const [open, setOpen] = React.useState(true);
  const { message } = useSelector((state) => state.global);
  useEffect(() => {
    setOpen(message.status);
  }, [message]);
  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };
  return (
    <Snackbar
      open={open}
      autoHideDuration={6000}
      onClose={handleClose}
      anchorOrigin={{ vertical: "top", horizontal: "center" }}
    >
      <Alert
        onClose={handleClose}
        severity={message.type}
        sx={{ width: "100%" }}
      >
        {message.message}
      </Alert>
    </Snackbar>
  );
};

export default Alerts;
