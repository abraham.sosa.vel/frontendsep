import { useState } from "react";

export const useModal = () => {
  const [statusModal, setStatusModal] = useState(false);
  const handleCloseModal = () => {
    setStatusModal(false);
  };
  const handleOpenModal = () => {
    setStatusModal(true);
  };
  return [statusModal, handleCloseModal, handleOpenModal];
};
