import { useSelector } from "react-redux";
import { store } from "../redux/store";

function parseJSON(response) {
  return response.status === 204 ? "" : response.json();
}
async function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }
  const message = await response.json();
  const error = { status: response.status, message: message };
  throw error;
}
export default function request(url, options) {
  return fetch(url, options).then(checkStatus).then(parseJSON);
}

export const getOptionWithoutToken = (method = "GET") => {
  return {
    method,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  };
};

export const getOptionWithToken = (method = "GET") => {
  const { auth } = store.getState();
  return {
    method,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "x-access-token": auth?.auth.token,
    },
  };
};

export const postOptionWithoutToken = (body = {}, method = "POST") => {
  return {
    method,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(body),
  };
};
export const postOptionWithToken = (body = {}, method = "POST") => {
  const { auth } = store.getState();
  return {
    method,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "x-access-token": auth?.auth.token,
    },
    body: JSON.stringify(body),
  };
};

export const putOptionWithToken = (body = {}, method = "PUT") => {
  const { auth } = store.getState();
  return {
    method,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "x-access-token": auth?.auth.token,
    },
    body: JSON.stringify(body),
  };
};
export const deleteOptionWithToken = (method = "DELETE") => {
  const { auth } = store.getState();
  return {
    method,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "x-access-token": auth?.auth.token,
    },
  };
};
