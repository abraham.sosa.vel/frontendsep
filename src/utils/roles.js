export const roles = [
  {
    id: "1",
    name: "Administrador",
    description:
      "El administrador tiene acceso usuarios generar reportes y estadísticas",
    icon: "LocalPolice",
  },
  {
    id: "2",
    name: "Operador",
    description: "El operador tiene acceso al modulo de casos y estadísticas",
    icon: "SupportAgent",
  },
  {
    id: "3",
    name: "Invitado",
    description: "El invitado tiene acceso al modulo de estadísticas",
    icon: "AssignmentInd",
  },
];

export const PERMISSIONS = {
  ADMIN_ROLE: "Administrador",
  OPERATOR_ROLE: "Operador",
  GUEST_ROLE: "Invitado",
};
