import { useDispatch } from "react-redux";
import { logOutSuccess } from "../containers/auth/redux/redux";

const useLogOut = () => {
  const dispatch = useDispatch();
  dispatch(logOutSuccess());
  localStorage.removeItem("userData");
};

export default useLogOut;
