import i18next from "i18next";
import global_es from "./español/global.json";
i18next.init({
  interpolation: {
    escapeValue: false,
    formatSeparator: ",",
  },
  lng: "es",
  resources: {
    es: {
      global: global_es,
    },
  },
});
export default i18next;
