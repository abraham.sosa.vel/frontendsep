import React, { lazy, Suspense } from "react";
import { Navigate, Outlet, Route, Routes } from "react-router";
import { CircularProgress, Grid } from "@mui/material";
import ErrorBoundary from "../components/errorBoundary";
import { exceptionDialog } from "../components/exceptionDialog";
import PermitionAccess from "./PermitionAccess";
import { PERMISSIONS } from "../utils/roles";
import Error404 from "../pages/404";

const LoginScreen = lazy(() => import("../pages/Auth"));
//admin
const GeneralReport = lazy(() => import("../pages/GeneralReport"));
const UserScreen = lazy(() => import("../pages/Users"));
//register case
const RegisterCaseScrenn = lazy(() => import("../pages/Register"));
const CreateCase = lazy(() =>
  import("../containers/Registers/components/FormRegister")
);
const GeneralFieldsPage = lazy(() => import("../pages/GeneralFields"));
const FieldListCreate = lazy(() =>
  import("../containers/GeneralFields/FieldListCreate")
);
const SpecificReportsPage = lazy(() => import("../pages/SpecificReports"));
const TracingReportsPage = lazy(() => import("../pages/TracingReport"));
const Progress = () => (
  <Grid container alignItems="center" justifyContent="center">
    <CircularProgress size={35} />
  </Grid>
);
const AppRoutes = ({ loggedIn }) => {
  return (
    <Routes>
      <Route
        path="/login"
        element={!loggedIn ? <LoginScreen /> : <Navigate to="/" />}
      />
      <Route
        path="/"
        element={loggedIn ? <Outlet /> : <Navigate to="/login" />}
      >
        <Route
          path="/"
          element={
            <Suspense fallback={<Progress />}>
              <PermitionAccess
                permissions={[
                  PERMISSIONS.ADMIN_ROLE,
                  PERMISSIONS.GUEST_ROLE,
                  PERMISSIONS.OPERATOR_ROLE,
                ]}
              >
                <ErrorBoundary fallback={exceptionDialog}>
                  <GeneralReport />
                </ErrorBoundary>
              </PermitionAccess>
            </Suspense>
          }
        />
        <Route
          path="/users"
          element={
            <Suspense fallback={<Progress />}>
              <PermitionAccess permissions={[PERMISSIONS.ADMIN_ROLE]}>
                <ErrorBoundary fallback={exceptionDialog}>
                  <UserScreen />
                </ErrorBoundary>
              </PermitionAccess>
            </Suspense>
          }
        />
        <Route
          path="/registers"
          element={
            <Suspense fallback={<Progress />}>
              <PermitionAccess
                permissions={[
                  PERMISSIONS.OPERATOR_ROLE,
                  PERMISSIONS.ADMIN_ROLE,
                ]}
              >
                <ErrorBoundary fallback={exceptionDialog}>
                  <RegisterCaseScrenn />
                </ErrorBoundary>
              </PermitionAccess>
            </Suspense>
          }
        />
        <Route
          path="/SpecificReports"
          element={
            <Suspense fallback={<Progress />}>
              <PermitionAccess
                permissions={[
                  PERMISSIONS.OPERATOR_ROLE,
                  PERMISSIONS.ADMIN_ROLE,
                  PERMISSIONS.GUEST_ROLE,
                ]}
              >
                <ErrorBoundary fallback={exceptionDialog}>
                  <SpecificReportsPage />
                </ErrorBoundary>
              </PermitionAccess>
            </Suspense>
          }
        />
        <Route
          path="/registers/create"
          element={
            <Suspense fallback={<Progress />}>
              <PermitionAccess
                permissions={[
                  PERMISSIONS.OPERATOR_ROLE,
                  PERMISSIONS.ADMIN_ROLE,
                ]}
              >
                <ErrorBoundary fallback={exceptionDialog}>
                  <CreateCase />
                </ErrorBoundary>
              </PermitionAccess>
            </Suspense>
          }
        />
        <Route
          path="/generalFields"
          element={
            <Suspense fallback={<Progress />}>
              <PermitionAccess permissions={[PERMISSIONS.ADMIN_ROLE]}>
                <ErrorBoundary fallback={exceptionDialog}>
                  <GeneralFieldsPage />
                </ErrorBoundary>
              </PermitionAccess>
            </Suspense>
          }
        />
        <Route
          path="/generalFields/:option"
          element={
            <Suspense fallback={<Progress />}>
              <PermitionAccess permissions={[PERMISSIONS.ADMIN_ROLE]}>
                <ErrorBoundary fallback={exceptionDialog}>
                  <FieldListCreate />
                </ErrorBoundary>
              </PermitionAccess>
            </Suspense>
          }
        />
        <Route
          path="/tracingReports"
          element={
            <Suspense fallback={<Progress />}>
              <PermitionAccess
                permissions={[
                  PERMISSIONS.ADMIN_ROLE,
                  PERMISSIONS.OPERATOR_ROLE,
                ]}
              >
                <ErrorBoundary fallback={exceptionDialog}>
                  <TracingReportsPage />
                </ErrorBoundary>
              </PermitionAccess>
            </Suspense>
          }
        />
        <Route
          path="*"
          element={
            <Suspense fallback={<Progress />}>
              <ErrorBoundary fallback={exceptionDialog}>
                <Error404 />
              </ErrorBoundary>
            </Suspense>
          }
        />
      </Route>
    </Routes>
  );
};

export default AppRoutes;
