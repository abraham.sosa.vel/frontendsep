import React from "react";

import { useSelector } from "react-redux";
import BaseCircularProgress from "../components/BaseCircularProgress";
import LayoutAdmin from "../layout";
import AppRoutes from "./AppRoutes";

function AppRouter() {
  const { auth, global } = useSelector((state) => state);
  return (
    <>
      <BaseCircularProgress loader={global.loader} />
      <LayoutAdmin loggedIn={auth.auth}>
        <AppRoutes loggedIn={auth.auth} />
      </LayoutAdmin>
    </>
  );
}

export default AppRouter;
