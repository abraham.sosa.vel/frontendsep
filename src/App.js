import React from "react";
import { I18nextProvider } from "react-i18next";
import i18next from "./translations/configureLanguaje";
import { store } from "./redux/store";
import { Theme } from "./theme/BaseStyles";
import { LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import "dayjs/locale/es";
import Alerts from "./pages/Alerts";
import { Provider } from "react-redux";
import AppRouter from "./routes";
import { BrowserRouter as Router } from "react-router-dom";
import "./App.css";

const App = () => {
  return (
    <Router basename="/">
      <Provider store={store}>
        <Theme>
          <I18nextProvider i18n={i18next}>
            <LocalizationProvider
              dateAdapter={AdapterDayjs}
              adapterLocale={"es"}
            >
              <AppRouter />
              <Alerts />
            </LocalizationProvider>
          </I18nextProvider>
        </Theme>
      </Provider>
    </Router>
  );
};
export default App;
