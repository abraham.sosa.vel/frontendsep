import { configureStore } from "@reduxjs/toolkit";
import authReducer from "../containers/auth/redux/redux";
import GlobalStoreSliceReducer from "./globalRedux";
import userReducer from "../containers/Users/redux/redux";
import registerReducer from "../containers/Registers/redux/redux";
import generalFieldReducer from "../containers/GeneralFields/redux/redux";
import specificReportReducer from "../containers/SpecificReports/redux/redux";
import generalReportReducer from "../containers/GeneralReports/redux/redux";
import tracingReportReducer from "../containers/TracingReport/redux/redux";

export const store = configureStore({
  reducer: {
    global: GlobalStoreSliceReducer,
    auth: authReducer,
    user: userReducer,
    register: registerReducer,
    generalFields: generalFieldReducer,
    specificReports: specificReportReducer,
    generalRepors: generalReportReducer,
    tracingReport: tracingReportReducer,
  },
});
