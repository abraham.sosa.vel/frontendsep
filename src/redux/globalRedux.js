import { createSlice } from "@reduxjs/toolkit";
const initialState = {
  themeSelect: localStorage.getItem("themeSelected") || "light",
  message: {
    type: "success",
    message: "",
    status: false,
  },
  loader: false,
};
export const globalSlice = createSlice({
  name: "global",
  initialState,
  reducers: {
    changeTheme: (state, action) => {
      state.themeSelect = action.payload;
    },
    messageSuccess: (state, action) => {
      state.message = {
        type: "success",
        message: action.payload,
        status: true,
      };
    },
    messageError: (state, action) => {
      state.message = {
        type: "error",
        message: action.payload.message,
        status: true,
      };
    },
    showGlobalLoader: (state, action) => {
      state.loader = true;
    },
    hideGlobalLoader: (state, action) => {
      state.loader = false;
    },
  },
});

export const {
  changeTheme,
  messageSuccess,
  messageError,
  showGlobalLoader,
  hideGlobalLoader,
} = globalSlice.actions;
export default globalSlice.reducer;
